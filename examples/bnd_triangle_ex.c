#include <stdlib.h>

#include "bnd_triangle.h"
#include "vertex_generator.h"

#ifdef DEBUG
#include "debug_output.h"
#endif // DEBUG

#define NUM_VERTICES (12)
#define NUM_TESTS (16)

int compare_vertices (const void * a, const void * b);

int main(void) {

  // double vertices[NUM_VERTICES][3];
  // generate_vertices(&vertices[0][0], NUM_VERTICES);

  double vertices[NUM_VERTICES][3] =
{{0.70021450728255308, 0.13928132531715343, -0.70021450728255319},
 {0.5252893005325312, 0.32563440388875486, -0.78615099424351009},
 {0.67859834454584689, 0.28108463771482023, -0.678598344545847},
 {0.38268343236508973, 0, -0.92387953251128674},
 {0.55557023301960218, 0, -0.83146961230254524},
 {0.54812419193877182, 0.16317295173655549, -0.82032582431193046},
 {0.37638067707860257, 0.18074437515937392, -0.90866333521833065},
 {0.35740674433659325, 0.35740674433659325, -0.86285620946101682},
 {0.37638067707860257, -0.18074437515937392, -0.90866333521833065},
 {0.54812419193877182, -0.16317295173655549, -0.82032582431193046},
 {0.70021450728255308, -0.13928132531715343, -0.70021450728255319},
 {0.70710678118654746, 0, -0.70710678118654757}};

  // sort vertices to ensure bit-reproducibility
  qsort(&vertices[0][0], NUM_VERTICES, 3*sizeof(vertices[0][0]),
        compare_vertices);

  double triangle[3][3];
  // compute the bounding triangle for the given vertices
  compute_bnd_triangle(&vertices[0][0], NUM_VERTICES, &triangle[0][0],
                       NUM_TESTS);

#ifdef DEBUG
  // debug output (final solution)
  print_bnd_triangle(&vertices[0][0], NUM_VERTICES, &triangle[0][0], NUM_TESTS);
#endif // DEBUG

  return EXIT_SUCCESS;
}

int compare_vertices (const void * a, const void * b)
{
  if (((double*)a)[0] != ((double*)b)[0])
    return (((double*)a)[0] > ((double*)b)[0])?-1:1;
  else if (((double*)a)[1] != ((double*)b)[1])
    return (((double*)a)[1] > ((double*)b)[1])?-1:1;
  else if (((double*)a)[2] != ((double*)b)[2])
    return (((double*)a)[2] > ((double*)b)[2])?-1:1;
  else return 0;
}
