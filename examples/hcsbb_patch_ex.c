#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "read_icon_grid.h"
#include "sbb_patch.h"
#include "bnd_triangle.h"
#include "sphere_part.h"
#include "geometry.h"
#include "test_function.h"
#include "generate_cubed_sphere.h"
#include "patch_common.h"
#include "utils.h"
#include "weight_store.h"

#ifdef DEBUG
#include "debug_output.h"
#endif

// degree of polynomial used to estimate directional derivatives
#define SBB_DEGREE (3)
// HCSBB degree (this has to be 3!)
#define HCSBB_DEGREE (3)

static void
derivative(double * base_point, double * direction, double * vertices,
           struct point_sphere_part_search * point_search,
           struct weight_vector * weights);
static double * vec_diff(double * a, double * b);
int vertex_is_in_triangle(double * vertex, double * triangle);
void compute_hcsbb_vertex_weights(
  double * vertex, double * triangle, struct weight_vector * hcsbb_weights,
  struct weight_vector * hcsbb_c_111_a_weights,
  struct weight_vector * weights);
static void get_triangle_gauss_legendre(
  unsigned gauss_order, double * abscissa);
void generate_gauss_legendre_points(
  double ** vertices, unsigned * num_vertices, double * triangle,
  unsigned gauss_order);

int main(void) {

  const char * icon_filename = "data/iconR2B03-grid.nc";
  int icon_nbr_vertices, icon_nbr_cells, icon_nbr_edges,
      * icon_num_vertices_per_cell, * icon_cell_to_vertex,
      * icon_vertex_to_cell, * icon_edge_to_vertex, * icon_cell_mask;
  double * icon_x_vertices, * icon_y_vertices, * icon_x_cells, * icon_y_cells,
         * icon_x_edges, * icon_y_edges;

  // read icon grid
  read_icon_grid_information(icon_filename, &icon_nbr_vertices, &icon_nbr_cells,
                             &icon_nbr_edges, &icon_num_vertices_per_cell,
                             &icon_cell_to_vertex, &icon_vertex_to_cell,
                             &icon_edge_to_vertex,
                             &icon_x_vertices, &icon_y_vertices,
                             &icon_x_cells, &icon_y_cells,
                             &icon_x_edges, &icon_y_edges,
                             &icon_cell_mask);

  free(icon_num_vertices_per_cell);
  free(icon_cell_mask);

  double * icon_vertices =
    malloc(3 * icon_nbr_vertices * sizeof(icon_vertices));
  for (int i = 0; i < icon_nbr_vertices; ++i)
    LLtoXYZ(icon_x_vertices[i], icon_y_vertices[i], icon_vertices + 3 * i);

  struct point_sphere_part_search * point_search =
    yac_point_sphere_part_search_new(
      icon_nbr_vertices, icon_x_vertices, icon_y_vertices);

  unsigned num_sbb_coeffs = get_num_sbb_coefficients(HCSBB_DEGREE);
  struct weight_vector * hcsbb_weights =
    malloc(num_sbb_coeffs * icon_nbr_cells * sizeof(*hcsbb_weights));
  struct weight_vector * hcsbb_c_111_a_weights =
    malloc(3 * icon_nbr_cells * sizeof(*hcsbb_c_111_a_weights));

  for (unsigned i = 0; i < num_sbb_coeffs * icon_nbr_cells; ++i)
    weight_vector_init(hcsbb_weights+i);
  for (unsigned i = 0; i < 3 * icon_nbr_cells; ++i)
    weight_vector_init(hcsbb_c_111_a_weights+i);

  // generate HCSBB-patch for all icon cells
  {
    for (int i = 0; i < icon_nbr_cells; ++i) {

      struct weight_vector * curr_hcsbb_weights =
        hcsbb_weights + i * num_sbb_coeffs;

      // c_300 = f(v1), c_030 = f(v2), c_003 = f(v3)
      // c_210 = c_300 + D_12 p(v1)/3 // compute remaining boundary coefficients
      //                             // analogical
      //! \todo write optimised version for computation of derivatives to avoid
      //!       needlessly computation and to reuse data (e.g. sb_polynomials at
      //!       vertices for different direction and adjacent triangles)
      // c_111 = c_111(v) = SUM(a_l*A_l(b_1(v), b_2(v), b_3(v))) where 1 <= l <= 3
      // a_l = paper: 4.1. Computing the Interior Coefficients
      // A_l(v) = paper: 4.2 Choice of the Blending Function A_l

      int curr_icon_cell_vertex_idx[3] =
        {icon_cell_to_vertex[3*i+0],
         icon_cell_to_vertex[3*i+1],
         icon_cell_to_vertex[3*i+2]};
      double curr_icon_cell[3][3] =
        {{icon_vertices[3*curr_icon_cell_vertex_idx[0]+0],
          icon_vertices[3*curr_icon_cell_vertex_idx[0]+1],
          icon_vertices[3*curr_icon_cell_vertex_idx[0]+2]},
         {icon_vertices[3*curr_icon_cell_vertex_idx[1]+0],
          icon_vertices[3*curr_icon_cell_vertex_idx[1]+1],
          icon_vertices[3*curr_icon_cell_vertex_idx[1]+2]},
         {icon_vertices[3*curr_icon_cell_vertex_idx[2]+0],
          icon_vertices[3*curr_icon_cell_vertex_idx[2]+1],
          icon_vertices[3*curr_icon_cell_vertex_idx[2]+2]}};

#define D_xy_3(x, y, w_idx) \
  { \
    size_t w_idx_ = w_idx; \
    derivative( \
      (icon_vertices + 3 * curr_icon_cell_vertex_idx[(x)-1]), \
      vec_diff(icon_vertices + 3 * curr_icon_cell_vertex_idx[(x)-1], \
              icon_vertices + 3 * curr_icon_cell_vertex_idx[(y)-1]), \
      icon_vertices, point_search, curr_hcsbb_weights + w_idx_); \
    weight_vector_mult_scalar(curr_hcsbb_weights+w_idx_, 1.0 / 3.0); \
  }
#define ADD_CORNER(w_idx, c_idx) \
  weight_vector_add_weight( \
    curr_hcsbb_weights+(w_idx), 1.0, curr_icon_cell_vertex_idx[(c_idx)-1])

      // c_003 = f(v3)
      ADD_CORNER(0,3); // f(v3)
      // c_012 = f(v3) + D_32 p(v3)/3
      D_xy_3(3,2,1); // D_32 p(v3)/3
      ADD_CORNER(1,3); // f(v3)
      // c_021 = f(v2) + D_23 p(v2)/3
      D_xy_3(2,3,2); // D_23 p(v2)/3
      ADD_CORNER(2,2); // f(v2)
      // c_030 = f(v2)
      ADD_CORNER(3,2); // f(v2)
      // c_102 = f(v3) + D_31 p(v3)/3
      D_xy_3(3,1,4); // D_31 p(v3)/3
      ADD_CORNER(4,3); // f(v3)
      // c_111 is computed each v individually
      // c_120 = f(v2) + D_21 p(v2)/3
      D_xy_3(2,1,6); // D_21 p(v2)/3
      ADD_CORNER(6,2); // f(v2)
      // c_201 = f(v1) + D_13 p(v1)/3
      D_xy_3(1,3,7); // D_13 p(v1)/3
      ADD_CORNER(7,1); // f(v1)
      // c_210 = f(v1) + D_12 p(v1)/3
      D_xy_3(1,2,8); // D_12 p(v1)/3
      ADD_CORNER(8,1); // f(v1)
      // c_300 = f(v1)
      ADD_CORNER(9,1); // f(v1)

#undef ADD_CORNER
#undef D_xy_3

      // z_w1/3 = B_020(w1) * (b_1(g1)*c_120 + b_2(g1)*c_030 + b_3(g1)*c_021) +
      //          B_011(w1) * (b_1(g1)*a_1     b_2(g1)*c_021 + b_3(g1)*c_012) +
      //          B_002(w1) * (b_1(g1)*c_102 + b_2(g1)*c_012 + b_3(g1)*c_003)
      // z_w1 = D_g f(w1)
      // w1 = ||v_2+v_3||
      // g1 = v_3 x v_2
      // =>
      // a_1 = (D_g f(w1)/3 -
      //        B_020(w1) * (b_1(g1)*c_120 + b_2(g1)*c_030 + b_3(g1)*c_021) -
      //        B_011(w1) * (                b_2(g1)*c_021 + b_3(g1)*c_012) -
      //        B_002(w1) * (b_1(g1)*c_102 + b_2(g1)*c_012 + b_3(g1)*c_003)) /
      //       (B_011(w1) * b_1(g1))
      // a_2 = (D_g f(w2)/3 -
      //        B_200(w2) * (b_1(g2)*c_300 + b_2(g2)*c_210 + b_3(g2)*c_201) -
      //        B_101(w2) * (b_1(g2)*c_201 +                 b_3(g2)*c_102) -
      //        B_002(w2) * (b_1(g2)*c_102 + b_2(g2)*c_012 + b_3(g2)*c_003)) /
      //       (B_101(w2) * b_2(g2))
      // a_3 = (D_g f(w3)/3 -
      //        B_200(w3) * (b_1(g3)*c_300 + b_2(g3)*c_210 + b_3(g3)*c_201) -
      //        B_110(w3) * (b_1(g3)*c_210 + b_2(g3)*c_120                ) -
      //        B_020(w3) * (b_1(g3)*c_120 + b_2(g3)*c_030 + b_3(g3)*c_021)) /
      //       (B_110(w3) * b_3(g3))

      double g[3][3];
      double w[3][3];
      double b_g[3][3];
      double * sb_polynomials =
        malloc(
          3 * get_num_sbb_coefficients(HCSBB_DEGREE-1) * sizeof(*sb_polynomials));

      for (int j = 0; j < 3; ++j) {
        for (int k = 0; k < 3; ++k)
          w[j][k] = curr_icon_cell[(j+1)%3][k] + curr_icon_cell[(j+2)%3][k];
        normalise_vector(&w[j][0]);
        // order of vertices does not matter does not matter
        // length of g is irrelevant -> no normalisation required 
        crossproduct_d(
          curr_icon_cell[(j+2)%3], curr_icon_cell[(j+1)%3], &g[j][0]);
      }

      compute_sb_polynomials(
        HCSBB_DEGREE-1, &curr_icon_cell[0][0], &w[0][0], 3, sb_polynomials);
      compute_sb_coords(&g[0][0], 3, &curr_icon_cell[0][0], &b_g[0][0]);

#define D_3(w, direction, w_idx) \
  { \
    derivative( \
      (w), (direction), icon_vertices, point_search, \
      hcsbb_c_111_a_weights + 3*i + w_idx); \
    weight_vector_mult_scalar( \
      hcsbb_c_111_a_weights + 3*i + w_idx, 1.0 / 3.0); \
  }

#define B(i, j, k, w) ( \
  sb_polynomials[(w) + 3 * get_sbb_coeff_idx_from_ijk((i), (j), (k), 2)])

#define C(i, j, k) ( \
  curr_hcsbb_weights + get_sbb_coeff_idx_from_ijk((i), (j), (k), (HCSBB_DEGREE)))

#define ADD_WEIGHTS_MULT_SCALAR(w_idx, weight_vector, scalar) (\
    weight_vector_add_weight_vector( \
      hcsbb_c_111_a_weights+3*i+(w_idx), (weight_vector), (scalar)))


// remark: we do not actually have the coefficients c_ijk, instead each
//         coefficient is represented by a weight vector

      double poly_factor;

      // D(&w[0][0], &g[0][0], 0) / 3.0
      D_3(&w[0][0], &g[0][0], 0);
      // - B(0,2,0,0) * (b_g[0][0]*C(1,2,0) + b_g[0][1]*C(0,3,0) + b_g[0][2]*C(0,2,1))
      poly_factor = -1.0 * B(0,2,0,0);
      ADD_WEIGHTS_MULT_SCALAR(0, C(1,2,0), b_g[0][0] * poly_factor);
      ADD_WEIGHTS_MULT_SCALAR(0, C(0,3,0), b_g[0][1] * poly_factor);
      ADD_WEIGHTS_MULT_SCALAR(0, C(0,2,1), b_g[0][2] * poly_factor);
      // - B(0,1,1,0) * (                     b_g[0][1]*C(0,2,1) + b_g[0][2]*C(0,1,2))
      poly_factor = -1.0 * B(0,1,1,0);
      ADD_WEIGHTS_MULT_SCALAR(0, C(0,2,1), b_g[0][1] * poly_factor);
      ADD_WEIGHTS_MULT_SCALAR(0, C(0,1,2), b_g[0][2] * poly_factor);
      // - B(0,0,2,0) * (b_g[0][0]*C(1,0,2) + b_g[0][1]*C(0,1,2) + b_g[0][2]*C(0,0,3))
      poly_factor = -1.0 * B(0,0,2,0);
      ADD_WEIGHTS_MULT_SCALAR(0, C(1,0,2), b_g[0][0] * poly_factor);
      ADD_WEIGHTS_MULT_SCALAR(0, C(0,1,2), b_g[0][1] * poly_factor);
      ADD_WEIGHTS_MULT_SCALAR(0, C(0,0,3), b_g[0][2] * poly_factor);
      // vector / (B(0,1,1,0) * b_g[0][0])
      weight_vector_compact(hcsbb_c_111_a_weights + 3*i + 0);
      weight_vector_mult_scalar(
        hcsbb_c_111_a_weights + 3*i + 0, 1.0 / (B(0,1,1,0) * b_g[0][0]));

      // D(&w[1][0], &g[1][0], 0) / 3.0
      D_3(&w[1][0], &g[1][0], 1);
      // - B(2,0,0,1) * (b_g[1][0]*C(3,0,0) + b_g[1][1]*C(2,1,0) + b_g[1][2]*C(2,0,1))
      poly_factor = -1.0 * B(2,0,0,1);
      ADD_WEIGHTS_MULT_SCALAR(1, C(3,0,0), b_g[1][0] * poly_factor);
      ADD_WEIGHTS_MULT_SCALAR(1, C(2,1,0), b_g[1][1] * poly_factor);
      ADD_WEIGHTS_MULT_SCALAR(1, C(2,0,1), b_g[1][2] * poly_factor);
      // - B(1,0,1,1) * (b_g[1][0]*C(2,0,1)                      + b_g[1][2]*C(1,0,2))
      poly_factor = -1.0 * B(1,0,1,1);
      ADD_WEIGHTS_MULT_SCALAR(1, C(2,0,1), b_g[1][0] * poly_factor);
      ADD_WEIGHTS_MULT_SCALAR(1, C(1,0,2), b_g[1][2] * poly_factor);
      // - B(0,0,2,1) * (b_g[1][0]*C(1,0,2) + b_g[1][1]*C(0,1,2) + b_g[1][2]*C(0,0,3))
      poly_factor = -1.0 * B(0,0,2,1);
      ADD_WEIGHTS_MULT_SCALAR(1, C(1,0,2), b_g[1][0] * poly_factor);
      ADD_WEIGHTS_MULT_SCALAR(1, C(0,1,2), b_g[1][1] * poly_factor);
      ADD_WEIGHTS_MULT_SCALAR(1, C(0,0,3), b_g[1][2] * poly_factor);
      // vector / (B(1,0,1,1) * b_g[1][1])
      weight_vector_compact(hcsbb_c_111_a_weights + 3*i + 1);
      weight_vector_mult_scalar(
        hcsbb_c_111_a_weights + 3*i + 1, 1.0 / (B(1,0,1,1) * b_g[1][1]));

      // D(&w[2][0], &g[2][0], 2) / 3.0
      D_3(&w[2][0], &g[2][0], 2);
      // - B(2,0,0,2) * (b_g[2][0]*C(3,0,0) + b_g[2][1]*C(2,1,0) + b_g[2][2]*C(2,0,1))
      poly_factor = -1.0 * B(2,0,0,2);
      ADD_WEIGHTS_MULT_SCALAR(2, C(3,0,0), b_g[2][0] * poly_factor);
      ADD_WEIGHTS_MULT_SCALAR(2, C(2,1,0), b_g[2][1] * poly_factor);
      ADD_WEIGHTS_MULT_SCALAR(2, C(2,0,1), b_g[2][2] * poly_factor);
      // - B(1,1,0,2) * (b_g[2][0]*C(2,1,0) + b_g[2][1]*C(1,2,0)                     )
      poly_factor = -1.0 * B(1,1,0,2);
      ADD_WEIGHTS_MULT_SCALAR(2, C(2,1,0), b_g[2][0] * poly_factor);
      ADD_WEIGHTS_MULT_SCALAR(2, C(1,2,0), b_g[2][1] * poly_factor);
      // - B(0,2,0,2) * (b_g[2][0]*C(1,2,0) + b_g[2][1]*C(0,3,0) + b_g[2][2]*C(0,2,1))
      poly_factor = -1.0 * B(0,2,0,2);
      ADD_WEIGHTS_MULT_SCALAR(2, C(1,2,0), b_g[2][0] * poly_factor);
      ADD_WEIGHTS_MULT_SCALAR(2, C(0,3,0), b_g[2][1] * poly_factor);
      ADD_WEIGHTS_MULT_SCALAR(2, C(0,2,1), b_g[2][2] * poly_factor);
      // vector / (B(1,1,0,2) * b_g[2][2])
      weight_vector_compact(hcsbb_c_111_a_weights + 3*i + 2);
      weight_vector_mult_scalar(
        hcsbb_c_111_a_weights + 3*i + 2, 1.0 / (B(1,1,0,2) * b_g[2][2]));

      free(sb_polynomials);

#undef ADD_WEIGHTS_MULT_SCALAR
#undef C
#undef B
#undef D_3
    }
  }

  unsigned cubed_n = 128; // number of vertices per edge of base cube
  unsigned num_cubed_vertices;
  double * cubed_vertices;
  {
    unsigned num_cubed_cells;
    unsigned * cubed_vertices_of_cell, * cubed_face_id;

    // generate cubed sphere grid with resolution higher than icon
    generate_cubed_sphere_grid_information(
      cubed_n, &num_cubed_cells, &num_cubed_vertices, &cubed_vertices,
      &cubed_vertices_of_cell, &cubed_face_id);

    free(cubed_vertices_of_cell);
    free(cubed_face_id);
  }

  struct weight_matrix weights_matrix;
  weights_matrix.weights =
    malloc(num_cubed_vertices * sizeof(*(weights_matrix.weights)));
  weights_matrix.n = num_cubed_vertices;

  // find associated icon cell for each cubed sphere vertex
  // and generate required weights
  {
    unsigned * closest_icon_vertex_idx = NULL;
    unsigned closest_icon_vertex_idx_array_size = 0;
    unsigned * num_closest_icon_vertex_idx =
      malloc(num_cubed_vertices * sizeof(num_closest_icon_vertex_idx));

    // find for each cubed vertex the closest icon vertex
    yac_point_sphere_part_search_NN(
      point_search, num_cubed_vertices, cubed_vertices,
      NULL, &closest_icon_vertex_idx, &closest_icon_vertex_idx_array_size,
      num_closest_icon_vertex_idx);

    unsigned * curr_closest_icon_vertex_idx_ = closest_icon_vertex_idx;

    for (unsigned i = 0; i < num_cubed_vertices; ++i) {

      double * curr_cubed_vertex = cubed_vertices + i * 3;

      unsigned curr_closest_icon_vertex_idx = *curr_closest_icon_vertex_idx_;

      unsigned j;
      for (j = 0; j < 6; ++j) {

        unsigned curr_icon_cell_idx =
          icon_vertex_to_cell[6*curr_closest_icon_vertex_idx+j];

        double triangle[3][3];

        for (unsigned k = 0; k < 3; ++k) {
          unsigned curr_icon_vertex_idx =
            icon_cell_to_vertex[3*curr_icon_cell_idx+k];
          for (unsigned l = 0; l < 3; ++l) {
            triangle[k][l] = icon_vertices[3*curr_icon_vertex_idx + l];
          }
        }

        if (vertex_is_in_triangle(curr_cubed_vertex, &triangle[0][0])) {

          // get indices of icon cell
          int icon_cell_vertex_idx[3] =
            {icon_cell_to_vertex[3*curr_icon_cell_idx+0],
             icon_cell_to_vertex[3*curr_icon_cell_idx+1],
             icon_cell_to_vertex[3*curr_icon_cell_idx+2]};
          // get matching icon cell
          double icon_cell[3][3] =
            {{icon_vertices[3*icon_cell_vertex_idx[0]+0],
              icon_vertices[3*icon_cell_vertex_idx[0]+1],
              icon_vertices[3*icon_cell_vertex_idx[0]+2]},
             {icon_vertices[3*icon_cell_vertex_idx[1]+0],
              icon_vertices[3*icon_cell_vertex_idx[1]+1],
              icon_vertices[3*icon_cell_vertex_idx[1]+2]},
             {icon_vertices[3*icon_cell_vertex_idx[2]+0],
              icon_vertices[3*icon_cell_vertex_idx[2]+1],
              icon_vertices[3*icon_cell_vertex_idx[2]+2]}};

          // compute the weights for the current cubed sphere vertex
          compute_hcsbb_vertex_weights(
            cubed_vertices+3*i, &icon_cell[0][0],
            hcsbb_weights + num_sbb_coeffs * curr_icon_cell_idx,
            hcsbb_c_111_a_weights + 3 * curr_icon_cell_idx,
            weights_matrix.weights + i);

          break;
        }
      }
      if (j == 6)
        abort_message("could not find matching icon cell", __FILE__, __LINE__);
      curr_closest_icon_vertex_idx_ += num_closest_icon_vertex_idx[i];
    }

    free(num_closest_icon_vertex_idx);
    free(closest_icon_vertex_idx);
  }

  for (unsigned i = 0; i < num_sbb_coeffs * icon_nbr_cells; ++i)
    weight_vector_free(hcsbb_weights + i);
  free(hcsbb_weights);
  for (unsigned i = 0; i < 3 * icon_nbr_cells; ++i)
    weight_vector_free(hcsbb_c_111_a_weights + i);
  free(hcsbb_c_111_a_weights);

  double * icon_field_data =
    malloc(icon_nbr_vertices * sizeof(*icon_field_data));

  // generate field data for all vertices of icon grid
  compute_field_data(icon_vertices, icon_nbr_vertices, icon_field_data);

#ifdef DEBUG
  {
    char * filename = "field_data_icon.vtk";
    print_field(
      icon_vertices, icon_nbr_vertices, icon_field_data, 1, filename,
      "icon_data", (char const *[]){"f"});
  }
#endif // DEBUG

  { // print results
    unsigned data_offset = 0;
    double * data = malloc(3 * num_cubed_vertices * sizeof(*data));
    double * field_data_ref = data + (data_offset++) * num_cubed_vertices;
    double * field_data_interp = data + (data_offset++) * num_cubed_vertices;
    double * field_data_err = data + (data_offset++) * num_cubed_vertices;

    weight_matrix_mult_vector(
      weights_matrix, icon_field_data, field_data_interp);

    for (unsigned i = 0; i < num_cubed_vertices; ++i)
      weight_vector_free(weights_matrix.weights + i);
    free(weights_matrix.weights);

    // compute reference field for cubed sphere vertices
    compute_field_data(cubed_vertices, num_cubed_vertices, field_data_ref);

    double field_avg = 0.0;
    for (unsigned i = 0; i < num_cubed_vertices; ++i)
      field_avg += field_data_ref[i];
    field_avg /= (double)num_cubed_vertices;
    field_avg = fabs(field_avg);

    double min_err = 0, max_err = 0;
    double min_err_w = 0, max_err_w = 0;

    // compute error
    double e[3] = {1.0, 0.0, 0.0};
    for (unsigned i = 0; i < num_cubed_vertices; ++i) {
      field_data_err[i] =
        (field_data_interp[i] - field_data_ref[i]) / (field_avg + 1e-13);
      min_err = MIN(min_err, field_data_err[i]);
      max_err = MAX(max_err, field_data_err[i]);
    }

    printf("min_err   %e max_err   %e\n", min_err, max_err);

    // print results
#ifdef DEBUG
    {
      char * filename = "field_data_hcsbb.vtk";
      print_field(
        cubed_vertices, num_cubed_vertices, data, 3, filename,
        "hcsbb_data", (char const *[]){"f_ref", "f_interp", "err"});
    }
#endif // DEBUG
    free(data);
  }

  yac_delete_point_sphere_part_search(point_search);
  free(cubed_vertices);
  free(icon_cell_to_vertex);
  free(icon_vertex_to_cell);
  free(icon_x_cells);
  free(icon_y_cells);
  free(icon_field_data);
  free(icon_vertices);
  free(icon_x_edges);
  free(icon_y_edges);
  free(icon_x_vertices);
  free(icon_y_vertices);
  free(icon_edge_to_vertex);

  return EXIT_SUCCESS;
}

static void
derivative(double * base_point, double * direction, double * vertices,
           struct point_sphere_part_search * point_search,
           struct weight_vector * weights) {

  // the derivative df at point p_b (base point) in the direction u (which is
  // the vector [u_x,u_y,u_z]) is defined as:
  // (1) d(f)/d(u) = u_x * d(f)/d(x) + u_y * d(f)/d(y) + u_z * d(f)/d(z)
  // or
  // (2) d(f)/d(u) = (f(p_b + h*u)-f(p_b))/h
  //     for h -> 0
  //
  // Since, we do not have the original function f, we have to estimate f which
  // gives us f_e. To estimate df(u), we then compute df_e(u). We use spherical
  // Bernstein-Bézier (SBB) polynomial to compute f_e [1].
  // Using the SBB polynomials we can compute any point p of f_e as follows:
  // (3) f_e(p) = B(p)^T * c
  // with B(p)^T being the transposed vector of the polynomials evaluated at p
  //      and c being a set of coefficients
  //
  // For a set of source points we can write the follwoing:
  // (4) f_s = P_s * c
  // where f_s is a vector of function values at the respective source points
  //       and P_s i the matrix SBB polynomials evaluated at the source points
  // Formula (4) is also valid for any function value at any location that is
  // within the respective patch described by f_e:
  // (5) f_e = P_e * c
  // In the put operation of the coupler f_s is provided and P_s can computed
  // based on the location of the source points. Using a solver we could then
  // compute c. However, we do not want to call a solver in every put operation,
  // because it would be too time consuming. Additionally, you cannot write this
  // into a weight file for later reuse. We need a direct method to compute f_e
  // from f_s.
  //
  // We can transform (4) into the following:
  // (6) P_s^T * f_s = P_s^T * P_s * c
  // (7) c = (P_s^T * P_s)^-1 * P_s^T * f_s
  // (8) f_e(p) = P_p * c
  // (9) f_e(p) = P_p * (P_s^T * P_s)^-1 * P_s^T * f_s
  // (10) f_e(p) = w * f_s
  // with
  // (11) w = P_p * (P_s^T * P_s)^-1 * P_s^T
  //
  // Formula (11) gives us a method to compute a weight matrix that can be used
  // to compute f_e based on the function values f_s at given point locations.
  //
  // Based on (2) we can derive:
  // (12) d(f_e(p_b))/d(u) = (f_e(p_b + h*u) - f_e(p_b)) / h
  // use (10) in (12)
  // (13) d(f_e(p_b))/d(u) = (w(p_b + h*u) * f_s - w(p_b) * f_s) / h
  // (14) d(f_e(p_b))/d(u) =
  //      (P_p(p_b + h*u) - P_p(p_b)) / h * (P_s^T * P_s)^-1 * P_s^T * f_s
  // (15) d(P_p(p_b))/d(u) = (P_p(p_b + h*u) - P_p(p_b)) / h
  // (16) d(f_e(p_b))/d(u) = w_p * w_s * f_s
  // with
  // (17) w_p = d(P_p(p_b))/d(u)
  // (18) w_s = (P_s^T * P_s)^-1 * P_s^T
  // The directional derivative of the SBB patch for p_b can be directly
  // computed, which gives w_p. The matrix w_s can be reused for all points that
  // are within the respective patch described by f_e.
  //
  // Depending on the location of the source points the computation of the
  // matrix inverse required to compute w_s. Can be numerically instable. Due to
  // numerically inaccuracy this can give wrong results for d(f_e(p_b))/d(u).
  //
  // To avoid this problem, I do not compute w_s on the actual source locations.
  // Instead, I compute w_g, which is based on locations of gauss integration
  // points that I define within the triangle used for the SBB polynomials.
  // These locations make the computation of the matrix inverse much more
  // stable. This gives:
  // (19) f_e(p) = w_g * f_g
  // with f_g being the function values at the locations of the gauss
  // integration points and
  // (20) w_g = (P_g^T * P_g)^-1 * P_g^T
  // where P_g are the SBB polynomials evaluated at the gauss integration points
  //
  // To estimate f_g, we can use a inverse distance weighted n-nearest-neighbour
  // approach.
  // (21) f_g = w_nnn * f_s
  // with w_nnn being a nnn weight matrix.
  //
  // We can use this to compute d(f_e(p_b))/d(u) in a numerically stable way,
  // which is relatively accurate.
  // (22) d(f_e(p_b))/d(u) = w_p * w_g * w_nnn * f_s
  //
  // The actual algorithm look as follows:
  //   I. find the NNN nearest source points for p_b
  //  II. compute bounding triangle (bnd_triangle) around these points
  // III. generate gauss integration points for bnd_triangle (p_g)
  //  IV. compute w_p * w_g
  //   V. for each p_g(i)
  //     V.a find the NNN nearest source points
  //     V.b compute nnn weight vector
  //     V.c compute w_p * w_g * w_nnn(i)
  //  VI. combine all w_p * w_g * w_nnn(i) into w_p * w_g * w_nnn
  //
  // [1]: Peter Alfeld, Marian Neamtu, Larry L. Schumaker, Bernstein-Bézier
  //      polynomials on spheres and sphere-like surfaces, In Computer Aided
  //      Geometric Design, Volume 13, Issue 4, 1996, Pages 333-349,
  //      ISSN 0167-8396, https://doi.org/10.1016/0167-8396(95)00030-5.
  //      (http://www.sciencedirect.com/science/article/pii/0167839695000305)

#define NNN (10)

  static unsigned * closest_vertices_idx = NULL;
  static unsigned closest_vertices_idx_array_size = 0;
  unsigned num_closest_vertices;

  yac_point_sphere_part_search_NNN(
    point_search, 1, base_point, NNN, NULL, NULL,
    &closest_vertices_idx, &closest_vertices_idx_array_size,
    &num_closest_vertices);

  if (num_closest_vertices < NNN)
    abort_message("internal error", __FILE__, __LINE__);

  double closest_vertices[NNN][3];

  for (int i = 0, idx = 0; i < NNN; ++i) {
    closest_vertices[i][0] = vertices[3*closest_vertices_idx[i]+0];
    closest_vertices[i][1] = vertices[3*closest_vertices_idx[i]+1];
    closest_vertices[i][2] = vertices[3*closest_vertices_idx[i]+2];
  }

  double bnd_triangle[3][3];
  compute_bnd_triangle(
    &closest_vertices[0][0], NNN, &bnd_triangle[0][0], 16);

  static const unsigned gauss_order = 7;
  unsigned num_gauss_vertices;
  double * gauss_vertices;

  generate_gauss_legendre_points(
    &gauss_vertices, &num_gauss_vertices, &bnd_triangle[0][0], gauss_order);

  unsigned num_sbb_coeffs = get_num_sbb_coefficients(SBB_DEGREE);
  double * sbb_polynomials =
    malloc(num_sbb_coeffs * num_gauss_vertices * sizeof(*sbb_polynomials));
  double d_sbb_vertex_polynomials[num_sbb_coeffs];

  compute_sb_polynomials(SBB_DEGREE, &bnd_triangle[0][0], gauss_vertices,
                         num_gauss_vertices, sbb_polynomials);
  compute_d_sb_polynomials(
    SBB_DEGREE, &bnd_triangle[0][0], base_point, 1,
    direction, &d_sbb_vertex_polynomials[0]);

  double sbb_weights[num_gauss_vertices];

  compute_patch_weights(
    num_sbb_coeffs, num_gauss_vertices, sbb_polynomials, 1,
    &d_sbb_vertex_polynomials[0], &sbb_weights[0]);

  unsigned num_local_point_ids[num_gauss_vertices];

  // find for each gaussian integration point of the bnd_triangle the
  // NNN-nearest source neighbours
  yac_point_sphere_part_search_NNN(
    point_search, num_gauss_vertices, gauss_vertices, NNN, NULL,
    NULL, &closest_vertices_idx, &closest_vertices_idx_array_size,
    &num_local_point_ids[0]);

  // for each gauss integration point
  for (size_t i = 0, closest_vertices_idx_offset = 0;
       i < num_gauss_vertices; ++i) {

    if (num_local_point_ids[i] < NNN)
      abort_message("could not find enough source points", __FILE__, __LINE__);

    unsigned * curr_closest_vertices_idx =
      closest_vertices_idx + closest_vertices_idx_offset;

    closest_vertices_idx_offset += num_local_point_ids[i];

    double distance_sum = 0.0;
    double inv_distances[NNN];
    unsigned flag = 0;

    for (size_t j = 0; j < NNN; ++j) {

      double curr_distance =
        get_vector_angle(
          gauss_vertices + 3*i, vertices + 3*curr_closest_vertices_idx[j]);

      // if both points are at nearly the same location
      if (curr_distance <= 1e-13) {
        flag = j + 1;
        break;
      }

      curr_distance *= curr_distance;

      // compute distance
      inv_distances[j] = 1.0 / curr_distance;
      distance_sum += inv_distances[j];
    }

    // if there is a point with nearly the same location
    if (flag) {

      weight_vector_add_weight(
        weights, sbb_weights[i], curr_closest_vertices_idx[flag-1]);

    } else {

      distance_sum = 1.0 / distance_sum;
      for (size_t j = 0; j < NNN; ++j)
        if (inv_distances[j] > 0.0)
          weight_vector_add_weight(
            weights, inv_distances[j] * distance_sum * sbb_weights[i],
            curr_closest_vertices_idx[j]);
    }
  }

  weight_vector_compact(weights);

  free(sbb_polynomials);
  free(gauss_vertices);

#undef NNN
}

void evaluate_blending_function(
  double * triangle, double * vertex, double * A) {

  double sb_coords[3];

  compute_sb_coords(vertex, 1, triangle, &sb_coords[0]);

  double b[3] = {sb_coords[1]*sb_coords[1] * sb_coords[2]*sb_coords[2],
                 sb_coords[0]*sb_coords[0] * sb_coords[2]*sb_coords[2],
                 sb_coords[0]*sb_coords[0] * sb_coords[1]*sb_coords[1]};
  double temp = 1.0 / (b[0] + b[1] + b[2]);

  for (size_t i = 0; i < 3; ++i) {

    if (fabs(b[i]) < 1e-9) A[i] = 0.0;
    else                   A[i] = b[i] * temp;
  }
}

void compute_hcsbb_vertex_weights(
  double * vertex, double * triangle, struct weight_vector * hcsbb_weights,
  struct weight_vector * hcsbb_c_111_a_weights,
  struct weight_vector * weights) {

  double A[3];
  evaluate_blending_function(triangle, vertex, &A[0]);

  unsigned num_sbb_coeffs = get_num_sbb_coefficients(HCSBB_DEGREE);
  double * restrict sb_polynomials =
    malloc(1 * num_sbb_coeffs * sizeof(*sb_polynomials));

  // compute the spherical Bernstein base polynomials for the given vertices
  compute_sb_polynomials(
    HCSBB_DEGREE, triangle, vertex, 1, sb_polynomials);

  weight_vector_init(weights);
  for (size_t i = 0; i < 10; ++i) {
    if (i == 5) continue;
    weight_vector_add_weight_vector(
      weights, hcsbb_weights + i, sb_polynomials[i]);
  }
  for (size_t i = 0; i < 3; ++i)
    weight_vector_add_weight_vector(
      weights, hcsbb_c_111_a_weights + i, sb_polynomials[5] * A[i]);

  free(sb_polynomials);

  weight_vector_compact(weights);
}

static double * vec_diff(double * a, double * b) {

  static double diff[3];
  diff[0] = b[0]-a[0];
  diff[1] = b[1]-a[1];
  diff[2] = b[2]-a[2];
  return &diff[0];
}

int vertex_is_in_triangle(double * vertex, double * triangle) {

  double sb_coords[3];
  compute_sb_coords(vertex, 1, triangle, &sb_coords[0]);
  return ((sb_coords[0] >= -1e-13) &&
          (sb_coords[1] >= -1e-13) &&
          (sb_coords[2] >= -1e-13));
}

static void get_triangle_gauss_legendre(
  unsigned gauss_order, double * abscissa) {

  if (gauss_order > 12)
    abort_message("internal error", __FILE__, __LINE__);

  if ( gauss_order <= 1 ) {
    abscissa[0]  =  0.33333333333333333;
    abscissa[1]  =  0.33333333333333333;
  }
  else if ( gauss_order == 2 ) {
    abscissa[0]  =  0.16666666666666667;
    abscissa[1]  =  0.16666666666666667;
    abscissa[2]  =  0.66666666666666667;
    abscissa[3]  =  0.16666666666666667;
    abscissa[4]  =  0.16666666666666667;
    abscissa[5]  =  0.66666666666666667;
  }
  else if ( gauss_order == 3 ) {
    abscissa[0]  =  0.33333333333333333;
    abscissa[1]  =  0.33333333333333333;
    abscissa[2]  =  0.20000000000000000;
    abscissa[3]  =  0.20000000000000000;
    abscissa[4]  =  0.60000000000000000;
    abscissa[5]  =  0.20000000000000000;
    abscissa[6]  =  0.20000000000000000;
    abscissa[7]  =  0.60000000000000000;
  }
  else if ( gauss_order == 4 ) {
    abscissa[0]  =  0.091576213509771;
    abscissa[1]  =  0.091576213509771;
    abscissa[2]  =  0.816847572980459;
    abscissa[3]  =  0.091576213509771;
    abscissa[4]  =  0.091576213509771;
    abscissa[5]  =  0.816847572980459;
    abscissa[6]  =  0.445948490915965;
    abscissa[7]  =  0.445948490915965;
    abscissa[8]  =  0.108103018168070;
    abscissa[9]  =  0.445948490915965;
    abscissa[10] =  0.445948490915965;
    abscissa[11] =  0.108103018168070;
  }
  else if ( gauss_order == 5 ) {
    abscissa[0]  =  0.333333333333333;
    abscissa[1]  =  0.333333333333333;
    abscissa[2]  =  0.101286507323456;
    abscissa[3]  =  0.101286507323456;
    abscissa[4]  =  0.797426985353087;
    abscissa[5]  =  0.101286507323456;
    abscissa[6]  =  0.101286507323456;
    abscissa[7]  =  0.797426985353087;
    abscissa[8]  =  0.470142064105115;
    abscissa[9]  =  0.470142064105115;
    abscissa[10] =  0.059715871789770;
    abscissa[11] =  0.470142064105115;
    abscissa[12] =  0.470142064105115;
    abscissa[13] =  0.059715871789770;
  }
  else if ( gauss_order == 6 ) {
    abscissa[0]  =  0.063089014491502;
    abscissa[1]  =  0.063089014491502;
    abscissa[2]  =  0.873821971016996;
    abscissa[3]  =  0.063089014491502;
    abscissa[4]  =  0.063089014491502;
    abscissa[5]  =  0.873821971016996;
    abscissa[6]  =  0.249286745170910;
    abscissa[7]  =  0.249286745170910;
    abscissa[8]  =  0.501426509658179;
    abscissa[9]  =  0.249286745170910;
    abscissa[10] =  0.249286745170910;
    abscissa[11] =  0.501426509658179;
    abscissa[12] =  0.310352451033785;
    abscissa[13] =  0.053145049844816;
    abscissa[14] =  0.053145049844816;
    abscissa[15] =  0.310352451033785;
    abscissa[16] =  0.636502499121399;
    abscissa[17] =  0.053145049844816;
    abscissa[18] =  0.053145049844816;
    abscissa[19] =  0.636502499121399;
    abscissa[20] =  0.636502499121399;
    abscissa[21] =  0.310352451033785;
    abscissa[22] =  0.310352451033785;
    abscissa[23] =  0.636502499121399;
  }
  else if ( gauss_order == 7 ) {
    abscissa[0]  =  0.333333333333333;
    abscissa[1]  =  0.333333333333333;
    abscissa[2]  =  0.260345966079038;
    abscissa[3]  =  0.260345966079038;
    abscissa[4]  =  0.479308067841923;
    abscissa[5]  =  0.260345966079038;
    abscissa[6]  =  0.260345966079038;
    abscissa[7]  =  0.479308067841923;
    abscissa[8]  =  0.065130102902216;
    abscissa[9]  =  0.065130102902216;
    abscissa[10] =  0.869739794195568;
    abscissa[11] =  0.065130102902216;
    abscissa[12] =  0.065130102902216;
    abscissa[13] =  0.869739794195568;
    abscissa[14] =  0.312865496004874;
    abscissa[15] =  0.048690315425316;
    abscissa[16] =  0.048690315425316;
    abscissa[17] =  0.312865496004874;
    abscissa[18] =  0.638444188569809;
    abscissa[19] =  0.048690315425316;
    abscissa[20] =  0.048690315425316;
    abscissa[21] =  0.638444188569809;
    abscissa[22] =  0.638444188569809;
    abscissa[23] =  0.312865496004874;
    abscissa[24] =  0.312865496004874;
    abscissa[25] =  0.638444188569809;
  } else if ( gauss_order == 8 ) {
    abscissa[0]  =  0.333333333333333;
    abscissa[1]  =  0.333333333333333;
    abscissa[2]  =  0.081414823414554;
    abscissa[3]  =  0.459292588292723;
    abscissa[4]  =  0.459292588292723;
    abscissa[5]  =  0.081414823414554;
    abscissa[6]  =  0.459292588292723;
    abscissa[7]  =  0.459292588292723;
    abscissa[8]  =  0.658861384496480;
    abscissa[9]  =  0.170569307751760;
    abscissa[10] =  0.170569307751760;
    abscissa[11] =  0.658861384496480;
    abscissa[12] =  0.170569307751760;
    abscissa[13] =  0.170569307751760;
    abscissa[14] =  0.898905543365938;
    abscissa[15] =  0.050547228317031;
    abscissa[16] =  0.050547228317031;
    abscissa[17] =  0.898905543365938;
    abscissa[18] =  0.050547228317031;
    abscissa[19] =  0.050547228317031;
    abscissa[20] =  0.008394777409958;
    abscissa[21] =  0.263112829634638;
    abscissa[22] =  0.263112829634638;
    abscissa[23] =  0.008394777409958;
    abscissa[24] =  0.008394777409958;
    abscissa[25] =  0.728492392955404;
    abscissa[26] =  0.728492392955404;
    abscissa[27] =  0.008394777409958;
    abscissa[28] =  0.263112829634638;
    abscissa[29] =  0.728492392955404;
    abscissa[30] =  0.728492392955404;
    abscissa[31] =  0.263112829634638;
  } else if ( gauss_order == 9 ) {
    abscissa[0]  =  0.333333333333333;
    abscissa[1]  =  0.333333333333333;
    abscissa[2]  =  0.020634961602525;
    abscissa[3]  =  0.489682519198738;
    abscissa[4]  =  0.489682519198738;
    abscissa[5]  =  0.020634961602525;
    abscissa[6]  =  0.489682519198738;
    abscissa[7]  =  0.489682519198738;
    abscissa[8]  =  0.125820817014127;
    abscissa[9]  =  0.437089591492937;
    abscissa[10] =  0.437089591492937;
    abscissa[11] =  0.125820817014127;
    abscissa[12] =  0.437089591492937;
    abscissa[13] =  0.437089591492937;
    abscissa[14] =  0.623592928761935;
    abscissa[15] =  0.188203535619033;
    abscissa[16] =  0.188203535619033;
    abscissa[17] =  0.623592928761935;
    abscissa[18] =  0.188203535619033;
    abscissa[19] =  0.188203535619033;
    abscissa[20] =  0.910540973211095;
    abscissa[21] =  0.044729513394453;
    abscissa[22] =  0.044729513394453;
    abscissa[23] =  0.910540973211095;
    abscissa[24] =  0.044729513394453;
    abscissa[25] =  0.044729513394453;
    abscissa[26] =  0.036838412054736;
    abscissa[27] =  0.221962989160766;
    abscissa[28] =  0.036838412054736;
    abscissa[29] =  0.741198598784498;
    abscissa[30] =  0.221962989160766;
    abscissa[31] =  0.036838412054736;
    abscissa[32] =  0.741198598784498;
    abscissa[33] =  0.036838412054736;
    abscissa[34] =  0.221962989160766;
    abscissa[35] =  0.741198598784498;
    abscissa[36] =  0.741198598784498;
    abscissa[37] =  0.221962989160766;
  } else if ( gauss_order == 10 ) {
    abscissa[0]  =  0.333333333333333;
    abscissa[1]  =  0.333333333333333;
    abscissa[2]  =  0.028844733232685;
    abscissa[3]  =  0.485577633383657;
    abscissa[4]  =  0.485577633383657;
    abscissa[5]  =  0.028844733232685;
    abscissa[6]  =  0.485577633383657;
    abscissa[7]  =  0.485577633383657;
    abscissa[8]  =  0.781036849029926;
    abscissa[9]  =  0.109481575485037;
    abscissa[10] =  0.109481575485037;
    abscissa[11] =  0.781036849029926;
    abscissa[12] =  0.109481575485037;
    abscissa[13] =  0.109481575485037;
    abscissa[14] =  0.141707219414880;
    abscissa[15] =  0.307939838764121;
    abscissa[16] =  0.141707219414880;
    abscissa[17] =  0.550352941820999;
    abscissa[18] =  0.307939838764121;
    abscissa[19] =  0.141707219414880;
    abscissa[20] =  0.550352941820999;
    abscissa[21] =  0.141707219414880;
    abscissa[22] =  0.307939838764121;
    abscissa[23] =  0.550352941820999;
    abscissa[24] =  0.550352941820999;
    abscissa[25] =  0.307939838764121;
    abscissa[26] =  0.025003534762686;
    abscissa[27] =  0.246672560639903;
    abscissa[28] =  0.025003534762686;
    abscissa[29] =  0.728323904597411;
    abscissa[30] =  0.246672560639903;
    abscissa[31] =  0.025003534762686;
    abscissa[32] =  0.728323904597411;
    abscissa[33] =  0.025003534762686;
    abscissa[34] =  0.246672560639903;
    abscissa[35] =  0.728323904597411;
    abscissa[36] =  0.728323904597411;
    abscissa[37] =  0.246672560639903;
    abscissa[38] =  0.009540815400299;
    abscissa[39] =  0.066803251012200;
    abscissa[40] =  0.009540815400299;
    abscissa[41] =  0.923655933587500;
    abscissa[42] =  0.066803251012200;
    abscissa[43] =  0.009540815400299;
    abscissa[44] =  0.923655933587500;
    abscissa[45] =  0.009540815400299;
    abscissa[46] =  0.066803251012200;
    abscissa[47] =  0.923655933587500;
    abscissa[48] =  0.923655933587500;
    abscissa[49] =  0.066803251012200;
  } else if ( gauss_order == 11 ) {
    abscissa[0]  = -0.069222096541517;
    abscissa[1]  =  0.534611048270758;
    abscissa[2]  =  0.534611048270758;
    abscissa[3]  = -0.069222096541517;
    abscissa[4]  =  0.534611048270758;
    abscissa[5]  =  0.534611048270758;
    abscissa[6]  =  0.202061394068290;
    abscissa[7]  =  0.398969302965855;
    abscissa[8]  =  0.398969302965855;
    abscissa[9]  =  0.202061394068290;
    abscissa[10] =  0.398969302965855;
    abscissa[11] =  0.398969302965855;
    abscissa[12] =  0.593380199137435;
    abscissa[13] =  0.203309900431282;
    abscissa[14] =  0.203309900431282;
    abscissa[15] =  0.593380199137435;
    abscissa[16] =  0.203309900431282;
    abscissa[17] =  0.203309900431282;
    abscissa[18] =  0.761298175434837;
    abscissa[19] =  0.119350912282581;
    abscissa[20] =  0.119350912282581;
    abscissa[21] =  0.761298175434837;
    abscissa[22] =  0.119350912282581;
    abscissa[23] =  0.119350912282581;
    abscissa[24] =  0.935270103777448;
    abscissa[25] =  0.032364948111276;
    abscissa[26] =  0.032364948111276;
    abscissa[27] =  0.935270103777448;
    abscissa[28] =  0.032364948111276;
    abscissa[29] =  0.032364948111276;
    abscissa[30] =  0.050178138310495;
    abscissa[31] =  0.356620648261293;
    abscissa[32] =  0.050178138310495;
    abscissa[33] =  0.593201213428213;
    abscissa[34] =  0.356620648261293;
    abscissa[35] =  0.050178138310495;
    abscissa[36] =  0.593201213428213;
    abscissa[37] =  0.050178138310495;
    abscissa[38] =  0.356620648261293;
    abscissa[39] =  0.593201213428213;
    abscissa[40] =  0.593201213428213;
    abscissa[41] =  0.356620648261293;
    abscissa[42] =  0.021022016536166;
    abscissa[43] =  0.171488980304042;
    abscissa[44] =  0.021022016536166;
    abscissa[45] =  0.807489003159792;
    abscissa[46] =  0.171488980304042;
    abscissa[47] =  0.021022016536166;
    abscissa[48] =  0.807489003159792;
    abscissa[49] =  0.021022016536166;
    abscissa[50] =  0.171488980304042;
    abscissa[51] =  0.807489003159792;
    abscissa[52] =  0.807489003159792;
    abscissa[53] =  0.171488980304042;
  } else if ( gauss_order == 12 ) {
    abscissa[0]  = 0.023565220452390;
    abscissa[1]  = 0.488217389773805;
    abscissa[2]  = 0.488217389773805;
    abscissa[3]  = 0.023565220452390;
    abscissa[4]  = 0.488217389773805;
    abscissa[5]  = 0.488217389773805;
    abscissa[6]  = 0.120551215411079;
    abscissa[7]  = 0.439724392294460;
    abscissa[8]  = 0.439724392294460;
    abscissa[9]  = 0.120551215411079;
    abscissa[10] = 0.439724392294460;
    abscissa[11] = 0.439724392294460;
    abscissa[12] = 0.457579229975768;
    abscissa[13] = 0.271210385012116;
    abscissa[14] = 0.271210385012116;
    abscissa[15] = 0.457579229975768;
    abscissa[16] = 0.271210385012116;
    abscissa[17] = 0.271210385012116;
    abscissa[18] = 0.744847708916828;
    abscissa[19] = 0.127576145541586;
    abscissa[20] = 0.127576145541586;
    abscissa[21] = 0.744847708916828;
    abscissa[22] = 0.127576145541586;
    abscissa[23] = 0.127576145541586;
    abscissa[24] = 0.957365299093579;
    abscissa[25] = 0.021317350453210;
    abscissa[26] = 0.021317350453210;
    abscissa[27] = 0.957365299093579;
    abscissa[28] = 0.021317350453210;
    abscissa[29] = 0.021317350453210;
    abscissa[30] = 0.115343494534698;
    abscissa[31] = 0.275713269685514;
    abscissa[32] = 0.115343494534698;
    abscissa[33] = 0.608943235779788;
    abscissa[34] = 0.275713269685514;
    abscissa[35] = 0.115343494534698;
    abscissa[36] = 0.608943235779788;
    abscissa[37] = 0.115343494534698;
    abscissa[38] = 0.275713269685514;
    abscissa[39] = 0.608943235779788;
    abscissa[40] = 0.608943235779788;
    abscissa[41] = 0.275713269685514;
    abscissa[42] = 0.022838332222257;
    abscissa[43] = 0.281325580989940;
    abscissa[44] = 0.022838332222257;
    abscissa[45] = 0.695836086787803;
    abscissa[46] = 0.281325580989940;
    abscissa[47] = 0.022838332222257;
    abscissa[48] = 0.695836086787803;
    abscissa[49] = 0.022838332222257;
    abscissa[50] = 0.281325580989940;
    abscissa[51] = 0.695836086787803;
    abscissa[52] = 0.695836086787803;
    abscissa[53] = 0.281325580989940;
    abscissa[54] = 0.025734050548330;
    abscissa[55] = 0.116251915907597;
    abscissa[56] = 0.025734050548330;
    abscissa[57] = 0.858014033544073;
    abscissa[58] = 0.116251915907597;
    abscissa[59] = 0.025734050548330;
    abscissa[60] = 0.858014033544073;
    abscissa[61] = 0.025734050548330;
    abscissa[62] = 0.116251915907597;
    abscissa[63] = 0.858014033544073;
    abscissa[64] = 0.858014033544073;
    abscissa[65] = 0.116251915907597;
  }
}

void generate_gauss_legendre_points(
  double ** vertices, unsigned * num_vertices, double * triangle,
  unsigned gauss_order) {

  static size_t const num_gauss_points[] = {1,1,3,4,6,7,12,13,16,19,25,27,33};

  if (gauss_order > 12) abort_message("internal error", __FILE__, __LINE__);

  size_t n = num_gauss_points[gauss_order];
  double * v = malloc(3 * n * sizeof(*v));

  *num_vertices = n;
  *vertices = v;

  double ab[3] = {triangle[3]-triangle[0],
                  triangle[4]-triangle[1],
                  triangle[5]-triangle[2]};
  double ac[3] = {triangle[6]-triangle[0],
                  triangle[7]-triangle[1],
                  triangle[8]-triangle[2]};
  double a[3] = {triangle[0], triangle[1], triangle[2]};

  static double * abscissa = NULL;
  static unsigned prev_order = -1;

  if (prev_order != gauss_order) {
    abscissa = realloc(abscissa, 2 * n * sizeof(*abscissa));
    prev_order = gauss_order;
    get_triangle_gauss_legendre(gauss_order, abscissa);
  }

  for (size_t i = 0; i < n; ++i) {

    double * curr_v = v + 3 * i;
    double * curr_abscissa = abscissa + 2 * i;

    curr_v[0] = ab[0]*curr_abscissa[0] + ac[0]*curr_abscissa[1] + a[0];
    curr_v[1] = ab[1]*curr_abscissa[0] + ac[1]*curr_abscissa[1] + a[1];
    curr_v[2] = ab[2]*curr_abscissa[0] + ac[2]*curr_abscissa[1] + a[2];

    normalise_vector(curr_v);
  }
}
