#include <stdlib.h>

#include "bnd_triangle.h"
#include "vertex_generator.h"
#include "utils.h"
#include "g_patch.h"
#include "test_function.h"
#include "geometry.h"

#ifdef DEBUG
#include <stdio.h>
#include "debug_output.h"
#endif // DEBUG

#define NUM_VERTICES (20)
#define NUM_TESTS (16)
#define DEGREE (4)

int compare_vertices (const void * a, const void * b);

int main(void) {

  // generate the reference locations
  double vertices[NUM_VERTICES][3] =
// {{0.70021450728255308, 0.13928132531715343, -0.70021450728255319},
 // {0.5252893005325312, 0.32563440388875486, -0.78615099424351009},
 // {0.67859834454584689, 0.28108463771482023, -0.678598344545847},
 // {0.38268343236508973, 0, -0.92387953251128674},
 // {0.55557023301960218, 0, -0.83146961230254524},
 // {0.54812419193877182, 0.16317295173655549, -0.82032582431193046},
 // {0.37638067707860257, 0.18074437515937392, -0.90866333521833065},
 // {0.35740674433659325, 0.35740674433659325, -0.86285620946101682},
 // {0.37638067707860257, -0.18074437515937392, -0.90866333521833065},
 // {0.54812419193877182, -0.16317295173655549, -0.82032582431193046},
 // {0.70021450728255308, -0.13928132531715343, -0.70021450728255319},
 // {0.70710678118654746, 0, -0.70710678118654757}};
// {{-0.20389698403004949, -0.54389900760103693, -0.81400238908375255},
 // {-0.23672499337642122, -0.5787675624669899, -0.78037772017591089},
 // {-0.22829759966654589, -0.61763984301851604, -0.75259632626166617},
 // {-0.19724059881423786, -0.58399685854258065, -0.78742860971125372},
 // {-0.15776703584388682, -0.58823898110874528, -0.79314844985360644},
 // {-0.15197529260603759, -0.62702435758406083, -0.76403139034571421},
 // {-0.11830782612370447, -0.59151568900445906, -0.79756657900106598},
 // {-0.11391880532947772, -0.63026342401416491, -0.76797820421040963},
 // {-0.16317295173655549, -0.54812419193877182, -0.82032582431193046},
 // {-0.12240933579037667, -0.55139217808406427, -0.82521671120928619},
 // {-0.078862651158381869, -0.59384399367298113, -0.80070593442961047},
 // {-0.19009822778951455, -0.62282517062692655, -0.75891466623384307}};
// {{-0.20389698403004949, -0.54389900760103693, -0.81400238908375255},
// {-0.23672499337642122, -0.5787675624669899, -0.78037772017591089},
// {-0.22829759966654589, -0.61763984301851604, -0.75259632626166617},
// {-0.19724059881423786, -0.58399685854258065, -0.78742860971125372},
// {-0.15776703584388682, -0.58823898110874528, -0.79314844985360644},
// {-0.15197529260603759, -0.62702435758406083, -0.76403139034571421},
// {-0.11830782612370447, -0.59151568900445906, -0.79756657900106598},
// {-0.11391880532947772, -0.63026342401416491, -0.76797820421040963},
// {-0.16317295173655549, -0.54812419193877182, -0.82032582431193046},
// {-0.12240933579037667, -0.55139217808406427, -0.82521671120928619},
// {-0.078862651158381869, -0.59384399367298113, -0.80070593442961047},
// {-0.19009822778951455, -0.62282517062692655, -0.75891466623384307}};
// {{0.43798504325645155, 0.64618045933832546, -0.62499593266912945},
 // {0.74989895820459995, 0.21686778606031118, -0.62499593266912923},
 // {0.81620260248056209, 0.28169600570047321, -0.50444095003910672},
 // {0.64108222512042967, 0.29592777040456875, -0.70812451965598511},
 // {0.6296487506034949, 0.61122825803876113, -0.47952316673791634},
 // {0.55932369171472396, 0.57176798703526255, -0.60019861453376921},
 // {0.52012929685530496, 0.68920595069721324, -0.50444095003910661},
 // {0.77588478217493217, 0.40995162806238294, -0.47952316673791662},
 // {0.71662419597523308, 0.35526241689009919, -0.60019861453376921},
 // {0.72360679774997894, 0.52573111211913381, -0.44721359549995776},
 // {0.67006739565385953, 0.48683245957232352, -0.56036045683864599},
 // {0.80086487859527511, 0.48683245957232346, -0.3487256837965011},
 // {0.60196194589781882, 0.43735095393562773, -0.66810624812413477},
 // {0.51967084108376294, 0.37756296661166927, -0.76641269768362397},
 // {0.47954933674987932, 0.51825871751928199, -0.70812451965598522}};
// {{0.43798504325645155, 0.64618045933832546, -0.62499593266912945},
 // {0.74989895820459995, 0.21686778606031118, -0.62499593266912923},
 // {0.81620260248056209, 0.28169600570047321, -0.50444095003910672},
 // {0.64108222512042967, 0.29592777040456875, -0.70812451965598511},
 // {0.6296487506034949, 0.61122825803876113, -0.47952316673791634},
 // {0.55932369171472396, 0.57176798703526255, -0.60019861453376921},
 // {0.52012929685530496, 0.68920595069721324, -0.50444095003910661},
 // {0.77588478217493217, 0.40995162806238294, -0.47952316673791662},
 // {0.71662419597523308, 0.35526241689009919, -0.60019861453376921},
 // {0.72360679774997894, 0.52573111211913381, -0.44721359549995776},
 // {0.67006739565385953, 0.48683245957232352, -0.56036045683864599},
 // {0.80086487859527511, 0.48683245957232346, -0.3487256837965011},
 // {0.60196194589781882, 0.43735095393562773, -0.66810624812413477},
 // {0.51967084108376294, 0.37756296661166927, -0.76641269768362397},
 // {0.47954933674987932, 0.51825871751928199, -0.70812451965598522}};

{{-0.47920931490801955, 0.61622927279741524, -0.62499593266912978},
 {0.070124593327559326, 0.91762020425638635, -0.39122334049889235},
 {-0.27639320225002101, 0.8506508083520401, -0.44721359549995776},
 {-0.25594297038665054, 0.78771146641472856, -0.56036045683864599},
 {-0.37094314378246596, 0.70863446655700046, -0.60019861453376933},
 {-0.49474501847939995, 0.70764870848003492, -0.50444095003910694},
 {-0.38674045332806672, 0.78771146641472822, -0.47952316673791678},
 {-0.22992900339894112, 0.70764870848003525, -0.66810624812413477},
 {-0.07294879135335923, 0.92514003670216449, -0.37255118618896094},
 {-0.015688346816198035, 0.86330365671518972, -0.50444095003910683},
 {-0.15012558386557776, 0.86459229792466907, -0.47952316673791651},
 {-0.11642558144146434, 0.79133223559877885, -0.6001986145337691},
 {-0.21552432533628554, 0.91210726488116622, -0.34872568379650071},
 {-0.5704074648958235, 0.78021212218853819, -0.25671846131740805},
 {-0.48476685795486985, 0.79133223559877852, -0.37255118618896127},
 {-0.59609561099313813, 0.70115213784428043, -0.39122334049889257},
 {-0.30059621969816702, 0.92514003670216449, -0.23185733802036332},
 {-0.14809635435642721, 0.95950370524162099, -0.23962493495864789},
 {-0.36176035690772351, 0.86459229792466896, -0.34872568379650082},
 {-0.44416965998168489, 0.86330365671518972, -0.23962493495864809}};

  // sort vertices to ensure bit-reproducibility
  qsort(&vertices[0][0], NUM_VERTICES, 3*sizeof(vertices[0][0]),
        compare_vertices);

  double triangle[3][3];
  // compute the bounding triangle for the given vertices
  compute_bnd_triangle(
    &vertices[0][0], NUM_VERTICES, &triangle[0][0], NUM_TESTS);

  unsigned const degree = DEGREE;
  unsigned num_g_coeffs = get_num_g_coefficients(degree);
  double * g_polynomials =
    malloc(NUM_VERTICES * num_g_coeffs * sizeof(*g_polynomials));

  // compute spherical Bernstein basis polynomials
  compute_g_polynomials(
    degree, &triangle[0][0], &vertices[0][0], NUM_VERTICES, g_polynomials);

  double * f_v = malloc(NUM_VERTICES * sizeof(*f_v));

  // compute field data at the reference locations
  compute_field_data(&vertices[0][0], NUM_VERTICES, f_v);

#ifdef DEBUG
    print_field(&vertices[0][0], NUM_VERTICES, f_v, 1, "field_data_g.vtk",
                "field_data", (char const *[]){"f_v"});
#endif

  double * g_coeffs = malloc(num_g_coeffs * sizeof(*g_coeffs));

  // compute coefficients of spherical Bernstein-Bézier polynomials (g-Patch)
  compute_g_patch_coefficients(
    degree, g_polynomials, f_v, NUM_VERTICES, g_coeffs);

  // use the g-Patch to interpolate a number of points
  {
    unsigned num_test_vertices = 1024 * 64;
    double * test_vertices =
      malloc(3 * num_test_vertices * sizeof(*test_vertices));

    // generate test vertices
    generate_vertices_2(
      test_vertices, num_test_vertices, 0.0, 2.0 * M_PI, -M_PI, M_PI);

    double * field_data_test =
      malloc(4 * num_test_vertices * sizeof(*field_data_test));
    double * field_data_interp = field_data_test + 0 * num_test_vertices;
    double * field_data_ref = field_data_test + 1 * num_test_vertices;
    double * field_data_diff = field_data_test + 2 * num_test_vertices;
    double * field_data_err = field_data_test + 3 * num_test_vertices;

    // evaluate g-Patch at the locations of the test vertices
    evaluate_g_patch(test_vertices, num_test_vertices, &triangle[0][0],
                     degree, g_coeffs, field_data_interp);

    // compute the reference data
    compute_field_data(test_vertices, num_test_vertices, field_data_ref);

    double field_avg = 0.0;
    for (unsigned i = 0; i < NUM_VERTICES; ++i)
      field_avg += f_v[i];
    field_avg /= (double)NUM_VERTICES;
    field_avg = fabs(field_avg);

    // compute the difference between field_data_ref and field_data_diff
    for (unsigned i = 0; i < num_test_vertices; ++i) {
      field_data_diff[i] = field_data_ref[i] - field_data_interp[i];
      field_data_err[i] =
        (field_data_ref[i] - field_data_interp[i]) / (field_avg + 1e-13);
    }

#ifdef DEBUG
    print_field(test_vertices, num_test_vertices, field_data_test, 4,
                "field_data_test_g.vtk", "field_data_test",
                (char const *[]){"f_interp", "f_ref", "f_diff", "f_err"});
#endif // DEBUG

    free(test_vertices);
    free(field_data_test);
  }

  // compute derivatives on the g-Patch
  {
    unsigned num_test_vertices = 1024 * 64;
    double * test_vertices =
      malloc(3 * num_test_vertices * sizeof(*test_vertices));

    // generate test vertices
    generate_vertices_2(
      test_vertices, num_test_vertices, 0.0, 2.0 * M_PI, -M_PI, M_PI);

    double * field_data_test =
      malloc(6 * num_test_vertices * sizeof(*field_data_test));
    double * d_field_data_g = field_data_test + 0 * num_test_vertices;
    double * d_field_data_g_2 = field_data_test + 1 * num_test_vertices;
    double * d_field_data_ref = field_data_test + 2 * num_test_vertices;
    double * d_field_data_ref_2 = field_data_test + 3 * num_test_vertices;
    double * d_field_data_diff = field_data_test + 4 * num_test_vertices;
    double * d_field_data_diff_2 = field_data_test + 5 * num_test_vertices;

    // double e[4][3] = {{1,0,0}, {0,1,0}, {0,0,1}};
    double e[3][3] =
{{0.025144396169229644, 0.11748437695806507, 0.099561819947594138},
 {-0.093121157519238218, 0.00060566067738831997, -0.11082615745641705},
 {0.080181627683723583, -0.11036054269782802, -5.5511151231257827e-17}};

    for (unsigned i = 0; i < 3; ++i) {

      double * direction = &e[i][0];

      // for (unsigned j = 0; j < num_test_vertices; ++j)
        // d_field_data_g[j] =
          // compute_directional_derivative(
            // degree, &triangle[0][0], g_coeffs, test_vertices+3*j, direction);

for (unsigned j = 0; j < num_test_vertices; ++j)
  d_field_data_g[j] =
    compute_directional_derivative_g_2(
      degree, &triangle[0][0], g_coeffs, test_vertices+3*j, direction);

      for (unsigned j = 0; j < num_test_vertices; ++j)
        d_field_data_g_2[j] =
          compute_directional_derivative_g_2(
            degree, &triangle[0][0], g_coeffs, test_vertices+3*j, direction);

      compute_field_data_derivative(
        test_vertices, num_test_vertices, direction, d_field_data_ref);

      compute_field_data_derivative_2(
        test_vertices, num_test_vertices, direction, d_field_data_ref_2);

      for (unsigned j = 0; j < num_test_vertices; ++j)
        d_field_data_diff[j] = d_field_data_g[j] - d_field_data_ref[j];

      for (unsigned j = 0; j < num_test_vertices; ++j)
        d_field_data_diff_2[j] = d_field_data_g_2[j] - d_field_data_ref_2[j];

#ifdef DEBUG
    {
      char filename[1024];
      sprintf(&filename[0], "d_field_data_test_g_%d.vtk", i);
      print_field(
        test_vertices, num_test_vertices, field_data_test, 6, filename,
        "d_field_data_test",
        (char const *[]){"df_interp", "df_interp_2", "df_ref", "df_ref_2", "df_diff", "df_diff_2"});
    }
#endif // DEBUG
    }

    free(test_vertices);
    free(field_data_test);
  }

  free(g_coeffs);
  free(f_v);
  free(g_polynomials);

  return EXIT_SUCCESS;
}

int compare_vertices (const void * a, const void * b)
{
  if (((double*)a)[0] != ((double*)b)[0])
    return (((double*)a)[0] > ((double*)b)[0])?-1:1;
  else if (((double*)a)[1] != ((double*)b)[1])
    return (((double*)a)[1] > ((double*)b)[1])?-1:1;
  else if (((double*)a)[2] != ((double*)b)[2])
    return (((double*)a)[2] > ((double*)b)[2])?-1:1;
  else return 0;
}
