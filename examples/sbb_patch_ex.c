#include <stdlib.h>

#include "bnd_triangle.h"
#include "vertex_generator.h"
#include "utils.h"
#include "sbb_patch.h"
#include "test_function.h"
#include "geometry.h"

#ifdef DEBUG
#include <stdio.h>
#include "debug_output.h"
#endif // DEBUG

#define NUM_VERTICES (12)
#define NUM_TESTS (16)
#define DEGREE (3)

int compare_vertices (const void * a, const void * b);

int main(void) {

  // generate the reference locations
  double vertices[NUM_VERTICES][3] =
// {{0.70021450728255308, 0.13928132531715343, -0.70021450728255319},
 // {0.5252893005325312, 0.32563440388875486, -0.78615099424351009},
 // {0.67859834454584689, 0.28108463771482023, -0.678598344545847},
 // {0.38268343236508973, 0, -0.92387953251128674},
 // {0.55557023301960218, 0, -0.83146961230254524},
 // {0.54812419193877182, 0.16317295173655549, -0.82032582431193046},
 // {0.37638067707860257, 0.18074437515937392, -0.90866333521833065},
 // {0.35740674433659325, 0.35740674433659325, -0.86285620946101682},
 // {0.37638067707860257, -0.18074437515937392, -0.90866333521833065},
 // {0.54812419193877182, -0.16317295173655549, -0.82032582431193046},
 // {0.70021450728255308, -0.13928132531715343, -0.70021450728255319},
 // {0.70710678118654746, 0, -0.70710678118654757}};
{{-0.20389698403004949, -0.54389900760103693, -0.81400238908375255},
 {-0.23672499337642122, -0.5787675624669899, -0.78037772017591089},
 {-0.22829759966654589, -0.61763984301851604, -0.75259632626166617},
 {-0.19724059881423786, -0.58399685854258065, -0.78742860971125372},
 {-0.15776703584388682, -0.58823898110874528, -0.79314844985360644},
 {-0.15197529260603759, -0.62702435758406083, -0.76403139034571421},
 {-0.11830782612370447, -0.59151568900445906, -0.79756657900106598},
 {-0.11391880532947772, -0.63026342401416491, -0.76797820421040963},
 {-0.16317295173655549, -0.54812419193877182, -0.82032582431193046},
 {-0.12240933579037667, -0.55139217808406427, -0.82521671120928619},
 {-0.078862651158381869, -0.59384399367298113, -0.80070593442961047},
 {-0.19009822778951455, -0.62282517062692655, -0.75891466623384307}};

  // sort vertices to ensure bit-reproducibility
  qsort(&vertices[0][0], NUM_VERTICES, 3*sizeof(vertices[0][0]),
        compare_vertices);

  double triangle[3][3];
  // compute the bounding triangle for the given vertices
  compute_bnd_triangle(
    &vertices[0][0], NUM_VERTICES, &triangle[0][0], NUM_TESTS);

  unsigned const degree = DEGREE;
  unsigned num_sbb_coeffs = get_num_sbb_coefficients(degree);
  double * sb_polynomials =
    malloc(NUM_VERTICES * num_sbb_coeffs * sizeof(*sb_polynomials));

  // compute spherical Bernstein basis polynomials
  compute_sb_polynomials(
    degree, &triangle[0][0], &vertices[0][0], NUM_VERTICES, sb_polynomials);

  double * f_v = malloc(NUM_VERTICES * sizeof(*f_v));

  // compute field data at the reference locations
  compute_field_data(&vertices[0][0], NUM_VERTICES, f_v);

#ifdef DEBUG
    print_field(&vertices[0][0], NUM_VERTICES, f_v, 1, "field_data.vtk",
                "field_data", (char const *[]){"f_v"});
#endif

  double * sbb_coeffs = malloc(num_sbb_coeffs * sizeof(*sbb_coeffs));

  // compute coefficients of spherical Bernstein-Bézier polynomials (SBB-Patch)
  compute_sbb_patch_coefficients(
    degree, sb_polynomials, f_v, NUM_VERTICES, sbb_coeffs);

  // use the SBB-Patch to interpolate a number of points
  {
    unsigned num_test_vertices = 1024 * 64;
    double * test_vertices =
      malloc(3 * num_test_vertices * sizeof(*test_vertices));

    // generate test vertices
    generate_vertices_2(
      test_vertices, num_test_vertices, 0.0, 2.0 * M_PI, -M_PI, M_PI);

    double * field_data_test =
      malloc(3 * num_test_vertices * sizeof(*field_data_test));
    double * field_data_interp = field_data_test + 0 * num_test_vertices;
    double * field_data_ref = field_data_test + 1 * num_test_vertices;
    double * field_data_diff = field_data_test + 2 * num_test_vertices;

    // evaluate SBB-Patch at the locations of the test vertices
    evaluate_sbb_patch(test_vertices, num_test_vertices, &triangle[0][0],
                       degree, sbb_coeffs, field_data_interp);

    // compute the reference data
    compute_field_data(test_vertices, num_test_vertices, field_data_ref);

    // compute the difference between field_data_ref and field_data_diff
    for (unsigned i = 0; i < num_test_vertices; ++i)
      field_data_diff[i] = field_data_ref[i] - field_data_interp[i];

#ifdef DEBUG
    print_field(test_vertices, num_test_vertices, field_data_test, 3,
                "field_data_test.vtk", "field_data_test",
                (char const *[]){"f_interp", "f_ref", "f_diff"});
#endif // DEBUG

    free(test_vertices);
    free(field_data_test);
  }

  // compute derivatives on the SBB-Patch
  {
    unsigned num_test_vertices = 1024 * 64;
    double * test_vertices =
      malloc(3 * num_test_vertices * sizeof(*test_vertices));

    // generate test vertices
    generate_vertices_2(
      test_vertices, num_test_vertices, 0.0, 2.0 * M_PI, -M_PI, M_PI);

    double * field_data_test =
      malloc(6 * num_test_vertices * sizeof(*field_data_test));
    double * d_field_data_sbb = field_data_test + 0 * num_test_vertices;
    double * d_field_data_sbb_2 = field_data_test + 1 * num_test_vertices;
    double * d_field_data_ref = field_data_test + 2 * num_test_vertices;
    double * d_field_data_ref_2 = field_data_test + 3 * num_test_vertices;
    double * d_field_data_diff = field_data_test + 4 * num_test_vertices;
    double * d_field_data_diff_2 = field_data_test + 5 * num_test_vertices;

    double e[4][3] = {{1,0,0}, {0,1,0}, {0,0,1}};

    crossproduct_d(&triangle[0][0], &triangle[1][0], &e[3][0]);
    normalise_vector(&e[3][0]);

    for (unsigned i = 0; i < 4; ++i) {

      double * direction = &e[i][0];

      for (unsigned j = 0; j < num_test_vertices; ++j)
        d_field_data_sbb[j] =
          compute_directional_derivative(
            degree, &triangle[0][0], sbb_coeffs, test_vertices+3*j, direction);

      for (unsigned j = 0; j < num_test_vertices; ++j)
        d_field_data_sbb_2[j] =
          compute_directional_derivative_2(
            degree, &triangle[0][0], sbb_coeffs, test_vertices+3*j, direction);

      compute_field_data_derivative(
        test_vertices, num_test_vertices, direction, d_field_data_ref);

      compute_field_data_derivative_2(
        test_vertices, num_test_vertices, direction, d_field_data_ref_2);

      for (unsigned j = 0; j < num_test_vertices; ++j)
        d_field_data_diff[j] = d_field_data_sbb[j] - d_field_data_ref[j];

      for (unsigned j = 0; j < num_test_vertices; ++j)
        d_field_data_diff_2[j] = d_field_data_sbb_2[j] - d_field_data_ref_2[j];

#ifdef DEBUG
    {
      char filename[1024];
      sprintf(&filename[0], "d_field_data_test_%d.vtk", i);
      print_field(
        test_vertices, num_test_vertices, field_data_test, 6, filename,
        "d_field_data_test",
        (char const *[]){"df_interp", "df_interp_2", "df_ref", "df_ref_2", "df_diff", "df_diff_2"});
    }
#endif // DEBUG
    }

    free(test_vertices);
    free(field_data_test);
  }

  free(sbb_coeffs);
  free(f_v);
  free(sb_polynomials);

  return EXIT_SUCCESS;
}

int compare_vertices (const void * a, const void * b)
{
  if (((double*)a)[0] != ((double*)b)[0])
    return (((double*)a)[0] > ((double*)b)[0])?-1:1;
  else if (((double*)a)[1] != ((double*)b)[1])
    return (((double*)a)[1] > ((double*)b)[1])?-1:1;
  else if (((double*)a)[2] != ((double*)b)[2])
    return (((double*)a)[2] > ((double*)b)[2])?-1:1;
  else return 0;
}
