include Make.include

SRCDIR=src
EXAMPLESDIR=examples
BUILDDIR=build
BINDIR=bin

SRC=$(wildcard $(SRCDIR)/*.c)
OBJ=$(patsubst $(SRCDIR)/%.c, $(BUILDDIR)/%.o, $(SRC))
EXEC_SRC=$(wildcard $(EXAMPLESDIR)/*.c)
EXEC_OBJ=$(patsubst $(EXAMPLESDIR)/%.c, $(BUILDDIR)/%.o, $(EXEC_SRC))
EXEC=$(patsubst $(EXAMPLESDIR)/%.c, $(BINDIR)/%, $(EXEC_SRC))

all: $(EXEC)

debug_flags:
	$(eval CFLAGS += $(DEBUG_CFLAGS))
	$(eval F90FLAGS += $(DEBUG_FFLAGS))

debug: debug_flags all

$(EXEC): $(OBJ) $(EXEC_OBJ) | $(BINDIR)
	$(CC) $(LDFLAGS) -o $@ $(patsubst $(BINDIR)/%, $(BUILDDIR)/%.o, $@) $(OBJ) $(LIBS)

$(OBJ) $(EXEC_OBJ): | $(BUILDDIR)

$(BINDIR) $(BUILDDIR):
	mkdir -p $@

clean:
	$(RM) -rf $(EXEC) core* $(BINDIR)/core* $(BUILDDIR)/*.o $(BUILDDIR)/*.mod

$(BUILDDIR)/%.o : $(SRCDIR)/%.c
	$(CC) $(INCLUDES) $(CFLAGS) -c $< -o $@

$(BUILDDIR)/%.o : $(EXAMPLESDIR)/%.c
	$(CC) $(INCLUDES) $(CFLAGS) -c $< -o $@

