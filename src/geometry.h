/**
 * @file geometry.h
 * @brief Structs and interfaces for investigating the geometry and relations of cells
 *
 * The functions are used to determine relations between
 * source and target cells. Complete cells are constructed
 * on the sphere by connecting cell vertices along either
 * great circles (along the orthodrome) or directly (along
 * the loxodrome).
 *
 * @copyright Copyright  (C)  2013 Moritz Hanke <hanke@dkrz.de>
 *                                 Rene Redler <rene.redler@mpimet.mpg.de>
 *
 * @version 1.0
 * @author Moritz Hanke <hanke@dkrz.de>
 *         Rene Redler <rene.redler@mpimet.mpg.de>
 */
/*
 * Keywords:
 * Maintainer: Moritz Hanke <hanke@dkrz.de>
 *             Rene Redler <rene.redler@mpimet.mpg.de>
 * URL: https://doc.redmine.dkrz.de/YAC/html/index.html
 *
 * This file is part of YAC.
 *
 * YAC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * YAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with YAC.  If not, see <http://www.gnu.org/licenses/gpl.txt>.
 */

#ifndef GEOMETRY_H
#define GEOMETRY_H

#include <math.h>

#include "utils.h"

#ifndef  M_PI
#define  M_PI        3.14159265358979323846264338327950288  /* pi */
#endif

/**
 * defines a spherical cap, which is used as a convex hull to describe the extents of subdomain
 */
struct reduced_bounding_circle {

   //! the middle point of the spherical cap in cartesian coordinates (is a unit vector)
   double base_vector[3];
   //! cosine of angle between the middle point and the boundary of the spherical cap
   double cos_inc_angle;
};

/**
 * converts lon-lat coordinates into xyz ones
 *
 * Further information:
 * http://en.wikipedia.org/wiki/List_of_common_coordinate_transformations
 *
 * @param[in]  lon   longitude coordinates in radian
 * @param[in]  lat   latitude coordinates in radian
 * @param[out] p_out xyz coordinates
 */
static inline void LLtoXYZ(double lon, double lat, double p_out[]) {

   double cos_lat = cos(lat);
   p_out[0] = cos_lat * cos(lon);
   p_out[1] = cos_lat * sin(lon);
   p_out[2] = sin(lat);
}

static inline void XYZtoLL (double p_in[], double * lon, double * lat) {

   *lon = atan2(p_in[1] , p_in[0]);
   *lat = 0.5 * M_PI - acos(p_in[2]);
}

static inline void crossproduct_ld (double a[], double b[], double cross[]) {

/* crossproduct in Cartesian coordinates */

   long double a_[3] = {a[0], a[1], a[2]};
   long double b_[3] = {b[0], b[1], b[2]};

   cross[0] = a_[1] * b_[2] - a_[2] * b_[1];
   cross[1] = a_[2] * b_[0] - a_[0] * b_[2];
   cross[2] = a_[0] * b_[1] - a_[1] * b_[0];
}

/**
 * for small angles <= 1e-?8? the crossproduct is inaccurate\n
 * use \ref crossproduct_ld for these cases
 */
static inline void crossproduct_d (double a[], double b[], double cross[]) {

/* crossproduct in Cartesian coordinates */

   cross[0] = a[1] * b[2] - a[2] * b[1];
   cross[1] = a[2] * b[0] - a[0] * b[2];
   cross[2] = a[0] * b[1] - a[1] * b[0];
}

static void normalise_vector(double v[]) {

   double norm = 1.0 / sqrt(v[0]*v[0] + v[1]*v[1] + v[2]*v[2]);

   v[0] *= norm;
   v[1] *= norm;
   v[2] *= norm;
}

static inline double get_vector_angle(double a[3], double b[3]) {

  double dot_product = a[0]*b[0] + a[1]*b[1] + a[2]*b[2];

  // the acos most accurate in the range [-0.5;0.5]
  if (fabs(dot_product) <= 0.5) // the range in which the acos is most accurate

    return acos(dot_product);

  else {

    double cross_ab[3];

    crossproduct_ld(a, b, cross_ab);

    double asin_tmp = asin(sqrt(cross_ab[0]*cross_ab[0]+
                                cross_ab[1]*cross_ab[1]+
                                cross_ab[2]*cross_ab[2]));

    if (dot_product < 0.0) // if the angle is bigger than (PI / 2)
      return MIN(M_PI - asin_tmp, M_PI);
    else
      return MAX(asin_tmp,0.0);
  }

  /*
  // this solution is simpler, but has a much worse performance
  double cross[3], dot, cross_abs;

  crossproduct_ld(a, b, cross);
  dot = a[0]*b[0] + a[1]*b[1] + a[2]*b[2];
  cross_abs = sqrt(cross[0]*cross[0] + cross[1]*cross[1] + cross[2]*cross[2]);

  return fabs(atan2(cross_abs, dot));
  */
}

#endif // GEOMETRY_H
