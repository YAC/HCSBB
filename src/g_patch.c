#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "g_patch.h"
#include "patch_common.h"
#include "utils.h"

unsigned get_num_g_coefficients(unsigned degree) {

  unsigned num_coeff = 0;

  for (unsigned i = 0; i <= degree; ++i)
    num_coeff += ((i + 2) * (i + 1)) / 2;

  return num_coeff;
}

static inline double pow_uint(double x, unsigned y) {

  double pow;

  switch(y) {
    case(0): pow = 1.0; break;
    case(1): pow = x; break;
    case(2): pow = x*x; break;
    case(3): pow = x*x*x; break;
    case(4): pow = x*x*x*x; break;
    case(5): pow = x*x*x*x*x; break;
    case(6): pow = x*x*x*x*x*x; break;
    case(7): pow = x*x*x*x*x*x*x; break;
    default: pow = 0.0;
  }
  return pow;
}

void compute_g_polynomials(
  unsigned degree, double * triangle, double * vertices, unsigned num_vertices,
  double * restrict g_polynomials) {

  double * restrict sb_coords = malloc(3 * num_vertices * sizeof(*sb_coords));

  // compute spherical barycentric coordinates for all vertices
  compute_sb_coords(vertices, num_vertices, triangle, sb_coords);

  size_t sb_poly_idx = 0;

  for (unsigned i = 0; i <= degree; ++i)
    for (unsigned j = 0; j <= degree - i; ++j)
      for (unsigned k = 0; k <= degree - i - j; ++k)
        for (size_t vertex_idx = 0; vertex_idx < (size_t)num_vertices;
             ++vertex_idx, ++sb_poly_idx)
          g_polynomials[sb_poly_idx] = (pow_uint(sb_coords[0+3*vertex_idx], i) *
                                        pow_uint(sb_coords[1+3*vertex_idx], j) *
                                        pow_uint(sb_coords[2+3*vertex_idx], k));

  free(sb_coords);
}

void compute_g_patch_coefficients(
  unsigned degree, double * g_polynomials, double * f_v, unsigned num_vertices,
  double * g_coefficients) {

  compute_patch_coefficients(
    get_num_g_coefficients(degree), g_polynomials, f_v, num_vertices,
    g_coefficients);
}

void evaluate_g_patch(
  double * vertices, unsigned num_vertices, double * triangle,
  unsigned degree, double * restrict g_coefficients, double * restrict p) {

  unsigned num_g_coeffs = get_num_g_coefficients(degree);
  double * restrict g_polynomials =
    malloc(num_vertices * num_g_coeffs * sizeof(*g_polynomials));

  // compute the spherical Bernstein base polynomials for the given vertices
  compute_g_polynomials(
    degree, triangle, vertices, num_vertices, g_polynomials);

  evaluate_patch(vertices, (size_t)num_vertices, num_g_coeffs, g_coefficients,
                 g_polynomials, p);

  free(g_polynomials);
}

double compute_directional_derivative_g_2(
  unsigned degree, double * triangle, double * coefficients, double * vertex,
  double * direction) {

  double dx = direction[0];
  double dy = direction[1];
  double dz = direction[2];

  double scale = 1.0e-6;

  double vertices[2][3];

  vertices[0][0] = vertex[0];
  vertices[0][1] = vertex[1];
  vertices[0][2] = vertex[2];
  vertices[1][0] = vertex[0] + scale * dx;
  vertices[1][1] = vertex[1] + scale * dy;
  vertices[1][2] = vertex[2] + scale * dz;

  double f[2];

  evaluate_g_patch(&vertices[0][0], 2, triangle, degree, coefficients, &f[0]);

  return (f[1] - f[0]) / scale;
}
