#ifndef BND_TRIANGLE_H
#define BND_TRIANGLE_H

void compute_bnd_triangle(
  double * vertices, unsigned num_vertices, double * triangle,
  unsigned num_tests);
#endif // BND_TRIANGLE_H
