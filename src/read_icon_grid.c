#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <math.h>

#include "utils.h"
#include "read_icon_grid.h"

#include <netcdf.h>

static void open_icon_file    ( int *ncid, const char *filename);

static void close_icon_file   ( int ncid );

static void get_icon_vertices ( int ncid,
                                int *vertex_type,  // data type for vertex coordinates
                                void **vertex_lon, // longitude of vertex coordinates
                                void **vertex_lat, // latitude of vertex coordinates
                                size_t *nbr_vertices );

static void get_icon_cell_center ( int ncid,
                                   int *cell_type,  // data type for cell coordinates
                                   void **cell_lon, // longitude of cell coordinates
                                   void **cell_lat, // latitude of cell coordinates
                                   size_t *nbr_cells );

static void get_icon_edge_middle ( int ncid,
                                   int *edge_type,  // data type for edge coordinates
                                   void **edge_lon, // longitude of edge coordinates
                                   void **edge_lat, // latitude of edge coordinates
                                   size_t *nbr_edges );

static void get_icon_cell_mask ( int ncid,
                                 int *cell_type,  // data type for cell mask
                                 int **cell_mask, // integer cell mask
                                 size_t *nbr_cells );

static void get_icon_connect_cell ( int ncid,
                                    int **vertex_of_cell,
                                    size_t **dimlen );

static void get_icon_connect_vertex ( int ncid,
                                      int **cells_of_vertex,
                                      size_t **dimlen );

static void get_icon_connect_edge ( int ncid,
                                    int **edge_vertices,
                                    size_t **dimlen );

static void handle_error(int status);

void read_icon_grid_information(const char * filename, int * nbr_vertices,
                                int * nbr_cells, int * nbr_edges,
                                int ** num_vertices_per_cell,
                                int ** cell_to_vertex, int ** vertex_to_cell,
                                int ** edge_to_vertex,
                                double ** x_vertices, double ** y_vertices,
                                double ** x_cells, double ** y_cells,
                                double ** x_edges, double ** y_edges,
                                int ** cell_mask) {

   int ncid;

   int * vertex_of_cell, * edge_vertices, * cells_of_vertex;

   /* Open file */

   open_icon_file ( &ncid, filename );

   /* Get vertex longitudes and latitudes of cells */

   {
      void * temp_vertex_lon;
      void * temp_vertex_lat;
      size_t temp_nbr_vertices;
      int vertex_type;

      get_icon_vertices ( ncid, &vertex_type, &temp_vertex_lon, &temp_vertex_lat, &temp_nbr_vertices );

      *nbr_vertices = temp_nbr_vertices;

      if (vertex_type == NC_FLOAT) {

         *x_vertices = malloc(temp_nbr_vertices * sizeof(**x_vertices));
         *y_vertices = malloc(temp_nbr_vertices * sizeof(**y_vertices));

         for (int i = 0; i < temp_nbr_vertices; ++i) {

            (*x_vertices)[i] = ((float*)temp_vertex_lon)[i];
            (*y_vertices)[i] = ((float*)temp_vertex_lat)[i];
         }
         free(temp_vertex_lon);
         free(temp_vertex_lat);
      } else {
         *x_vertices = temp_vertex_lon;
         *y_vertices = temp_vertex_lat;
      }
   }

   /* Get cell center longitudes and latitudes of cells */

   {
      void * temp_cell_lon;
      void * temp_cell_lat;
      size_t temp_nbr_cells;
      int cell_type;

      get_icon_cell_center ( ncid, &cell_type, &temp_cell_lon, &temp_cell_lat, &temp_nbr_cells );

      *nbr_cells = temp_nbr_cells;

      if (cell_type == NC_FLOAT) {

         *x_cells = malloc(temp_nbr_cells * sizeof(**x_cells));
         *y_cells = malloc(temp_nbr_cells * sizeof(**y_cells));

         for (int i = 0; i < temp_nbr_cells; ++i) {

            (*x_cells)[i] = ((float*)temp_cell_lon)[i];
            (*y_cells)[i] = ((float*)temp_cell_lat)[i];
         }
         free(temp_cell_lon);
         free(temp_cell_lat);
      } else {
         *x_cells = temp_cell_lon;
         *y_cells = temp_cell_lat;
      }
   }

   /* Get edge middle point longitudes and latitudes */

   {
      void * temp_edge_lon;
      void * temp_edge_lat;
      size_t temp_nbr_edges;
      int edge_type;

      get_icon_edge_middle ( ncid, &edge_type, &temp_edge_lon, &temp_edge_lat, &temp_nbr_edges );

      *nbr_edges = temp_nbr_edges;

      if (edge_type == NC_FLOAT) {

         *x_edges = malloc(temp_nbr_edges * sizeof(**x_edges));
         *y_edges = malloc(temp_nbr_edges * sizeof(**y_edges));

         for (int i = 0; i < temp_nbr_edges; ++i) {

            (*x_edges)[i] = ((float*)temp_edge_lon)[i];
            (*y_edges)[i] = ((float*)temp_edge_lat)[i];
         }
         free(temp_edge_lon);
         free(temp_edge_lat);
      } else {
         *x_edges = temp_edge_lon;
         *y_edges = temp_edge_lat;
      }
   }

   /* Get mask of cells */

   {
      int * temp_cell_mask;
      size_t temp_nbr_cells;
      int cell_type;

      get_icon_cell_mask ( ncid, &cell_type, &temp_cell_mask, &temp_nbr_cells );

      *nbr_cells = temp_nbr_cells;

      *cell_mask = temp_cell_mask;
   }

   /* Get relations between vertices and cells */

   {
      size_t * dimlen;

      get_icon_connect_cell ( ncid, &vertex_of_cell, &dimlen);

      *nbr_cells = dimlen[1];

      free(dimlen);
   }

   /* Get relations between cells and vertices */

   {
      size_t * dimlen;

      get_icon_connect_vertex ( ncid, &cells_of_vertex, &dimlen);

      *nbr_vertices = dimlen[1];

      free(dimlen);
   }

   /* Get relations between vertices and edges */

   {
      size_t * dimlen;

      get_icon_connect_edge ( ncid, &edge_vertices, &dimlen);

      *nbr_edges = dimlen[1];

      free(dimlen);
   }

   close_icon_file ( ncid );

   //-------------------------------------------------------------------------//

   *num_vertices_per_cell = malloc(*nbr_cells * sizeof(**num_vertices_per_cell));
   for (int i = 0; i < *nbr_cells; (*num_vertices_per_cell)[i++] = 3);

   *cell_to_vertex = malloc(*nbr_cells * 3 * sizeof(**cell_to_vertex));

   // Unfortunately the data is only available in Fortran order
   for (int i = 0; i < *nbr_cells; ++i) {

      (*cell_to_vertex)[3*i+0] = vertex_of_cell[i+0*(*nbr_cells)] - 1;
      (*cell_to_vertex)[3*i+1] = vertex_of_cell[i+1*(*nbr_cells)] - 1;
      (*cell_to_vertex)[3*i+2] = vertex_of_cell[i+2*(*nbr_cells)] - 1;
   }

   free(vertex_of_cell);

   //-------------------------------------------------------------------------//

   *vertex_to_cell = malloc(*nbr_vertices * 6 * sizeof(**vertex_to_cell));

   // Unfortunately the data is only available in Fortran order
   for (int i = 0; i < *nbr_vertices; ++i) {

      (*vertex_to_cell)[6*i+0] = cells_of_vertex[i+0*(*nbr_vertices)] - 1;
      (*vertex_to_cell)[6*i+1] = cells_of_vertex[i+1*(*nbr_vertices)] - 1;
      (*vertex_to_cell)[6*i+2] = cells_of_vertex[i+2*(*nbr_vertices)] - 1;
      (*vertex_to_cell)[6*i+3] = cells_of_vertex[i+3*(*nbr_vertices)] - 1;
      (*vertex_to_cell)[6*i+4] = cells_of_vertex[i+4*(*nbr_vertices)] - 1;
      (*vertex_to_cell)[6*i+5] = cells_of_vertex[i+5*(*nbr_vertices)] - 1;
   }

   free(cells_of_vertex);

   //-------------------------------------------------------------------------//

   *edge_to_vertex = malloc(*nbr_edges * 2 * sizeof(**edge_to_vertex));

   // Unfortunately the data is only available in Fortran order
   for (int i = 0; i < *nbr_edges; ++i) {

      (*edge_to_vertex)[2*i+0] = edge_vertices[i+0*(*nbr_edges)] - 1;
      (*edge_to_vertex)[2*i+1] = edge_vertices[i+1*(*nbr_edges)] - 1;
   }

   free(edge_vertices);
}

static void open_icon_file (int *ncid, const char *filename) {

  int id, status;

  /* open netCDF dataset */

  status = nc_open(filename, NC_NOWRITE, &id);
  if (status != NC_NOERR) {
     printf ("Looking for file %s .\n", filename);
     handle_error(status);
  }

  *ncid = id;

}

/* ---------------------------------------------------------------- */

static void close_icon_file (int ncid) {

   int status;

  /* close netCDF dataset */

  status = nc_close(ncid);
  if (status != NC_NOERR) handle_error(status);
}

/* ---------------------------------------------------------------- */

static void get_icon_vertices ( int ncid,
                                int  *vertex_type,
                                void **vertex_lon,
                                void **vertex_lat,
                                size_t *nbr_vertices ) {

  int status;

  int glat_id;                        // Various NetCDF IDs
  int glon_id;

  int glon_ndims;                     // number of dimensions in file
  int glat_ndims;

  int glon_dimids[NC_MAX_VAR_DIMS];   // dimension NetCDF IDs
  int glat_dimids[NC_MAX_VAR_DIMS];

  nc_type glon_type;                  // variable type
  nc_type glat_type;

  int nbr_atts;                     // number of attributes

  status = nc_inq_varid (ncid, "vlon", &glon_id);
  if (status != NC_NOERR) handle_error(status);

  status = nc_inq_varid (ncid, "vlat", &glat_id);
  if (status != NC_NOERR) handle_error(status);

  status = nc_inq_var (ncid, glon_id, 0, &glon_type, &glon_ndims, glon_dimids, &nbr_atts);
  if (status != NC_NOERR) handle_error(status);

  status = nc_inq_var (ncid, glat_id, 0, &glat_type, &glat_ndims, glat_dimids, &nbr_atts);
  if (status != NC_NOERR) handle_error(status);

  /* Allocate memory to read in coordinates */

  if ( glon_type != glat_type ) {
    exit(EXIT_FAILURE);
  }

  if (glon_ndims != 1 || glat_ndims != 1) {
    exit(EXIT_FAILURE);
  }

  /* get size of arrays */

  size_t dimlen_lat; // dimension size for latitude
  size_t dimlen_lon; // dimension size for longitude

  status = nc_inq_dimlen(ncid, *glon_dimids, &dimlen_lon);
  if (status != NC_NOERR) handle_error(status);

  status = nc_inq_dimlen(ncid, *glat_dimids, &dimlen_lat);
  if (status != NC_NOERR) handle_error(status);

  if (dimlen_lon != dimlen_lat)
    exit(EXIT_FAILURE);

  *nbr_vertices = dimlen_lon;

  /* read center coordinates and convert radians into degrees */

  size_t start = 0;

  switch(glon_type) {

  case NC_FLOAT:

    *vertex_lon = (float * ) malloc ( *nbr_vertices * sizeof ( float ) );
    *vertex_lat = (float * ) malloc ( *nbr_vertices * sizeof ( float ) );

    status = nc_get_vara (ncid, glon_id, &start, nbr_vertices, (*vertex_lon));
    if (status != NC_NOERR) handle_error(status);

    status = nc_get_vara (ncid, glat_id, &start, nbr_vertices, (*vertex_lat));
    if (status != NC_NOERR) handle_error(status);

    break;

  case NC_DOUBLE:

    *vertex_lon = (double * ) malloc ( *nbr_vertices * sizeof ( double ) );
    *vertex_lat = (double * ) malloc ( *nbr_vertices * sizeof ( double ) );

    status = nc_get_vara_double (ncid, glon_id, &start, nbr_vertices, (*vertex_lon));
    if (status != NC_NOERR) handle_error(status);

    status = nc_get_vara_double (ncid, glat_id, &start, nbr_vertices, (*vertex_lat));
    if (status != NC_NOERR) handle_error(status);

    break;

  default:

    exit(EXIT_FAILURE);

  }

  *vertex_type = glon_type;
}

/* ---------------------------------------------------------------- */

static void get_icon_cell_center ( int ncid,
                                   int  *cell_type,
                                   void **cell_lon,
                                   void **cell_lat,
                                   size_t *nbr_cells ) {

  int status;

  int glat_id;                        // Various NetCDF IDs
  int glon_id;

  int glon_ndims;                     // number of dimensions in file
  int glat_ndims;

  int glon_dimids[NC_MAX_VAR_DIMS];   // dimension NetCDF IDs
  int glat_dimids[NC_MAX_VAR_DIMS];

  nc_type glon_type;                  // variable type
  nc_type glat_type;

  int nbr_atts;                       // number of attributes

  status = nc_inq_varid (ncid, "clon", &glon_id);
  if (status != NC_NOERR) handle_error(status);

  status = nc_inq_varid (ncid, "clat", &glat_id);
  if (status != NC_NOERR) handle_error(status);

  status = nc_inq_var (ncid, glon_id, 0, &glon_type, &glon_ndims, glon_dimids, &nbr_atts);
  if (status != NC_NOERR) handle_error(status);

  status = nc_inq_var (ncid, glat_id, 0, &glat_type, &glat_ndims, glat_dimids, &nbr_atts);
  if (status != NC_NOERR) handle_error(status);

  /* Allocate memory to read in coordinates */

  if ( glon_type != glat_type ) {
    exit(EXIT_FAILURE);
  }

  if (glon_ndims != 1 || glat_ndims != 1) {
    exit(EXIT_FAILURE);
  }

  /* get size of arrays */

  size_t dimlen_lat; // dimension size for latitude
  size_t dimlen_lon; // dimension size for longitude

  status = nc_inq_dimlen(ncid, *glon_dimids, &dimlen_lon);
  if (status != NC_NOERR) handle_error(status);

  status = nc_inq_dimlen(ncid, *glat_dimids, &dimlen_lat);
  if (status != NC_NOERR) handle_error(status);

  if (dimlen_lon != dimlen_lat)
    exit(EXIT_FAILURE);

  *nbr_cells = dimlen_lon;

  /* read center coordinates and convert radians into degrees */

  size_t start = 0;

  switch(glon_type) {

  case NC_FLOAT:

    *cell_lon = (float * ) malloc ( *nbr_cells * sizeof ( float ) );
    *cell_lat = (float * ) malloc ( *nbr_cells * sizeof ( float ) );

    status = nc_get_vara (ncid, glon_id, &start, nbr_cells, (*cell_lon));
    if (status != NC_NOERR) handle_error(status);

    status = nc_get_vara (ncid, glat_id, &start, nbr_cells, (*cell_lat));
    if (status != NC_NOERR) handle_error(status);

    break;

  case NC_DOUBLE:

    *cell_lon = (double * ) malloc ( *nbr_cells * sizeof ( double ) );
    *cell_lat = (double * ) malloc ( *nbr_cells * sizeof ( double ) );

    status = nc_get_vara_double (ncid, glon_id, &start, nbr_cells, (*cell_lon));
    if (status != NC_NOERR) handle_error(status);

    status = nc_get_vara_double (ncid, glat_id, &start, nbr_cells, (*cell_lat));
    if (status != NC_NOERR) handle_error(status);

    break;

  default:

    exit(EXIT_FAILURE);

  }

  *cell_type = glon_type;
}

/* ---------------------------------------------------------------- */

static void get_icon_edge_middle ( int ncid,
                                   int  *edge_type,
                                   void **edge_lon,
                                   void **edge_lat,
                                   size_t *nbr_edges ) {

  int status;

  int glat_id;                        // Various NetCDF IDs
  int glon_id;

  int glon_ndims;                     // number of dimensions in file
  int glat_ndims;

  int glon_dimids[NC_MAX_VAR_DIMS];   // dimension NetCDF IDs
  int glat_dimids[NC_MAX_VAR_DIMS];

  nc_type glon_type;                  // variable type
  nc_type glat_type;

  int nbr_atts;                       // number of attributes

  status = nc_inq_varid (ncid, "elon", &glon_id);
  if (status != NC_NOERR) handle_error(status);

  status = nc_inq_varid (ncid, "elat", &glat_id);
  if (status != NC_NOERR) handle_error(status);

  status = nc_inq_var (ncid, glon_id, 0, &glon_type, &glon_ndims, glon_dimids, &nbr_atts);
  if (status != NC_NOERR) handle_error(status);

  status = nc_inq_var (ncid, glat_id, 0, &glat_type, &glat_ndims, glat_dimids, &nbr_atts);
  if (status != NC_NOERR) handle_error(status);

  /* Allocate memory to read in coordinates */

  if ( glon_type != glat_type ) {
    exit(EXIT_FAILURE);
  }

  if (glon_ndims != 1 || glat_ndims != 1) {
    exit(EXIT_FAILURE);
  }

  /* get size of arrays */

  size_t dimlen_lat; // dimension size for latitude
  size_t dimlen_lon; // dimension size for longitude

  status = nc_inq_dimlen(ncid, *glon_dimids, &dimlen_lon);
  if (status != NC_NOERR) handle_error(status);

  status = nc_inq_dimlen(ncid, *glat_dimids, &dimlen_lat);
  if (status != NC_NOERR) handle_error(status);

  if (dimlen_lon != dimlen_lat)
    exit(EXIT_FAILURE);

  *nbr_edges = dimlen_lon;

  /* read center coordinates and convert radians into degrees */

  size_t start = 0;

  switch(glon_type) {

  case NC_FLOAT:

    *edge_lon = (float * ) malloc ( *nbr_edges * sizeof ( float ) );
    *edge_lat = (float * ) malloc ( *nbr_edges * sizeof ( float ) );

    status = nc_get_vara (ncid, glon_id, &start, nbr_edges, (*edge_lon));
    if (status != NC_NOERR) handle_error(status);

    status = nc_get_vara (ncid, glat_id, &start, nbr_edges, (*edge_lat));
    if (status != NC_NOERR) handle_error(status);

    break;

  case NC_DOUBLE:

    *edge_lon = (double * ) malloc ( *nbr_edges * sizeof ( double ) );
    *edge_lat = (double * ) malloc ( *nbr_edges * sizeof ( double ) );

    status = nc_get_vara_double (ncid, glon_id, &start, nbr_edges, (*edge_lon));
    if (status != NC_NOERR) handle_error(status);

    status = nc_get_vara_double (ncid, glat_id, &start, nbr_edges, (*edge_lat));
    if (status != NC_NOERR) handle_error(status);

    break;

  default:

    exit(EXIT_FAILURE);

  }

  *edge_type = glon_type;
}

/* ---------------------------------------------------------------- */

static void get_icon_cell_mask ( int ncid,
                                 int  *cell_type,
                                 int **cell_mask,
                                 size_t *nbr_cells ) {

  int status;

  int mask_id;                        // Various NetCDF IDs
  int mask_ndims;                     // number of dimensions in file
  int mask_dimids[NC_MAX_VAR_DIMS];   // dimension NetCDF IDs

  nc_type mask_type;                  // variable type

  int nbr_atts;                       // number of attributes

  status = nc_inq_varid (ncid, "cell_sea_land_mask", &mask_id);

if (status != NC_NOERR) handle_error(status);

  status = nc_inq_var (ncid, mask_id, 0, &mask_type, &mask_ndims, mask_dimids, &nbr_atts);
  if (status != NC_NOERR) handle_error(status);

  /* Allocate memory to read in coordinates */

  if (mask_ndims != 1) exit(EXIT_FAILURE);

  /* get size of arrays */

  size_t dimlen_mask; // dimension size for latitude

  status = nc_inq_dimlen(ncid, *mask_dimids, &dimlen_mask);
  if (status != NC_NOERR) handle_error(status);

  *nbr_cells = dimlen_mask;

  /* read cell mask */

  size_t start = 0;

  switch(mask_type) {

  case NC_INT:

    *cell_mask = (int * ) malloc ( *nbr_cells * sizeof ( int ) );

    status = nc_get_vara_int (ncid, mask_id, &start, nbr_cells, (*cell_mask));
    if (status != NC_NOERR) handle_error(status);

    break;

  default:

    exit(EXIT_FAILURE);

  }

  *cell_type = mask_type;
}

/* ---------------------------------------------------------------- */

static void get_icon_connect_cell ( int ncid, int **vertex_of_cell,
                                    size_t **dimlen ) {

  int status;

  int conn_id;
  int conn_ndims;
  int conn_dimids[NC_MAX_VAR_DIMS];
  nc_type conn_type;

  int nbr_atts;                     // number of attributes

  status = nc_inq_varid (ncid, "vertex_of_cell", &conn_id);
  if (status != NC_NOERR) handle_error(status);

  status = nc_inq_var (ncid, conn_id, 0, &conn_type, &conn_ndims, conn_dimids, &nbr_atts);
  if (status != NC_NOERR) handle_error(status);

  /* get size of arrays */

  size_t *start;
  size_t *count;

  start = ( size_t * ) malloc ( conn_ndims * sizeof (size_t) );
  count = ( size_t * ) malloc ( conn_ndims * sizeof (size_t) );

  *dimlen = ( size_t * ) malloc ( conn_ndims * sizeof (size_t) );

  int length = 1;

  int i;

  for ( i = 0; i < conn_ndims; i++ ) {

    start[i] = 0;

    status = nc_inq_dimlen(ncid, conn_dimids[i], &count[i]);
    if (status != NC_NOERR) handle_error(status);

    length = length * count[i];
    (*dimlen)[i] = count[i];

  }

  /* read connectivity */

  *vertex_of_cell = ( int *) malloc ( length * sizeof ( int ) );

  status = nc_get_vara_int(ncid, conn_id, start, count, (*vertex_of_cell));
  if (status != NC_NOERR) handle_error(status);

  free (start);
  free (count);

}

/* ---------------------------------------------------------------- */

static void get_icon_connect_vertex ( int ncid, int **cells_of_vertex,
                                      size_t **dimlen ) {

  int status;

  int conn_id;
  int conn_ndims;
  int conn_dimids[NC_MAX_VAR_DIMS];
  nc_type conn_type;

  int nbr_atts;                     // number of attributes

  status = nc_inq_varid (ncid, "cells_of_vertex", &conn_id);
  if (status != NC_NOERR) handle_error(status);

  status = nc_inq_var (ncid, conn_id, 0, &conn_type, &conn_ndims, conn_dimids, &nbr_atts);
  if (status != NC_NOERR) handle_error(status);

  /* get size of arrays */

  size_t *start;
  size_t *count;

  start = ( size_t * ) malloc ( conn_ndims * sizeof (size_t) );
  count = ( size_t * ) malloc ( conn_ndims * sizeof (size_t) );

  *dimlen = ( size_t * ) malloc ( conn_ndims * sizeof (size_t) );

  int length = 1;

  int i;

  for ( i = 0; i < conn_ndims; i++ ) {

    start[i] = 0;

    status = nc_inq_dimlen(ncid, conn_dimids[i], &count[i]);
    if (status != NC_NOERR) handle_error(status);

    length = length * count[i];
    (*dimlen)[i] = count[i];

  }

  /* read connectivity */

  *cells_of_vertex = ( int *) malloc ( length * sizeof ( int ) );

  status = nc_get_vara_int(ncid, conn_id, start, count, (*cells_of_vertex));
  if (status != NC_NOERR) handle_error(status);

  free (start);
  free (count);

}

/* ---------------------------------------------------------------- */

static void get_icon_connect_edge ( int ncid, int **edge_vertices,
                                    size_t **dimlen ) {

  int status;

  int conn_id;
  int conn_ndims;
  int conn_dimids[NC_MAX_VAR_DIMS];
  nc_type conn_type;

  int nbr_atts;                     // number of attributes

  status = nc_inq_varid (ncid, "edge_vertices", &conn_id);
  if (status != NC_NOERR) handle_error(status);

  status = nc_inq_var (ncid, conn_id, 0, &conn_type, &conn_ndims, conn_dimids, &nbr_atts);
  if (status != NC_NOERR) handle_error(status);

  /* get size of arrays */

  size_t *start;
  size_t *count;

  start = ( size_t * ) malloc ( conn_ndims * sizeof (size_t) );
  count = ( size_t * ) malloc ( conn_ndims * sizeof (size_t) );

  *dimlen = ( size_t * ) malloc ( conn_ndims * sizeof (size_t) );

  int length = 1;

  int i;

  for ( i = 0; i < conn_ndims; i++ ) {

    start[i] = 0;

    status = nc_inq_dimlen(ncid, conn_dimids[i], &count[i]);
    if (status != NC_NOERR) handle_error(status);

    length = length * count[i];
    (*dimlen)[i] = count[i];

  }

  /* read connectivity */

  *edge_vertices = ( int *) malloc ( length * sizeof ( int ) );

  status = nc_get_vara_int(ncid, conn_id, start, count, (*edge_vertices));
  if (status != NC_NOERR) handle_error(status);

  free (start);
  free (count);

}

/* ---------------------------------------------------------------- */

static void handle_error(int status)
{
  if (status == NC_NOERR) return;
  const char *err_string;
  err_string = nc_strerror(status);
  printf ( "%s \n", err_string );
  exit(EXIT_FAILURE);
}
