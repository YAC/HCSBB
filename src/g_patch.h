#ifndef G_PATCH_H
#define G_PATCH_H

unsigned get_num_g_coefficients(unsigned degree);
void compute_g_polynomials(
  unsigned degree, double * triangle, double * vertices, unsigned num_vertices,
  double * restrict g_polynomials);
void compute_g_patch_coefficients(
  unsigned degree, double * g_polynomials, double * f_v, unsigned num_vertices,
  double * g_coefficients);
void evaluate_g_patch(
  double * vertices, unsigned num_vertices, double * triangle,
  unsigned degree, double * sbb_coefficients, double * p);
double compute_directional_derivative_g_2(
  unsigned degree, double * triangle, double * coefficients, double * vertex,
  double * direction);

#endif // G_PATCH_H