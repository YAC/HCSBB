#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "patch_common.h"
#include "sbb_patch.h"
#include "utils.h"

unsigned get_num_sbb_coefficients(unsigned degree) {

  return ((degree + 2) * (degree + 1)) / 2;
}

static inline double factorial(unsigned x) {

  double fac;

  switch(x) {
    case(0): fac = 1.0; break;
    case(1): fac = 1.0; break;
    case(2): fac = 2.0; break;
    case(3): fac = 6.0; break;
    case(4): fac = 24.0; break;
    case(5): fac = 120.0; break;
    case(6): fac = 720.0; break;
    case(7): fac = 5040.0; break;
    default: fac = -1.0;
  }
  return fac;
}

static inline double pow_uint(double x, unsigned y) {

  double pow;

  switch(y) {
    case(0): pow = 1.0; break;
    case(1): pow = x; break;
    case(2): pow = x*x; break;
    case(3): pow = x*x*x; break;
    case(4): pow = x*x*x*x; break;
    case(5): pow = x*x*x*x*x; break;
    case(6): pow = x*x*x*x*x*x; break;
    case(7): pow = x*x*x*x*x*x*x; break;
    default: pow = 0.0;
  }
  return pow;
}

// computes the spherical Bernstein basis polynomials
// for degree = 3 the polynomials B_ijk are ordered in the following order:
// B_003, B_012, B_021, B_030, B_102, B_111, B_120, B_201, B_210, B_300
//! \todo precompute all required powers of the coordinates and factorials,
//!       hardcode all computation for degree = 3
void compute_sb_polynomials(
  unsigned degree, double * triangle, double * vertices, unsigned num_vertices,
  double * restrict sb_polynomials) {

  double * restrict sb_coords = malloc(3 * num_vertices * sizeof(*sb_coords));

  // compute spherical barycentric coordinates for all vertices
  compute_sb_coords(vertices, num_vertices, triangle, sb_coords);

  double d_factorial = factorial(degree);

  size_t sb_poly_idx = 0;

  for (unsigned i = 0; i <= degree; ++i) {

    double i_factorial = factorial(i);

    for (unsigned j = 0; j <= degree - i; ++j) {

      unsigned k = degree - j - i;
      double t = (d_factorial / (i_factorial * factorial(j) * factorial(k)));

      for (size_t vertex_idx = 0; vertex_idx < (size_t)num_vertices;
           ++vertex_idx, ++sb_poly_idx) {

        // Bd_ijk(v) = d!/(i!j!k!) * b_1(v)^i * b_2(v)^j * b_3(v)^k
        // where: i + j + k = d and
        //        b(v) = spherical barycentric coordinates of vertex v
        sb_polynomials[sb_poly_idx] =
          t * (pow_uint(sb_coords[0+3*vertex_idx], i) *
               pow_uint(sb_coords[1+3*vertex_idx], j) *
               pow_uint(sb_coords[2+3*vertex_idx], k));
      }
    }
  }

  free(sb_coords);
}

void compute_d_sb_polynomials(
  unsigned degree, double * triangle, double * vertices, unsigned num_vertices,
  double * direction, double * restrict sb_polynomials) {

  double * restrict sb_coords = malloc(3 * num_vertices * sizeof(*sb_coords));
  double sb_direction[3];

  // compute spherical barycentric coordinates for all vertices
  compute_sb_coords(vertices, num_vertices, triangle, sb_coords);

  // compute spherical barycentric coordinate for the direction
  compute_sb_coords(direction, 1, triangle, &sb_direction[0]);

  double d_factorial = factorial(degree);

  size_t sb_poly_idx = 0;

  for (unsigned i = 0; i <= degree; ++i) {

    double i_factorial = factorial(i);

    for (unsigned j = 0; j <= degree - i; ++j) {

      unsigned k = degree - j - i;
      double t = (d_factorial / (i_factorial * factorial(j) * factorial(k)));

      for (size_t vertex_idx = 0; vertex_idx < (size_t)num_vertices;
           ++vertex_idx, ++sb_poly_idx) {

        sb_polynomials[sb_poly_idx] = 0.0;

        if (i > 0)
          sb_polynomials[sb_poly_idx] +=
            sb_direction[0] * (t * (double)i) *
            (pow_uint(sb_coords[0+3*vertex_idx], i-1) *
             pow_uint(sb_coords[1+3*vertex_idx], j) *
             pow_uint(sb_coords[2+3*vertex_idx], k));
        if (j > 0)
          sb_polynomials[sb_poly_idx] +=
            sb_direction[1] * (t * (double)j) *
            (pow_uint(sb_coords[0+3*vertex_idx], i) *
             pow_uint(sb_coords[1+3*vertex_idx], j-1) *
             pow_uint(sb_coords[2+3*vertex_idx], k));
        if (k > 0)
          sb_polynomials[sb_poly_idx] +=
            sb_direction[2] * (t * (double)k) *
            (pow_uint(sb_coords[0+3*vertex_idx], i) *
             pow_uint(sb_coords[1+3*vertex_idx], j) *
             pow_uint(sb_coords[2+3*vertex_idx], k-1));
      }
    }
  }

  free(sb_coords);
}

void compute_sbb_patch_coefficients(
  unsigned degree, double * sb_polynomials, double * f_v, unsigned num_vertices,
  double * sbb_coefficients) {

  compute_patch_coefficients(
    get_num_sbb_coefficients(degree), sb_polynomials, f_v, num_vertices,
    sbb_coefficients);
}

void evaluate_sbb_patch(
  double * vertices, unsigned num_vertices, double * triangle,
  unsigned degree, double * restrict sbb_coefficients, double * restrict p) {

  unsigned num_sbb_coeffs = get_num_sbb_coefficients(degree);
  double * restrict sb_polynomials =
    malloc(num_vertices * num_sbb_coeffs * sizeof(*sb_polynomials));

  // compute the spherical Bernstein base polynomials for the given vertices
  compute_sb_polynomials(
    degree, triangle, vertices, num_vertices, sb_polynomials);

  evaluate_patch(
    vertices, (size_t)num_vertices, num_sbb_coeffs,  sbb_coefficients,
    sb_polynomials, p);

  free(sb_polynomials);
}

// computes the derivative of the SBB-patch in v in the direction of g
double compute_directional_derivative(
  unsigned degree, double * triangle, double * coefficients, double * vertex,
  double * direction) {

  // the directional derivative is computed by:
  // D_g p(v) = d * SUM(c^1_ijk(g) * B^(d-1)_ijk(v))
  // where: i + j + k = d - 1 and
  //        c^1_ijk(g) = b_1(g)*c_(i+1,j,k) +
  //                     b_2(g)*c_(i,j+1,k) +
  //                     b_3(g)*c_(i,j,k+1)

  unsigned num_sbb_coeffs = get_num_sbb_coefficients(degree - 1);
  double * sb_polynomials = malloc(num_sbb_coeffs * sizeof(*sb_polynomials));

  // compute the spherical Bernstein basis polynomials
  compute_sb_polynomials(degree-1, triangle, vertex, 1, sb_polynomials);

  double b_g[3];

  // compute the spherical barycentric coordinates for the directional vector
  compute_sb_coords(direction, 1, triangle, &b_g[0]);

  double D_g = 0.0;

  size_t sb_poly_idx = 0;

  for (unsigned i = 0; i <= degree - 1; ++i) {

    for (unsigned j = 0; j <= degree - i - 1; ++j) {

      unsigned k = degree - j - i - 1;

      D_g += (b_g[0] * coefficients[get_sbb_coeff_idx_from_ijk(i+1,j,k, degree)] +
              b_g[1] * coefficients[get_sbb_coeff_idx_from_ijk(i,j+1,k, degree)] +
              b_g[2] * coefficients[get_sbb_coeff_idx_from_ijk(i,j,k+1, degree)]) *
             sb_polynomials[sb_poly_idx++];
    }
  }

  free(sb_polynomials);

  return D_g * (double)degree;
}

double compute_directional_derivative_2(
  unsigned degree, double * triangle, double * coefficients, double * vertex,
  double * direction) {

  double dx = direction[0];
  double dy = direction[1];
  double dz = direction[2];

  double scale = 1.0e-6;

  double vertices[2][3];

  vertices[0][0] = vertex[0];
  vertices[0][1] = vertex[1];
  vertices[0][2] = vertex[2];
  vertices[1][0] = vertex[0] + scale * dx;
  vertices[1][1] = vertex[1] + scale * dy;
  vertices[1][2] = vertex[2] + scale * dz;

  double f[2];

  evaluate_sbb_patch(&vertices[0][0], 2, triangle, degree, coefficients, &f[0]);

  return (f[1] - f[0]) / scale;
}
