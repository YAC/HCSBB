#ifndef DEBUG_OUTPUT_H
#define DEBUG_OUTPUT_H

#ifdef DEBUG
void print_bnd_triangle(double * vertices, unsigned num_vertices,
                        double * x_triangle, int file_id);
void print_field(double * vertices, unsigned num_vertices, double * f_v,
                 unsigned num_fields, char const * filename, char const * title,
                 char const * field_name[]);
#endif // DEBUG
#endif //DEBUG_OUTPUT_H
