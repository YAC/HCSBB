#ifndef VERTEX_GENERATOR_H
#define VERTEX_GENERATOR_H

void generate_vertices(double * vertices, unsigned num_vertices);
void generate_vertices_2(double * vertices, unsigned num_vertices,
                         double min_lon, double max_lon,
                         double min_lat, double max_lat);
#endif // VERTEX_GENERATOR_H
