#include <stdlib.h>
#include <string.h>
#include <math.h>

#ifdef DEBUG
#include "vtk_output.h"
#include "utils.h"
#include "geometry.h"
#include "debug_output.h"

#define EDGE_RESOLUTION (100)

void generate_edge_point_data(
  double * point_data, double a[], double b[], unsigned edge_resolution) {

  double t[3];
  for (unsigned i = 0; i < 3; ++i)
    t[i] = (b[i] - a[i]) / (double)edge_resolution;

  for (unsigned i = 0; i < edge_resolution; ++i) {

    double * x = point_data + 3 * i;
    for (unsigned j = 0; j < 3; ++j) x[j] = a[j] + t[j] * (double)i;
    normalise_vector(x);
  }
}

void print_bnd_triangle(double * vertices, unsigned num_vertices,
                        double * x_triangle, int file_id) {

  const char * filename_format = "result_%d.vtk";
  char filename[1024];
  const char * title = "result";

  sprintf(filename, filename_format, file_id);

  // generate point data
  double * point_data =
    malloc(3 * (num_vertices + 3 * EDGE_RESOLUTION) * sizeof(*point_data));
  memcpy(point_data, vertices, 3 * num_vertices * sizeof(*vertices));
  generate_edge_point_data(point_data + 3*num_vertices + 0*3*EDGE_RESOLUTION,
                           x_triangle + 0*3, x_triangle + 1*3, EDGE_RESOLUTION);
  generate_edge_point_data(point_data + 3*num_vertices + 1*3*EDGE_RESOLUTION,
                           x_triangle + 1*3, x_triangle + 2*3, EDGE_RESOLUTION);
  generate_edge_point_data(point_data + 3*num_vertices + 2*3*EDGE_RESOLUTION,
                           x_triangle + 2*3, x_triangle + 0*3, EDGE_RESOLUTION);

  // generate cell data
  unsigned * cell_data =
    malloc((num_vertices + 3 * EDGE_RESOLUTION) * sizeof(*cell_data));
  unsigned * num_points_per_cell =
    malloc((num_vertices + 1) * sizeof(*num_points_per_cell));
  for (unsigned i = 0; i < num_vertices; ++i) cell_data[i] = i;
  for (unsigned i = 0; i < num_vertices; ++i) num_points_per_cell[i] = 1;
  for (unsigned i = 0; i < 3 * EDGE_RESOLUTION; ++i)
    cell_data[i+num_vertices] = num_vertices + i;
  num_points_per_cell[num_vertices] = 3 * EDGE_RESOLUTION;

  // generate scalar point data (vertices == 0, triangle corners  == 1)
  int * scalar_point_data =
    malloc((num_vertices + 3 * EDGE_RESOLUTION) * sizeof(*scalar_point_data));
  for (unsigned i = 0; i < num_vertices; ++i) scalar_point_data[i] = 0;
  for (unsigned i = 0; i < 3 * EDGE_RESOLUTION; ++i)
    scalar_point_data[num_vertices + i] = 1;

  VTK_FILE * file = vtk_open(filename, title);
  vtk_write_point_data(file, point_data, num_vertices + 3 * EDGE_RESOLUTION);
  vtk_write_cell_data(file, cell_data, num_points_per_cell, num_vertices + 1);
  vtk_write_point_scalars_int(file, scalar_point_data,
                              num_vertices + 3 * EDGE_RESOLUTION, "type");
  vtk_close(file);

  free(scalar_point_data);
  free(num_points_per_cell);
  free(cell_data);
  free(point_data);
}

void print_field(double * vertices, unsigned num_vertices, double * f_v,
                 unsigned num_fields, char const * filename, char const * title,
                 char const * field_name[]) {

  // generate cell data
  unsigned * cell_data =
    malloc(num_vertices * sizeof(*cell_data));
  unsigned * num_points_per_cell =
    malloc(num_vertices * sizeof(*num_points_per_cell));
  for (unsigned i = 0; i < num_vertices; ++i) cell_data[i] = i;
  for (unsigned i = 0; i < num_vertices; ++i) num_points_per_cell[i] = 1;

  VTK_FILE * file = vtk_open(filename, title);
  vtk_write_point_data(file, vertices, num_vertices);
  vtk_write_cell_data(file, cell_data, num_points_per_cell, num_vertices);
  for (unsigned i = 0; i < num_fields; ++i)
    vtk_write_point_scalars_double(file, f_v + i * num_vertices, num_vertices,
                                   field_name[i]);
  vtk_close(file);

  free(num_points_per_cell);
  free(cell_data);
}
#endif // DEBUG
