#include <stdlib.h>
#include <math.h>

#include "vertex_generator.h"

#ifndef  M_PI
#define  M_PI        3.14159265358979323846264338327950288  /* pi */
#endif
static double const rad = M_PI / 180.0;

// computes the cartesian coordinates of a point based on the given
// lon lat coordinates
static inline void LLtoXYZ(double lon, double lat, double p_out[]) {

   double cos_lat = cos(lat);
   p_out[0] = cos_lat * cos(lon);
   p_out[1] = cos_lat * sin(lon);
   p_out[2] = sin(lat);
}

void generate_vertices(double * vertices, unsigned num_vertices) {

  srand (1337);

  for (unsigned i = 0; i < num_vertices; ++i) {

    // double const min_lon = -5.0 * rad, max_lon = 5.0 * rad,
                 // min_lat = -5.0 * rad, max_lat = 5.0 * rad;
    double const min_lon = 23.0 * rad, max_lon = 24.0 * rad,
                 min_lat = -60.0 * rad, max_lat = -59.0 * rad;

    LLtoXYZ(min_lon + (max_lon - min_lon) * ((double)rand()/(double)RAND_MAX),
            min_lat + (max_lat - min_lat) * ((double)rand()/(double)RAND_MAX),
            vertices + i * 3);
  }
}

void generate_vertices_2(double * vertices, unsigned num_vertices,
                         double min_lon, double max_lon,
                         double min_lat, double max_lat) {

  srand (1337);

  for (unsigned i = 0; i < num_vertices; ++i) {

    LLtoXYZ(min_lon + (max_lon - min_lon) * ((double)rand()/(double)RAND_MAX),
            min_lat + (max_lat - min_lat) * ((double)rand()/(double)RAND_MAX),
            vertices + i * 3);
  }
}
