/**
 * @file sphere_part.c
 *
 * @copyright Copyright  (C)  2014 Moritz Hanke <hanke@dkrz.de>
 *                                 Thomas Jahns <jahns@dkrz.de>
 *
 * @version 1.0
 * @author Moritz Hanke <hanke@dkrz.de>
 *         Thomas Jahns <jahns@dkrz.de>
 */
/*
 * Keywords:
 * Maintainer: Moritz Hanke <hanke@dkrz.de>
 *             Rene Redler <rene.redler@mpimet.mpg.de>
 * URL: https://doc.redmine.dkrz.de/YAC/html/index.html
 *
 * This file is part of YAC.
 *
 * YAC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * YAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with YAC.  If not, see <http://www.gnu.org/licenses/gpl.txt>.
 */

#include <assert.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include "sphere_part.h"
#include "ensure_array_size.h"
#include "geometry.h"
#include "utils.h"

enum {
   I_list_tree_min_size = 2, //!< make I list into tree when list is
                             //!<  larger than this
};

enum {
   U_IS_LEAF = 1,
   T_IS_LEAF = 2,
};


struct point_sphere_part_search {

   struct point_sphere_part_node base_node;
   double * coordinates_xyz;
};

static void init_point_sphere_part_node(struct point_sphere_part_node * node) {

   node->flags = 0;
   node->U_size = 0;
   node->T_size = 0;
   node->gc_norm_vector[0] = 0;
   node->gc_norm_vector[1] = 0;
   node->gc_norm_vector[2] = 1;
}

static struct point_sphere_part_node * get_point_sphere_part_node() {

   struct point_sphere_part_node * node = malloc(1 * sizeof(*node));

   init_point_sphere_part_node(node);

   return node;
}


static void partition_point_data (
  double * coordinates_xyz, unsigned * local_point_ids, unsigned num_points,
  unsigned threshold, struct point_sphere_part_node * parent_node,
  double prev_gc_norm_vector[]) {

  double balance_point[3] = {0.0,0.0,0.0};

  // compute balance point

  for (unsigned i = 0; i < num_points; ++i) {

    balance_point[0] += coordinates_xyz[0+local_point_ids[i]*3];
    balance_point[1] += coordinates_xyz[1+local_point_ids[i]*3];
    balance_point[2] += coordinates_xyz[2+local_point_ids[i]*3];
  }

  normalise_vector(balance_point);

  // compute the great circle that partitions the data in half (more or less)

  crossproduct_ld(balance_point, prev_gc_norm_vector,
                  parent_node->gc_norm_vector);
  normalise_vector(parent_node->gc_norm_vector);

  // partition data into points that are on the great circle and points that
  // are on side of the circle

  unsigned * U = NULL; // list of points on one side of the great circle
  unsigned * T = NULL; // list of points on that other side of the great circle

  unsigned U_size = 0, U_array_size = 0;
  unsigned T_size = 0, T_array_size = 0;

  for (unsigned i = 0; i < num_points; ++i) {

    // angle between the point of the point coordinates and the great circle
    // plane
    // acos(dot(gc_norm_vector, point_xyz)) =
    //   angle(gc_norm_vector, point_xyz)
    // acos(dot(gc_norm_vector, point_xyz)) - PI/2 =
    //   angle(gc_plane, point_xyz)
    // acos(x) = PI/2 - asin(x)
    // - asin(dot(gc_norm_vector, point_xyz)) =
    //   angle(gc_plane, point_xyz)
    // since we just want to know whether (angle >= 0.0), we can test
    // (dot <= 0.0) instead
    double dot = coordinates_xyz[0+local_point_ids[i]*3]*
                 parent_node->gc_norm_vector[0] +
                 coordinates_xyz[1+local_point_ids[i]*3]*
                 parent_node->gc_norm_vector[1] +
                 coordinates_xyz[2+local_point_ids[i]*3]*
                 parent_node->gc_norm_vector[2];

    // (angle >= M_PI_2)
    if (dot <= 0.0) {

      // add to list U
      ENSURE_ARRAY_SIZE(U, U_array_size, U_size + 1);
      U[U_size++] = local_point_ids[i];

    } else {

      // add to list T
      ENSURE_ARRAY_SIZE(T, T_array_size, T_size + 1);
      T[T_size++] = local_point_ids[i];
    }
  }

  parent_node->U_size = U_size;
  parent_node->T_size = T_size;

  // check whether the lists are small enough (if not -> partition again)
  if (U_size <= threshold) {

    parent_node->U = realloc(U, U_size * sizeof(unsigned));
    parent_node->flags |= U_IS_LEAF;

  } else {

    parent_node->U = get_point_sphere_part_node();
    partition_point_data(coordinates_xyz, U, U_size, threshold,
                         parent_node->U, parent_node->gc_norm_vector);
    free(U);
  }

  if (T_size <= threshold) {

    parent_node->T = realloc(T, T_size * sizeof(unsigned));
    parent_node->flags |= T_IS_LEAF;

  } else {

    parent_node->T = get_point_sphere_part_node();
    partition_point_data(coordinates_xyz, T, T_size, threshold,
                         parent_node->T, parent_node->gc_norm_vector);
    free(T);
  }
}
struct point_sphere_part_search * yac_point_sphere_part_search_new (
  unsigned num_points, double * x_coordinates, double * y_coordinates) {

  if (num_points == 0) return NULL;

  struct point_sphere_part_search * search = malloc(1 * sizeof(*search));

  double gc_norm_vector[3] = {0.0,0.0,1.0};

  unsigned * local_point_ids = malloc(num_points * sizeof(*local_point_ids));
  double * coordinates_xyz = malloc(3 * num_points * sizeof(*coordinates_xyz));

  for (unsigned i = 0; i < num_points; ++i) {
    local_point_ids[i] = i;
    LLtoXYZ(x_coordinates[i], y_coordinates[i], coordinates_xyz + i * 3);
  }

  init_point_sphere_part_node(&(search->base_node));

  partition_point_data(coordinates_xyz, local_point_ids, num_points,
                       I_list_tree_min_size, &(search->base_node),
                       gc_norm_vector);

  search->coordinates_xyz = coordinates_xyz;

  free(local_point_ids);

  return search;
}

static void point_search_bnd_circle(struct point_sphere_part_node * node,
                                    double * bnd_circle_base_vector,
                                    double sin_inc_angle,
                                    unsigned ** restrict overlap_cells,
                                    unsigned * overlap_cells_array_size,
                                    unsigned * restrict num_overlap_cells) {

   double dot = node->gc_norm_vector[0]*bnd_circle_base_vector[0] +
                node->gc_norm_vector[1]*bnd_circle_base_vector[1] +
                node->gc_norm_vector[2]*bnd_circle_base_vector[2];

   // angle + inc_angle >= M_PI_2
   if (dot <= sin_inc_angle) {

      if (node->flags & U_IS_LEAF) {

         ENSURE_ARRAY_SIZE(*overlap_cells, *overlap_cells_array_size,
                           *num_overlap_cells + node->U_size);
         for (unsigned i = 0; i < node->U_size; ++i)
            (*overlap_cells)[(*num_overlap_cells) + i] =
               ((unsigned*)(node->U))[i];
         *num_overlap_cells += node->U_size;

      } else {
         point_search_bnd_circle(node->U, bnd_circle_base_vector, sin_inc_angle,
                                 overlap_cells, overlap_cells_array_size,
                                 num_overlap_cells);
      }
   }

   // angle - inc_angle < M_PI_2
   if (dot > - sin_inc_angle) {

      if (node->flags & T_IS_LEAF) {

         ENSURE_ARRAY_SIZE(*overlap_cells, *overlap_cells_array_size,
                           *num_overlap_cells + node->T_size);
         for (unsigned i = 0; i < node->T_size; ++i)
            (*overlap_cells)[(*num_overlap_cells) + i] =
               ((unsigned*)(node->T))[i];
         *num_overlap_cells += node->T_size;

      } else {
         point_search_bnd_circle(node->T, bnd_circle_base_vector, sin_inc_angle,
                                 overlap_cells, overlap_cells_array_size,
                                 num_overlap_cells);
      }
   }
}

static int point_check_bnd_circle(struct point_sphere_part_node * node,
                                  double * bnd_circle_base_vector,
                                  double sin_inc_angle, double cos_inc_angle,
                                  struct point_sphere_part_search * search) {

   double dot = node->gc_norm_vector[0]*bnd_circle_base_vector[0] +
                node->gc_norm_vector[1]*bnd_circle_base_vector[1] +
                node->gc_norm_vector[2]*bnd_circle_base_vector[2];

   int ret = 0;

   // angle + inc_angle >= M_PI_2
   if (dot <= sin_inc_angle) {

      if (node->flags & U_IS_LEAF) {

         unsigned * node_point_indices = (unsigned*)(node->U);
         unsigned num_node_point_indices = node->U_size;
         double * coordinates_xyz = search->coordinates_xyz;
         for (unsigned i = 0; i < num_node_point_indices; ++i) {
            double cos_angle = coordinates_xyz[3*node_point_indices[i]+0] *
                               bnd_circle_base_vector[0] +
                               coordinates_xyz[3*node_point_indices[i]+1] *
                               bnd_circle_base_vector[1] +
                               coordinates_xyz[3*node_point_indices[i]+2] *
                               bnd_circle_base_vector[2];
            if (cos_angle > cos_inc_angle) return 1;
         }

      } else {
         ret = point_check_bnd_circle(node->U, bnd_circle_base_vector,
                                      sin_inc_angle, cos_inc_angle, search);
      }
   }

   // angle - inc_angle < M_PI_2
   if ((!ret) && (dot > - sin_inc_angle)) {

      if (node->flags & T_IS_LEAF) {

         unsigned * node_point_indices = (unsigned*)(node->T);
         unsigned num_node_point_indices = node->T_size;
         double * coordinates_xyz = search->coordinates_xyz;
         for (unsigned i = 0; i < num_node_point_indices; ++i) {
            double cos_angle = coordinates_xyz[3*node_point_indices[i]+0] *
                               bnd_circle_base_vector[0] +
                               coordinates_xyz[3*node_point_indices[i]+1] *
                               bnd_circle_base_vector[1] +
                               coordinates_xyz[3*node_point_indices[i]+2] *
                               bnd_circle_base_vector[2];
            if (cos_angle > cos_inc_angle) return 1;
         }

      } else {
         ret = point_check_bnd_circle(node->T, bnd_circle_base_vector,
                                      sin_inc_angle, cos_inc_angle, search);
      }
   }

   return ret;
}

void yac_point_sphere_part_search_NN(struct point_sphere_part_search * search,
                                     unsigned num_points,
                                     double * coordinates_xyz,
                                     double * cos_angles,
                                     unsigned ** local_point_ids,
                                     unsigned * local_point_ids_array_size,
                                     unsigned * num_local_point_ids) {

  for (unsigned i = 0; i < num_points; ++i) num_local_point_ids[i] = 0;
  if (cos_angles != NULL)
    for (unsigned i = 0; i < num_points; ++i) cos_angles[i] = -1.0;

  if (search == NULL) return;

  struct point_sphere_part_node * base_node = &(search->base_node);

  unsigned * overlap_leaf_points = NULL;
  unsigned overlap_leaf_points_array_size = 0;
  unsigned num_overlap_leaf_points;

  unsigned total_num_local_point_ids = 0;

  double * cos_distances = NULL;
  unsigned cos_distances_array_size = 0;

  // coarse search
  for (unsigned i = 0; i < num_points; ++i) {

    struct point_sphere_part_node * curr_node = base_node;

    double * curr_coordinates_xyz = coordinates_xyz + i * 3;

    unsigned  * leaf_points = NULL;
    unsigned num_leaf_points = -1;

    // get the matching leaf for the current point
    do {

      double dot = curr_node->gc_norm_vector[0]*curr_coordinates_xyz[0] +
                   curr_node->gc_norm_vector[1]*curr_coordinates_xyz[1] +
                   curr_node->gc_norm_vector[2]*curr_coordinates_xyz[2];

      // angle >= M_PI_2
      if (dot <= 0.0) {

        if (curr_node->flags & U_IS_LEAF) {
          if (curr_node->U_size > 0) {
            leaf_points = (unsigned*)(curr_node->U);
            num_leaf_points = curr_node->U_size;
          } else if (curr_node->flags & T_IS_LEAF) {
            leaf_points = (unsigned*)(curr_node->T);
            num_leaf_points = curr_node->T_size;
          } else curr_node = curr_node->T;
        } else curr_node = curr_node->U;

      } else {

        if (curr_node->flags & T_IS_LEAF) {
          if (curr_node->T_size > 0) {
            leaf_points = (unsigned*)(curr_node->T);
            num_leaf_points = curr_node->T_size;
          } else if (curr_node->flags & U_IS_LEAF) {
            leaf_points = (unsigned*)(curr_node->U);
            num_leaf_points = curr_node->U_size;
          } else curr_node = curr_node->U;
        } else curr_node = curr_node->T;
      }
    } while (num_leaf_points == -1);

    assert(num_leaf_points > 0);

    // compute the best point of the found leaf
    // since the angle is in the interval [-Pi;Pi], we can use the dot product
    // instead of computing the exact angle (the higher the dot product the
    // smaller the angle)
    double cos_distance =
      search->coordinates_xyz[0+leaf_points[0]*3]*curr_coordinates_xyz[0] +
      search->coordinates_xyz[1+leaf_points[0]*3]*curr_coordinates_xyz[1] +
      search->coordinates_xyz[2+leaf_points[0]*3]*curr_coordinates_xyz[2];
    for (unsigned j = 1; j < num_leaf_points; ++j) {
      double tmp_cos_distance =
        search->coordinates_xyz[0+leaf_points[j]*3]*curr_coordinates_xyz[0] +
        search->coordinates_xyz[1+leaf_points[j]*3]*curr_coordinates_xyz[1] +
        search->coordinates_xyz[2+leaf_points[j]*3]*curr_coordinates_xyz[2];
      if (tmp_cos_distance > cos_distance) cos_distance = tmp_cos_distance;
    }

    num_overlap_leaf_points = 0;

    // get all leaf points that match the bounding circle of the current point
    // cos(acos(cos_distance)+PI/2) = sin(acos(cos_distance))
    //                              = sqrt(1-cos_distance*cos_distance)
    point_search_bnd_circle(base_node, curr_coordinates_xyz,
                            sqrt(1.0 - MIN(cos_distance * cos_distance, 1.0)),
                            &overlap_leaf_points,
                            &overlap_leaf_points_array_size,
                            &num_overlap_leaf_points);

    assert(num_overlap_leaf_points > 0);

    ENSURE_ARRAY_SIZE(cos_distances, cos_distances_array_size,
                      num_overlap_leaf_points);

    for (unsigned j = 0; j < num_overlap_leaf_points; ++j)
      cos_distances[j] =
        search->coordinates_xyz[0+overlap_leaf_points[j]*3] *
        curr_coordinates_xyz[0] +
        search->coordinates_xyz[1+overlap_leaf_points[j]*3] *
        curr_coordinates_xyz[1] +
        search->coordinates_xyz[2+overlap_leaf_points[j]*3] *
        curr_coordinates_xyz[2];
    double max_cos_distance = cos_distances[0];
    for (unsigned j = 1; j < num_overlap_leaf_points; ++j)
      if (cos_distances[j] > max_cos_distance)
        max_cos_distance = cos_distances[j];
    if (cos_angles != NULL) cos_angles[i] = max_cos_distance;

    ENSURE_ARRAY_SIZE(*local_point_ids, *local_point_ids_array_size,
                      total_num_local_point_ids + num_overlap_leaf_points);

    for (unsigned j = 0; j < num_overlap_leaf_points; ++j) {
      if (cos_distances[j] == max_cos_distance) {
        (*local_point_ids)[total_num_local_point_ids++] =
          overlap_leaf_points[j];
        num_local_point_ids[i]++;
      }
    }
  }

  free(cos_distances);
  free(overlap_leaf_points);
}

static unsigned
get_max_free_depth_mask(struct point_sphere_part_node * node, unsigned mask) {

  unsigned ret = mask;

  if (!(node->flags & U_IS_LEAF))
    ret |= get_max_free_depth_mask(node->U, mask << 1);
  if (!(node->flags & T_IS_LEAF))
    ret |= get_max_free_depth_mask(node->T, mask << 1);

  return ret;
}

// this insertion sort is slightly adjusted to the usage in
// yac_point_sphere_part_search_NNN
static void
inv_insertion_sort_dble(double element, double * a, unsigned * curr_length) {

  unsigned i;
  for (i = 0; i < *curr_length; ++i)
    if (a[i] <= element) break;

  // copy new element into array and move bigger elements one position up
  for (unsigned j = *curr_length; j > i; --j) a[j] = a[j-1];
  a[i] = element;

  // increase array length indicator
  ++(*curr_length);
}

static unsigned get_leaf_points(unsigned * leaf_points,
                                struct point_sphere_part_node * node) {

  unsigned size;

  if (node->flags & U_IS_LEAF) {
    for (unsigned i = 0; i < node->U_size; ++i)
      leaf_points[i] = ((unsigned*)(node->U))[i];
    size = node->U_size;
  } else
    size = get_leaf_points(leaf_points, node->U);

  if (node->flags & T_IS_LEAF) {
    for (unsigned i = 0; i < node->T_size; ++i)
      leaf_points[i+size] = ((unsigned*)(node->T))[i];
    return size + node->T_size;
  } else {
    return size + get_leaf_points(leaf_points + size, node->T);
  }
}

void yac_point_sphere_part_search_NNN(struct point_sphere_part_search * search,
                                      unsigned num_points,
                                      double * coordinates_xyz, unsigned n,
                                      double ** cos_angles,
                                      unsigned * cos_angles_array_size,
                                      unsigned ** local_point_ids,
                                      unsigned * local_point_ids_array_size,
                                      unsigned * num_local_point_ids) {

  if (n == 1) {
    if (cos_angles != NULL)
      ENSURE_ARRAY_SIZE(*cos_angles, *cos_angles_array_size, num_points);
    yac_point_sphere_part_search_NN(search, num_points, coordinates_xyz,
                                    (cos_angles!=NULL)?*cos_angles:NULL,
                                    local_point_ids, local_point_ids_array_size,
                                    num_local_point_ids);

    unsigned total_num_local_points = 0;
    for (unsigned i = 0; i < num_points; ++i)
      total_num_local_points += num_local_point_ids[i];

    if (total_num_local_points > num_points) {

      ENSURE_ARRAY_SIZE(*cos_angles, *cos_angles_array_size,
                        total_num_local_points);

      for (unsigned i = num_points - 1, offset = total_num_local_points - 1;
           i < num_points; i--) {

        for (unsigned j = 0; j < num_local_point_ids[i]; ++j, --offset)
          (*cos_angles)[offset] = (*cos_angles)[i];
      }
    }
    return;
  }

  for (unsigned i = 0; i < num_points; ++i)
    num_local_point_ids[i] = 0;

  if (search == NULL) return;

  struct point_sphere_part_node * base_node = &(search->base_node);

  unsigned * overlap_leaf_points = NULL;
  unsigned overlap_leaf_points_array_size = 0;
  unsigned num_overlap_leaf_points;

  unsigned total_num_local_point_ids = 0;

  double * cos_distances = NULL;
  unsigned cos_distances_array_size = 0;

  double * n_cos_distances = malloc((n + 1) * sizeof(*n_cos_distances));

  unsigned  * leaf_points = NULL;
  unsigned leaf_points_array_size = 0;

  // coarse search
  for (unsigned i = 0; i < num_points; ++i) {

    double * curr_coordinates_xyz = coordinates_xyz + i * 3;

    struct point_sphere_part_node * curr_node = base_node;
    struct point_sphere_part_node * next_node;
    unsigned next_node_size, next_node_is_leaf;

    unsigned num_leaf_points = 0;

    // get the matching leaf for the current point
    do {

      double dot = curr_node->gc_norm_vector[0]*coordinates_xyz[0] +
                   curr_node->gc_norm_vector[1]*coordinates_xyz[1] +
                   curr_node->gc_norm_vector[2]*coordinates_xyz[2];

      // angle >= M_PI_2
      if (dot <= 0.0) {
        next_node = curr_node->U;
        next_node_size = curr_node->U_size;
        next_node_is_leaf = curr_node->flags & U_IS_LEAF;
      } else {
        next_node = curr_node->T;
        next_node_size = curr_node->T_size;
        next_node_is_leaf = curr_node->flags & T_IS_LEAF;
      }

      // if the next node is too small
      if (next_node_size < n) {
        break;
      // if the next node is a leaf
      } else if (next_node_is_leaf) {
        ENSURE_ARRAY_SIZE(leaf_points, leaf_points_array_size, next_node_size);
        memcpy(leaf_points, next_node, next_node_size * sizeof(*leaf_points));
        num_leaf_points = next_node_size;
        break;
      // go down one level in the tree
      } else {
        curr_node = next_node;
      }
    } while (1);

    if (num_leaf_points == 0) {
      num_leaf_points = curr_node->U_size + curr_node->T_size;
      ENSURE_ARRAY_SIZE(leaf_points, leaf_points_array_size, num_leaf_points);
      num_leaf_points = get_leaf_points(leaf_points, curr_node);
    }

    // compute the n best points of the found leaf points
    // since the angle is in the interval [-Pi;Pi], we can use the dot product
    // instead of computing the exact angle (the higher the dot product the
    // smaller the angle)
    n_cos_distances[0] =
      search->coordinates_xyz[0+leaf_points[0]*3]*curr_coordinates_xyz[0] +
      search->coordinates_xyz[1+leaf_points[0]*3]*curr_coordinates_xyz[1] +
      search->coordinates_xyz[2+leaf_points[0]*3]*curr_coordinates_xyz[2];
    unsigned num_distances = 1;
    for (unsigned j = 1; j < num_leaf_points; ++j) {
      double cos_distance =
        search->coordinates_xyz[0+leaf_points[j]*3]*curr_coordinates_xyz[0] +
        search->coordinates_xyz[1+leaf_points[j]*3]*curr_coordinates_xyz[1] +
        search->coordinates_xyz[2+leaf_points[j]*3]*curr_coordinates_xyz[2];
      if ((num_distances < n) ||
          (n_cos_distances[num_distances-1] < cos_distance)) {
        inv_insertion_sort_dble(cos_distance, n_cos_distances, &num_distances);
        // we only need n points
        if (num_distances > n) num_distances = n;
      }
    }
    double max_cos_distance = n_cos_distances[num_distances-1];

    num_overlap_leaf_points = 0;

    // get all leaf points that match the bounding circle of the current point
    // cos(acos(cos_distance)+PI/2) = sin(acos(cos_distance))
    //                              = sqrt(1-cos_distance*cos_distance)
    point_search_bnd_circle(base_node, curr_coordinates_xyz,
                            sqrt(1.0 - MIN(max_cos_distance * max_cos_distance,
                                 1.0)), &overlap_leaf_points,
                            &overlap_leaf_points_array_size,
                            &num_overlap_leaf_points);

    assert(num_overlap_leaf_points > 0);

    ENSURE_ARRAY_SIZE(cos_distances, cos_distances_array_size,
                      num_overlap_leaf_points);

    for (unsigned j = 0; j < num_overlap_leaf_points; ++j)
      cos_distances[j] =
        search->coordinates_xyz[0+overlap_leaf_points[j]*3] *
        curr_coordinates_xyz[0] +
        search->coordinates_xyz[1+overlap_leaf_points[j]*3] *
        curr_coordinates_xyz[1] +
        search->coordinates_xyz[2+overlap_leaf_points[j]*3] *
        curr_coordinates_xyz[2];
    n_cos_distances[0] = cos_distances[0];
    num_distances = 1;
    for (unsigned j = 1; j < num_overlap_leaf_points; ++j) {
      if ((num_distances < n) ||
          (n_cos_distances[num_distances-1] < cos_distances[j])) {
        inv_insertion_sort_dble(cos_distances[j], n_cos_distances,
                                &num_distances);
        // we only need n points
        if (num_distances > n) num_distances = n;
      }
    }
    max_cos_distance = n_cos_distances[num_distances-1];

    ENSURE_ARRAY_SIZE(*local_point_ids, *local_point_ids_array_size,
                      total_num_local_point_ids + num_overlap_leaf_points);
    if (cos_angles != NULL)
      ENSURE_ARRAY_SIZE(*cos_angles, *cos_angles_array_size,
                        total_num_local_point_ids + num_overlap_leaf_points);

    for (unsigned j = 0; j < num_overlap_leaf_points; ++j) {
      if (cos_distances[j] >= max_cos_distance) {
        if (cos_angles != NULL)
          (*cos_angles)[total_num_local_point_ids] = cos_distances[j];
        (*local_point_ids)[total_num_local_point_ids++] =
          overlap_leaf_points[j];
        num_local_point_ids[i]++;
      }
    }
  }

  free(leaf_points);
  free(cos_distances);
  free(n_cos_distances);
  free(overlap_leaf_points);
}

int yac_point_sphere_part_search_bnd_circle_contains_points(
  struct point_sphere_part_search * search,
  struct reduced_bounding_circle circle) {

  // cos(acos(cos_angle)+PI/2) = sin(acos(cos_angle))
  //                           = sqrt(1-cos_angle*cos_angle)
  return
    point_check_bnd_circle(
      &(search->base_node), circle.base_vector,
      sqrt(1.0 - MIN(circle.cos_inc_angle * circle.cos_inc_angle, 1.0)),
      circle.cos_inc_angle, search);
}

static void free_point_sphere_part_tree (struct point_sphere_part_node * tree) {

   if ((tree->flags & U_IS_LEAF) == 0)
      free_point_sphere_part_tree(tree->U);
   free(tree->U);

   if ((tree->flags & T_IS_LEAF) == 0)
      free_point_sphere_part_tree(tree->T);
   free(tree->T);
}

void yac_delete_point_sphere_part_search(
   struct point_sphere_part_search * search) {

   if (search == NULL) return;

   free_point_sphere_part_tree(&(search->base_node));
   free(search->coordinates_xyz);
   free(search);
}
