#ifndef TEST_FUNCTION_H
#define TEST_FUNCTION_H

void compute_field_data(
  double * vertices, unsigned num_vertices, double * f_v);

void compute_field_data_derivative(
  double * vertices, unsigned num_vertices, double * u, double * df_v);
void compute_field_data_derivative_2(
  double * vertices, unsigned num_vertices, double * u, double * df_v);

#endif // TEST_FUNCTION_H
