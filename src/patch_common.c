#include <stdlib.h>
#include <string.h>
#if !defined HAVE_LAPACKE && !defined HAVE_MKL_LAPACKE
#  include <assert.h>
#  if defined HAVE_CLAPACK
#    include <clapack.h>
#  else
#    include "cfortran.h"
#  endif
#elif defined HAVE_MKL_LAPACKE
#include <mkl_lapacke.h>
#include <mkl.h>
#else
#include <lapacke.h>
#include <cblas.h>
#endif

#include "patch_common.h"
#include "utils.h"

// compute the spherical barycentric coordinates of the given vertices with
// respect to the given triangle
void compute_sb_coords(double * vertices, unsigned num_vertices,
                       double * triangle, double * sb_coords) {

  double A[3][3];
  lapack_int n = 3, nrhs = num_vertices, lda = n, ldx = n, ipiv[3], info;
  memcpy(sb_coords, vertices, 3 * num_vertices * sizeof(*vertices));
  memcpy(&A[0][0], triangle, 9 * sizeof(*triangle));

  // for a vertex v the spherical barycentric coordinates b are defined as
  // follows
  // A * b = v
  // where: A is the matrix consisting of the vertex coordinates of the three
  // corners of the triangle
  // we compute b by solving this linear system using LAPACK
  if (0 < (info = LAPACKE_dgesv(LAPACK_COL_MAJOR, n, nrhs, &A[0][0], lda,
                                &ipiv[0], sb_coords, ldx)))
    abort_message(
      "ERROR: internal error (could not solve linear 3x3 system)\n",
      __FILE__, __LINE__);
}

void compute_patch_coefficients(
  unsigned num_coeffs, double * polynomials, double * f_v,
  unsigned num_vertices, double * coefficients) {

  // the patch is defined by:
  // p(v) = SUM(c_ijk*Bd_ijk(v))
  //   for all i+j+k = d
  // this can also be written as:
  // p(v) = b(v)^T * c
  //
  // we want to determine c_ijk so that
  // SUM((p(v_i)-f(v_i))^2) is minimal
  //   for i = 1 to num_vertices
  // we compute c using the lapack routine LAPACK_dgels
  // it solves over- or underdetermined linear system of the form:
  // B * x = y
  // in our case we choose:
  //   B to be a matrix containing the spherical Bernstein basis polynomials,
  //   x to be a vector containing c_ijk and
  //   y to be a vector containg f(v_i)

  lapack_int m = num_vertices, n = num_coeffs, nrhs = 1, lda = m,
             ldb = MAX(m, n), info;
  double * B = malloc(m * n * sizeof(*B));
  double * c = malloc(ldb * sizeof(*c));
  memcpy(B, polynomials, m * n * sizeof(*B));
  memcpy(c, f_v, num_vertices * sizeof(*c));

  double * s = malloc(MIN(num_coeffs, num_vertices) * sizeof(*s));;
  double rcond = 1e-9;
  lapack_int rank;

  // we cannot use LAPACKE_dgels because we may not have a full-rank matrix
  // if (0 < (info = LAPACKE_dgels(LAPACK_COL_MAJOR, 'N', m, n, nrhs, B, lda, c, ldb)))
    // abort_message(
      // "ERROR: internal error (could not determine c_ijk)\n",
      // __FILE__, __LINE__);
  if (0 < (info = LAPACKE_dgelsd(LAPACK_COL_MAJOR, m, n, nrhs, B, lda, c, ldb, s, rcond, &rank)))
    abort_message(
      "ERROR: internal error (could not determine c_ijk)\n",
      __FILE__, __LINE__);

  free(s);
  memcpy(coefficients, c, num_coeffs * sizeof(*c));
  free(c);
  free(B);
}

static void inverse ( double* A, int n ) {

  int ipiv[n+1];
  int info;
  int lwork = n*n;

  if ( n > 512 )
    abort_message("internal ERROR: to much memory required",
                  __FILE__, __LINE__);

  double work[lwork];

  for (unsigned i = 0; i < n+1; ++i) ipiv[i] = 0;
  for (unsigned i = 0; i < lwork; ++i) work[i] = 0;

  info = LAPACKE_dgetrf(LAPACK_COL_MAJOR, n, n, A, n, ipiv);
  if ( info != 0 )
    abort_message("internal ERROR: dgetrf", __FILE__, __LINE__);

  info = LAPACKE_dgetri_work(LAPACK_COL_MAJOR, n, A, n, ipiv, work, lwork);
  if ( info != 0 )
    abort_message("internal ERROR: dgetri", __FILE__, __LINE__);
}

static void inverse_syt(double * A, int n) {

  lapack_int ipiv[n];

  if ( n > 512 )
    abort_message("internal ERROR: to much memory required",
                  __FILE__, __LINE__);

  if (0 != LAPACKE_dsytrf(LAPACK_COL_MAJOR, 'L', n , A , n, &ipiv[0]))
    abort_message("internal ERROR: dsytrf", __FILE__, __LINE__);

  if (0 != LAPACKE_dsytri(LAPACK_COL_MAJOR, 'L', n , A, n, &ipiv[0]))
    abort_message("internal ERROR: dsytri", __FILE__, __LINE__);

  for (unsigned i = 0; i < n; ++i)
    for (unsigned j = i + 1; j < n; ++j)
      A[j*n + i] = A[i*n + j];
}

static void inverse_simple(double * A, int n) {

  long double A_inv[n][n];
  long double A_temp[n*n];

  if ( n > 512 )
    abort_message("internal ERROR: to much memory required",
                  __FILE__, __LINE__);

  for (int i = 0; i < n; ++i) for (int j = 0; j < n; ++j) A_inv[i][j] = 0;
  for (int i = 0; i < n; ++i) A_inv[i][i] = 1;
  for (int i = 0; i < n*n; ++i) A_temp[i] = A[i];

  for(int i = 0; i < n; i++){
    for(int j = 0; j < n; j++){
      if(i!=j){
        long double ratio = A_temp[j*n+i]/A_temp[i*n+i];
        for(int k = 0; k < n; k++){
            A_temp[j*n+k] -= ratio * A_temp[i*n+k];
            A_inv[j][k] -= ratio * A_inv[i][k];
        }
      }
    }
  }

  for(int i = 0; i < n; i++){
    long double a = A_temp[i*n+i];
    for(int j = 0; j < n; j++){
      A_temp[i*n+j] /= a;
      A_inv[i][j] /= a;
    }
  }

  for (int i = 0; i < n; ++i)
    for (int j = 0; j < n; ++j)
      A[i*n+j] = A_inv[i][j];
}

void compute_patch_weights(
  unsigned num_coeffs, unsigned num_patch_vertices, double * patch_polynomials,
  unsigned num_eval_vertices, double * eval_polynomials, double * weights) {

  double * C = malloc(num_coeffs * num_coeffs * sizeof(*C));
  double * D = malloc(num_coeffs * num_patch_vertices * sizeof(*D));

  for (unsigned i = 0; i < num_coeffs * num_coeffs; ++i) C[i] = 0.0;
  for (unsigned i = 0; i < num_coeffs * num_patch_vertices; ++i) D[i] = 0.0;
  for (unsigned i = 0; i < num_eval_vertices * num_patch_vertices; ++i) weights[i] = 0.0;

  int m = num_coeffs, n = num_coeffs, k = num_patch_vertices;
  double alpha = 1.0, beta = 0.0;

  // P_p^T * P_p
  cblas_dgemm(CblasColMajor, CblasTrans, CblasNoTrans,
              m, n, k, alpha, patch_polynomials, k, patch_polynomials, k,
              beta, C, m);

  // (P_p^T * P_p)^-1
  inverse_syt(C, num_coeffs);
  // inverse(C, num_coeffs);
  // inverse_simple(C, num_coeffs);

  m = num_coeffs;
  n = num_patch_vertices;
  k = num_coeffs;

  // (P_p^T * P_p)^-1 * P^T
  cblas_dgemm(CblasColMajor, CblasNoTrans, CblasTrans,
              m, n, k, alpha, C, m, patch_polynomials, n, beta, D, m);

  m = num_eval_vertices;
  n = num_patch_vertices;
  k = num_coeffs;

  // P_e * (P_p^T * P_p)^-1 * P^T
  cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans,
              m, n, k, alpha, eval_polynomials, m, D, k, beta, weights, m);

  free(D);
  free(C);
}

void evaluate_patch_weights(
  unsigned num_patch_vertices, unsigned num_eval_vertices,
  double * weights, double * f, double * p) {

  for (unsigned i = 0; i < num_eval_vertices; ++i) p[i] = 0.0;

  int m = num_eval_vertices, n = num_patch_vertices;
  double alpha = 1.0, beta = 0.0;

  cblas_dgemv(
    CblasColMajor, CblasNoTrans, m, n, alpha, weights, m, f, 1, beta, p, 1);
}

void evaluate_patch(
  double * vertices, size_t num_vertices, unsigned num_coeffs,
  double * restrict coefficients, double * restrict polynomials,
  double * restrict p) {

  for (size_t vertex_idx = 0; vertex_idx < num_vertices; ++vertex_idx)
    p[vertex_idx] = 0.0;

  // computes B*c=p
  // where: B is a matrix containing the base polynomials
  //        c is a vector containg the coefficients of the patch
  for (unsigned i = 0; i < num_coeffs; ++i) {
    double curr_coeff = coefficients[i];
    for (size_t vertex_idx = 0; vertex_idx < (size_t)num_vertices; ++vertex_idx)
      p[vertex_idx] += curr_coeff * polynomials[i * num_vertices + vertex_idx];
  }
}
