#include <string.h>

#include "weight_store.h"

void weight_vector_init(struct weight_vector * vector) {

  vector->weights = NULL;
  vector->idx = NULL;
  vector->n = 0;
}

void weight_vector_add_weight(
  struct weight_vector * vector, double weight, size_t idx) {

  for (size_t i = 0; i < vector->n; ++i) {
    if (vector->idx[i] == idx) {
      vector->weights[i] += weight;
      return;
    }
  }

  vector->weights =
    realloc(vector->weights, (vector->n+1) * sizeof(*(vector->weights)));
  vector->idx =
    realloc(vector->idx, (vector->n+1) * sizeof(*(vector->idx)));

  vector->weights[vector->n] = weight;
  vector->idx[vector->n] = idx;

  vector->n += 1;
}

void weight_vector_mult_scalar(struct weight_vector * vector, double scalar) {

  for (size_t i = 0 ; i < vector->n; ++i)
    vector->weights[i] *= scalar;
}

void weight_vector_add_weight_vector(
  struct weight_vector * vector_a, struct weight_vector * vector_b,
  double factor) {

  if (vector_b->n == 0) return;

  vector_a->idx =
    realloc(vector_a->idx, (vector_a->n + vector_b->n) *
            sizeof(*(vector_a->idx)));
  memcpy(vector_a->idx + vector_a->n, vector_b->idx,
         vector_b->n * sizeof(*(vector_a->idx)));
  vector_a->weights =
    realloc(vector_a->weights, (vector_a->n + vector_b->n) *
            sizeof(*(vector_a->weights)));
  memcpy(vector_a->weights + vector_a->n, vector_b->weights,
         vector_b->n * sizeof(*(vector_a->weights)));
  for (size_t i = vector_a->n; i < vector_a->n + vector_b->n; ++i)
    vector_a->weights[i] *= factor;
  vector_a->n += vector_b->n;
}

double weight_vector_mult_vector(
  struct weight_vector * vector, double * f) {

  double sum = 0.0;

  for (size_t i = 0; i < vector->n; ++i)
    sum += vector->weights[i] * f[vector->idx[i]];

  return sum;
}

struct weights {
  size_t idx, pos;
  double weight;
};

static int compare_weights_idx (const void * a, const void * b) {

  int ret = (((struct weights*)a)->idx > ((struct weights*)b)->idx) -
            (((struct weights*)a)->idx < ((struct weights*)b)->idx);
  if (ret) return ret;

  return (((struct weights*)a)->pos > ((struct weights*)b)->pos) -
         (((struct weights*)a)->pos < ((struct weights*)b)->pos);
}

static int compare_weights_pos (const void * a, const void * b) {

  return (((struct weights*)a)->pos > ((struct weights*)b)->pos) -
         (((struct weights*)a)->pos < ((struct weights*)b)->pos);
}

void weight_vector_compact(struct weight_vector * vector) {

  if (vector->n == 0) return;

  struct weights * sorted_weights = malloc(vector->n * sizeof(*sorted_weights));
  for (size_t i = 0; i < vector->n; ++i) {
    sorted_weights[i].idx = vector->idx[i];
    sorted_weights[i].pos = i;
    sorted_weights[i].weight = vector->weights[i];
  }

  qsort(sorted_weights, vector->n, sizeof(*sorted_weights),
        compare_weights_idx);

  size_t prev_pos = 0;
  for (size_t prev_idx = sorted_weights[0].idx, i = 1; i < vector->n; ++i) {

    size_t curr_idx = sorted_weights[i].idx;
    if (curr_idx == prev_idx) {
      sorted_weights[prev_pos].weight += sorted_weights[i].weight;
    } else {
      ++prev_pos;
      prev_idx = curr_idx;
      if (prev_pos != i) sorted_weights[prev_pos] = sorted_weights[i];
    }
  }

  size_t new_n = prev_pos + 1;

  if (vector->n != new_n) {

    qsort(sorted_weights, new_n, sizeof(*sorted_weights), compare_weights_pos);

    vector->weights =
      realloc(vector->weights, new_n * sizeof(*(vector->weights)));
    vector->idx =
      realloc(vector->idx, new_n * sizeof(*(vector->idx)));
    vector->n = new_n;
    for (size_t i = 0; i < new_n; ++i) {
      vector->weights[i] = sorted_weights[i].weight;
      vector->idx[i] = sorted_weights[i].idx;
    }
  }

  free(sorted_weights);
}

void weight_vector_free(struct weight_vector * vector) {
  free(vector->idx);
  free(vector->weights);
  vector->idx = NULL;
  vector->weights = NULL;
  vector->n = 0;
}

void weight_matrix_mult_vector(
  struct weight_matrix weights, double * f, double * results) {

  for (size_t i = 0; i < weights.n; ++i)
    results[i] = weight_vector_mult_vector(&(weights.weights[i]), f);
}
