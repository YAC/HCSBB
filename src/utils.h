#ifndef UTILS_H
#define UTILS_H

#define MAX(a,b) (((a)>(b))?(a):(b))
#define MIN(a,b) (((a)<(b))?(a):(b))

void abort_message ( const char * text, const char * file, int line );
#endif // UTILS_H

