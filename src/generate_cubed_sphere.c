#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "generate_cubed_sphere.h"
#include "utils.h"

#ifndef  M_PI_2
#define  M_PI_2      1.57079632679489661923132169163975144  /* pi/2 */
#endif
#ifndef  M_PI_4
#define M_PI_4 0.78539816339744830962  /* pi/4 */
#endif

// This routine is based on Mathlab code provided by Mike Hobson and written by
// Mike Rezny (both from MetOffice)
void generate_cubed_sphere_grid_information(
  unsigned n, unsigned * num_cells, unsigned * num_vertices,
  double ** vertices, unsigned ** cell_to_vertex, unsigned ** face_id) {

  if (n < 1)
    abort_message("invalid number of linear subdivisions", __FILE__, __LINE__);

  *num_cells = n * n * 6;
  *num_vertices = *num_cells + 2;

  // allocation of output variables
  *vertices = malloc(3 * *num_vertices * sizeof(**vertices));

  *cell_to_vertex = malloc(*num_cells * 4 * sizeof(**cell_to_vertex));

  *face_id = malloc(*num_cells * sizeof(**face_id));

  // allocation of temporary coordinate variables
  unsigned num_edge_coords = n - 1;
  double * cube_edge_x_vertices = malloc(12 * num_edge_coords * sizeof(*cube_edge_x_vertices));
  double * cube_edge_y_vertices = malloc(12 * num_edge_coords * sizeof(*cube_edge_y_vertices));
  double * cube_edge_z_vertices = malloc(12 * num_edge_coords * sizeof(*cube_edge_z_vertices));

  unsigned num_inner_coords = num_edge_coords * num_edge_coords;
  double * cube_inner_x_vertices = malloc(num_inner_coords * sizeof(*cube_inner_x_vertices));
  double * cube_inner_y_vertices = malloc(num_inner_coords * sizeof(*cube_inner_y_vertices));
  double * cube_inner_z_vertices = malloc(num_inner_coords * sizeof(*cube_inner_z_vertices));

  double * temp_x_vertices = malloc((n + 1) * (n + 1) * sizeof(*temp_x_vertices));
  double * temp_y_vertices = malloc((n + 1) * (n + 1) * sizeof(*temp_y_vertices));
  double * temp_z_vertices = malloc((n + 1) * (n + 1) * sizeof(*temp_z_vertices));

  {
    double * theta = malloc((n + 1) * sizeof(*theta));

    {
      double d = (M_PI_2 / (double)n);
      for (unsigned i = 0; i < n + 1; ++i)
        theta[i] = tan(- M_PI_4 + d * (double)i);
    }

    for (unsigned i = 0; i < n + 1; ++i) {
      for (unsigned j = 0; j < n + 1; ++j) {

        double scale =
          sqrt(1.0 / (theta[i] * theta[i] + theta[j] * theta[j] + 1.0));

        temp_x_vertices[i * (n + 1) + j] = theta[j] * scale;
        temp_y_vertices[i * (n + 1) + j] = theta[i] * scale;
        temp_z_vertices[i * (n + 1) + j] = - scale;
      }
    }
    free(theta);
  }

  // Store the coordinates for the 4 vertices on face 1
  (*vertices)[0+0*3] = temp_x_vertices[ 0 * (n + 1) + 0];
  (*vertices)[1+0*3] = temp_y_vertices[ 0 * (n + 1) + 0];
  (*vertices)[2+0*3] = temp_z_vertices[ 0 * (n + 1) + 0];
  (*vertices)[0+1*3] = temp_x_vertices[ n * (n + 1) + 0];
  (*vertices)[1+1*3] = temp_y_vertices[ n * (n + 1) + 0];
  (*vertices)[2+1*3] = temp_z_vertices[ n * (n + 1) + 0];
  (*vertices)[0+2*3] = temp_x_vertices[ 0 * (n + 1) + n];
  (*vertices)[1+2*3] = temp_y_vertices[ 0 * (n + 1) + n];
  (*vertices)[2+2*3] = temp_z_vertices[ 0 * (n + 1) + n];
  (*vertices)[0+3*3] = temp_x_vertices[ n * (n + 1) + n];
  (*vertices)[1+3*3] = temp_y_vertices[ n * (n + 1) + n];
  (*vertices)[2+3*3] = temp_z_vertices[ n * (n + 1) + n];
  // Store the coordinates for the 4 vertices on face 2
  (*vertices)[0+4*3] =  temp_x_vertices[ 0 * (n + 1) + 0];
  (*vertices)[1+4*3] =  temp_y_vertices[ 0 * (n + 1) + 0];
  (*vertices)[2+4*3] = -temp_z_vertices[ 0 * (n + 1) + 0];
  (*vertices)[0+5*3] =  temp_x_vertices[ n * (n + 1) + 0];
  (*vertices)[1+5*3] =  temp_y_vertices[ n * (n + 1) + 0];
  (*vertices)[2+5*3] = -temp_z_vertices[ n * (n + 1) + 0];
  (*vertices)[0+6*3] =  temp_x_vertices[ 0 * (n + 1) + n];
  (*vertices)[1+6*3] =  temp_y_vertices[ 0 * (n + 1) + n];
  (*vertices)[2+6*3] = -temp_z_vertices[ 0 * (n + 1) + n];
  (*vertices)[0+7*3] =  temp_x_vertices[ n * (n + 1) + n];
  (*vertices)[1+7*3] =  temp_y_vertices[ n * (n + 1) + n];
  (*vertices)[2+7*3] = -temp_z_vertices[ n * (n + 1) + n];

  // Store the coordinates for the edges
  for (unsigned i = 0; i < num_edge_coords; ++i) {
    cube_edge_x_vertices[ 0 * num_edge_coords + i] = temp_x_vertices[(1 + i) * (n + 1) + 0];
    cube_edge_y_vertices[ 0 * num_edge_coords + i] = temp_y_vertices[(1 + i) * (n + 1) + 0];
    cube_edge_z_vertices[ 0 * num_edge_coords + i] = temp_z_vertices[(1 + i) * (n + 1) + 0];
    cube_edge_x_vertices[ 1 * num_edge_coords + i] = temp_x_vertices[0 * (n + 1) + (1 + i)];
    cube_edge_y_vertices[ 1 * num_edge_coords + i] = temp_y_vertices[0 * (n + 1) + (1 + i)];
    cube_edge_z_vertices[ 1 * num_edge_coords + i] = temp_z_vertices[0 * (n + 1) + (1 + i)];
    cube_edge_x_vertices[ 2 * num_edge_coords + i] = temp_x_vertices[n * (n + 1) + (1 + i)];
    cube_edge_y_vertices[ 2 * num_edge_coords + i] = temp_y_vertices[n * (n + 1) + (1 + i)];
    cube_edge_z_vertices[ 2 * num_edge_coords + i] = temp_z_vertices[n * (n + 1) + (1 + i)];
    cube_edge_x_vertices[ 3 * num_edge_coords + i] = temp_x_vertices[(1 + i) * (n + 1) + n];
    cube_edge_y_vertices[ 3 * num_edge_coords + i] = temp_y_vertices[(1 + i) * (n + 1) + n];
    cube_edge_z_vertices[ 3 * num_edge_coords + i] = temp_z_vertices[(1 + i) * (n + 1) + n];
    cube_edge_x_vertices[ 4 * num_edge_coords + i] =  temp_x_vertices[(1 + i) * (n + 1) + 0];
    cube_edge_y_vertices[ 4 * num_edge_coords + i] =  temp_y_vertices[(1 + i) * (n + 1) + 0];
    cube_edge_z_vertices[ 4 * num_edge_coords + i] = -temp_z_vertices[(1 + i) * (n + 1) + 0];
    cube_edge_x_vertices[ 5 * num_edge_coords + i] =  temp_x_vertices[0 * (n + 1) + (1 + i)];
    cube_edge_y_vertices[ 5 * num_edge_coords + i] =  temp_y_vertices[0 * (n + 1) + (1 + i)];
    cube_edge_z_vertices[ 5 * num_edge_coords + i] = -temp_z_vertices[0 * (n + 1) + (1 + i)];
    cube_edge_x_vertices[ 6 * num_edge_coords + i] =  temp_x_vertices[n * (n + 1) + (1 + i)];
    cube_edge_y_vertices[ 6 * num_edge_coords + i] =  temp_y_vertices[n * (n + 1) + (1 + i)];
    cube_edge_z_vertices[ 6 * num_edge_coords + i] = -temp_z_vertices[n * (n + 1) + (1 + i)];
    cube_edge_x_vertices[ 7 * num_edge_coords + i] =  temp_x_vertices[(1 + i) * (n + 1) + n];
    cube_edge_y_vertices[ 7 * num_edge_coords + i] =  temp_y_vertices[(1 + i) * (n + 1) + n];
    cube_edge_z_vertices[ 7 * num_edge_coords + i] = -temp_z_vertices[(1 + i) * (n + 1) + n];
    cube_edge_x_vertices[ 8 * num_edge_coords + i] =  temp_z_vertices[0 * (n + 1) + (1 + i)];
    cube_edge_y_vertices[ 8 * num_edge_coords + i] =  temp_y_vertices[0 * (n + 1) + (1 + i)];
    cube_edge_z_vertices[ 8 * num_edge_coords + i] =  temp_x_vertices[0 * (n + 1) + (1 + i)];
    cube_edge_x_vertices[ 9 * num_edge_coords + i] =  temp_z_vertices[n * (n + 1) + (1 + i)];
    cube_edge_y_vertices[ 9 * num_edge_coords + i] =  temp_y_vertices[n * (n + 1) + (1 + i)];
    cube_edge_z_vertices[ 9 * num_edge_coords + i] =  temp_x_vertices[n * (n + 1) + (1 + i)];
    cube_edge_x_vertices[10 * num_edge_coords + i] = -temp_z_vertices[0 * (n + 1) + (1 + i)];
    cube_edge_y_vertices[10 * num_edge_coords + i] =  temp_y_vertices[0 * (n + 1) + (1 + i)];
    cube_edge_z_vertices[10 * num_edge_coords + i] =  temp_x_vertices[0 * (n + 1) + (1 + i)];
    cube_edge_x_vertices[11 * num_edge_coords + i] = -temp_z_vertices[n * (n + 1) + (1 + i)];
    cube_edge_y_vertices[11 * num_edge_coords + i] =  temp_y_vertices[n * (n + 1) + (1 + i)];
    cube_edge_z_vertices[11 * num_edge_coords + i] =  temp_x_vertices[n * (n + 1) + (1 + i)];
  }
  
  // Move the 12 edges to the final vertices array
  unsigned Estart = 9 - 1;
  unsigned Eend = Estart + 12 * num_edge_coords;
  for (unsigned i = 0; i < 12 * num_edge_coords; ++i) {
    (*vertices)[0+3*(i + Estart)] = cube_edge_x_vertices[i];
    (*vertices)[1+3*(i + Estart)] = cube_edge_y_vertices[i];
    (*vertices)[2+3*(i + Estart)] = cube_edge_z_vertices[i];
  }

  free(cube_edge_x_vertices);
  free(cube_edge_y_vertices);
  free(cube_edge_z_vertices);

  // store the internal vertices for face 1
  for (unsigned i = 0; i < num_edge_coords; ++i) {
    for (unsigned j = 0; j < num_edge_coords; ++j) {

      cube_inner_x_vertices[i * num_edge_coords + j] = temp_x_vertices[(1 + i) * (n + 1) + (1 + j)];
      cube_inner_y_vertices[i * num_edge_coords + j] = temp_y_vertices[(1 + i) * (n + 1) + (1 + j)];
      cube_inner_z_vertices[i * num_edge_coords + j] = temp_z_vertices[(1 + i) * (n + 1) + (1 + j)];
    }
  }

  // Move face 1 to final Vertices array
  unsigned Fstart  = Eend;
  unsigned Fend    = Fstart + num_inner_coords;
  for (unsigned i = 0; i < num_inner_coords; ++i) {
    (*vertices)[0+3*(i + Fstart)] = cube_inner_x_vertices[i];
    (*vertices)[1+3*(i + Fstart)] = cube_inner_y_vertices[i];
    (*vertices)[2+3*(i + Fstart)] = cube_inner_z_vertices[i];
  }

  // store the internal vertices for face 2
  for (unsigned i = 0; i < num_edge_coords; ++i) {
    for (unsigned j = 0; j < num_edge_coords; ++j) {

      cube_inner_x_vertices[i * num_edge_coords + j] =  temp_x_vertices[(1 + i) * (n + 1) + (1 + j)];
      cube_inner_y_vertices[i * num_edge_coords + j] =  temp_y_vertices[(1 + i) * (n + 1) + (1 + j)];
      cube_inner_z_vertices[i * num_edge_coords + j] = -temp_z_vertices[(1 + i) * (n + 1) + (1 + j)];
    }
  }

  // Move face 2 to final Vertices array
  Fstart += num_inner_coords;
  Fend   += num_inner_coords;
  for (unsigned i = 0; i < num_inner_coords; ++i) {
    (*vertices)[0+3*(i + Fstart)] = cube_inner_x_vertices[i];
    (*vertices)[1+3*(i + Fstart)] = cube_inner_y_vertices[i];
    (*vertices)[2+3*(i + Fstart)] = cube_inner_z_vertices[i];
  }

  // store the internal vertices for face 3
  for (unsigned i = 0; i < num_edge_coords; ++i) {
    for (unsigned j = 0; j < num_edge_coords; ++j) {

      cube_inner_x_vertices[i * num_edge_coords + j] = temp_z_vertices[(1 + i) * (n + 1) + (1 + j)];
      cube_inner_y_vertices[i * num_edge_coords + j] = temp_y_vertices[(1 + i) * (n + 1) + (1 + j)];
      cube_inner_z_vertices[i * num_edge_coords + j] = temp_x_vertices[(1 + i) * (n + 1) + (1 + j)];
    }
  }

  // Move face 3 to final Vertices array
  Fstart += num_inner_coords;
  Fend   += num_inner_coords;
  for (unsigned i = 0; i < num_inner_coords; ++i) {
    (*vertices)[0+3*(i + Fstart)] = cube_inner_x_vertices[i];
    (*vertices)[1+3*(i + Fstart)] = cube_inner_y_vertices[i];
    (*vertices)[2+3*(i + Fstart)] = cube_inner_z_vertices[i];
  }

  // store the internal vertices for face 4
  for (unsigned i = 0; i < num_edge_coords; ++i) {
    for (unsigned j = 0; j < num_edge_coords; ++j) {

      cube_inner_x_vertices[i * num_edge_coords + j] = -temp_z_vertices[(1 + i) * (n + 1) + (1 + j)];
      cube_inner_y_vertices[i * num_edge_coords + j] =  temp_y_vertices[(1 + i) * (n + 1) + (1 + j)];
      cube_inner_z_vertices[i * num_edge_coords + j] =  temp_x_vertices[(1 + i) * (n + 1) + (1 + j)];
    }
  }

  // Move face 4 to final Vertices array
  Fstart += num_inner_coords;
  Fend   += num_inner_coords;
  for (unsigned i = 0; i < num_inner_coords; ++i) {
    (*vertices)[0+3*(i + Fstart)] = cube_inner_x_vertices[i];
    (*vertices)[1+3*(i + Fstart)] = cube_inner_y_vertices[i];
    (*vertices)[2+3*(i + Fstart)] = cube_inner_z_vertices[i];
  }

  // store the internal vertices for face 5
  for (unsigned i = 0; i < num_edge_coords; ++i) {
    for (unsigned j = 0; j < num_edge_coords; ++j) {

      cube_inner_x_vertices[i * num_edge_coords + j] = temp_x_vertices[(1 + i) * (n + 1) + (1 + j)];
      cube_inner_y_vertices[i * num_edge_coords + j] = temp_z_vertices[(1 + i) * (n + 1) + (1 + j)];
      cube_inner_z_vertices[i * num_edge_coords + j] = temp_y_vertices[(1 + i) * (n + 1) + (1 + j)];
    }
  }

  // Move face 5 to final Vertices array
  Fstart += num_inner_coords;
  Fend   += num_inner_coords;
  for (unsigned i = 0; i < num_inner_coords; ++i) {
    (*vertices)[0+3*(i + Fstart)] = cube_inner_x_vertices[i];
    (*vertices)[1+3*(i + Fstart)] = cube_inner_y_vertices[i];
    (*vertices)[2+3*(i + Fstart)] = cube_inner_z_vertices[i];
  }

  // store the internal vertices for face 6
  for (unsigned i = 0; i < num_edge_coords; ++i) {
    for (unsigned j = 0; j < num_edge_coords; ++j) {

      cube_inner_x_vertices[i * num_edge_coords + j] =  temp_x_vertices[(1 + i) * (n + 1) + (1 + j)];
      cube_inner_y_vertices[i * num_edge_coords + j] = -temp_z_vertices[(1 + i) * (n + 1) + (1 + j)];
      cube_inner_z_vertices[i * num_edge_coords + j] =  temp_y_vertices[(1 + i) * (n + 1) + (1 + j)];
    }
  }

  // Move face 6 to final Vertices array
  Fstart += num_inner_coords;
  Fend   += num_inner_coords;
  for (unsigned i = 0; i < num_inner_coords; ++i) {
    (*vertices)[0+3*(i + Fstart)] = cube_inner_x_vertices[i];
    (*vertices)[1+3*(i + Fstart)] = cube_inner_y_vertices[i];
    (*vertices)[2+3*(i + Fstart)] = cube_inner_z_vertices[i];
  }

  free(temp_x_vertices);
  free(temp_y_vertices);
  free(temp_z_vertices);
  free(cube_inner_x_vertices);
  free(cube_inner_y_vertices);
  free(cube_inner_z_vertices);

  for (unsigned i = 0; i < 6; ++i)
    for (unsigned j = 0; j < n * n; ++j)
      (*face_id)[i * n * n + j] = i + 1;

  unsigned * edge_vertices = malloc(num_edge_coords * 12 * sizeof(*edge_vertices));

  Fstart = Estart + 12 * num_edge_coords;

  for (unsigned i = 0; i < num_edge_coords * 12; ++i) edge_vertices[i] = i + Estart;

  unsigned * F = malloc((n + 1) * (n + 1) * sizeof(*F));
  unsigned cell_to_vertex_offset = 0;

// Calculate the indices for all the vertices on face 1
//        v2-----e3-----v4
//        |              |
//        e1     f1     e4
//        |              |
//        v1-----e2-----v3

  F[0] = 0; F[n] = 2; F[n * (n + 1) + 0] = 1; F[n * (n + 1) + n] = 3;
  for (unsigned i = 0; i < num_edge_coords; ++i) {
    F[i + 1] =                 edge_vertices[1 * num_edge_coords + i];
    F[(i + 1) * (n + 1)] =     edge_vertices[0 * num_edge_coords + i];
    F[(i + 1) * (n + 1) + n] = edge_vertices[3 * num_edge_coords + i];
    F[n * (n + 1) + i + 1] =   edge_vertices[2 * num_edge_coords + i];
  }

  for (unsigned i = 0; i < n-1; ++i)
    for (unsigned j = 0; j < n-1; ++j)
      F[(i + 1) * (n + 1) + (j + 1)] = Fstart + i * (n - 1) + j;

  for (unsigned i = 0; i < n; ++i) {
    for (unsigned j = 0; j < n; ++j) {
      (*cell_to_vertex)[cell_to_vertex_offset++] = F[(i + 0) * (n + 1) + (j + 0)];
      (*cell_to_vertex)[cell_to_vertex_offset++] = F[(i + 1) * (n + 1) + (j + 0)];
      (*cell_to_vertex)[cell_to_vertex_offset++] = F[(i + 1) * (n + 1) + (j + 1)];
      (*cell_to_vertex)[cell_to_vertex_offset++] = F[(i + 0) * (n + 1) + (j + 1)];
    }
  }

// Calculate the indices for all the vertices on face 2
//        v6-----e7-----v8
//        |              |
//        e5     f2     e8
//        |              |
//        v5-----e6-----v7

  F[0] = 4; F[n] = 6; F[n * (n + 1) + 0] = 5; F[n * (n + 1) + n] = 7;
  for (unsigned i = 0; i < num_edge_coords; ++i) {
    F[i + 1] =                 edge_vertices[5 * num_edge_coords + i];
    F[(i + 1) * (n + 1)] =     edge_vertices[4 * num_edge_coords + i];
    F[(i + 1) * (n + 1) + n] = edge_vertices[7 * num_edge_coords + i];
    F[n * (n + 1) + i + 1] =   edge_vertices[6 * num_edge_coords + i];
  }

  for (unsigned i = 0; i < n-1; ++i)
    for (unsigned j = 0; j < n-1; ++j)
      F[(i + 1) * (n + 1) + (j + 1)] += (n - 1) * (n - 1);

  for (unsigned i = 0; i < n; ++i) {
    for (unsigned j = 0; j < n; ++j) {
      (*cell_to_vertex)[cell_to_vertex_offset++] = F[(i + 0) * (n + 1) + (j + 0)];
      (*cell_to_vertex)[cell_to_vertex_offset++] = F[(i + 1) * (n + 1) + (j + 0)];
      (*cell_to_vertex)[cell_to_vertex_offset++] = F[(i + 1) * (n + 1) + (j + 1)];
      (*cell_to_vertex)[cell_to_vertex_offset++] = F[(i + 0) * (n + 1) + (j + 1)];
    }
  }

// Calculate the indices for all the vertices on face 3
//        v2-----e10----v6
//        |              |
//        e1     f3     e5
//        |              |
//        v1-----e9-----v5

  F[0] = 0; F[n] = 4; F[n * (n + 1) + 0] = 1; F[n * (n + 1) + n] = 5;
  for (unsigned i = 0; i < num_edge_coords; ++i) {
    F[i + 1] =                 edge_vertices[8 * num_edge_coords + i];
    F[(i + 1) * (n + 1)] =     edge_vertices[0 * num_edge_coords + i];
    F[(i + 1) * (n + 1) + n] = edge_vertices[4 * num_edge_coords + i];
    F[n * (n + 1) + i + 1] =   edge_vertices[9 * num_edge_coords + i];
  }

  for (unsigned i = 0; i < n-1; ++i)
    for (unsigned j = 0; j < n-1; ++j)
      F[(i + 1) * (n + 1) + (j + 1)] += (n - 1) * (n - 1);

  for (unsigned i = 0; i < n; ++i) {
    for (unsigned j = 0; j < n; ++j) {
      (*cell_to_vertex)[cell_to_vertex_offset++] = F[(i + 0) * (n + 1) + (j + 0)];
      (*cell_to_vertex)[cell_to_vertex_offset++] = F[(i + 1) * (n + 1) + (j + 0)];
      (*cell_to_vertex)[cell_to_vertex_offset++] = F[(i + 1) * (n + 1) + (j + 1)];
      (*cell_to_vertex)[cell_to_vertex_offset++] = F[(i + 0) * (n + 1) + (j + 1)];
    }
  }

// Calculate the indices for all the vertices on face 4
//        v4-----e12----v8
//        |              |
//        e4     f4     e8
//        |              |
//        v3-----e11----v7

  F[0] = 2; F[n] = 6; F[n * (n + 1) + 0] = 3; F[n * (n + 1) + n] = 7;
  for (unsigned i = 0; i < num_edge_coords; ++i) {
    F[i + 1] =                 edge_vertices[10 * num_edge_coords + i];
    F[(i + 1) * (n + 1)] =     edge_vertices[ 3 * num_edge_coords + i];
    F[(i + 1) * (n + 1) + n] = edge_vertices[ 7 * num_edge_coords + i];
    F[n * (n + 1) + i + 1] =   edge_vertices[11 * num_edge_coords + i];
  }

  for (unsigned i = 0; i < n-1; ++i)
    for (unsigned j = 0; j < n-1; ++j)
      F[(i + 1) * (n + 1) + (j + 1)] += (n - 1) * (n - 1);

  for (unsigned i = 0; i < n; ++i) {
    for (unsigned j = 0; j < n; ++j) {
      (*cell_to_vertex)[cell_to_vertex_offset++] = F[(i + 0) * (n + 1) + (j + 0)];
      (*cell_to_vertex)[cell_to_vertex_offset++] = F[(i + 1) * (n + 1) + (j + 0)];
      (*cell_to_vertex)[cell_to_vertex_offset++] = F[(i + 1) * (n + 1) + (j + 1)];
      (*cell_to_vertex)[cell_to_vertex_offset++] = F[(i + 0) * (n + 1) + (j + 1)];
    }
  }

// Calculate the indices for all the vertices on face 5
//        v5-----e6-----v7
//        |              |
//        e9     f5     e11
//        |              |
//        v1-----e2-----v3

  F[0] = 0; F[n] = 2; F[n * (n + 1) + 0] = 4; F[n * (n + 1) + n] = 6;
  for (unsigned i = 0; i < num_edge_coords; ++i) {
    F[i + 1] =                 edge_vertices[ 1 * num_edge_coords + i];
    F[(i + 1) * (n + 1)] =     edge_vertices[ 8 * num_edge_coords + i];
    F[(i + 1) * (n + 1) + n] = edge_vertices[10 * num_edge_coords + i];
    F[n * (n + 1) + i + 1] =   edge_vertices[ 5 * num_edge_coords + i];
  }

  for (unsigned i = 0; i < n-1; ++i)
    for (unsigned j = 0; j < n-1; ++j)
      F[(i + 1) * (n + 1) + (j + 1)] += (n - 1) * (n - 1);

  for (unsigned i = 0; i < n; ++i) {
    for (unsigned j = 0; j < n; ++j) {
      (*cell_to_vertex)[cell_to_vertex_offset++] = F[(i + 0) * (n + 1) + (j + 0)];
      (*cell_to_vertex)[cell_to_vertex_offset++] = F[(i + 1) * (n + 1) + (j + 0)];
      (*cell_to_vertex)[cell_to_vertex_offset++] = F[(i + 1) * (n + 1) + (j + 1)];
      (*cell_to_vertex)[cell_to_vertex_offset++] = F[(i + 0) * (n + 1) + (j + 1)];
    }
  }

// Calculate the indices for all the vertices on face 6
//        v6-----e7-----v8
//        |              |
//       e10     f6     e12
//        |              |
//        v2-----e3-----v4

  F[0] = 1; F[n] = 3; F[n * (n + 1) + 0] = 5; F[n * (n + 1) + n] = 7;
  for (unsigned i = 0; i < num_edge_coords; ++i) {
    F[i + 1] =                 edge_vertices[ 2 * num_edge_coords + i];
    F[(i + 1) * (n + 1)] =     edge_vertices[ 9 * num_edge_coords + i];
    F[(i + 1) * (n + 1) + n] = edge_vertices[11 * num_edge_coords + i];
    F[n * (n + 1) + i + 1] =   edge_vertices[ 6 * num_edge_coords + i];
  }

  for (unsigned i = 0; i < n-1; ++i)
    for (unsigned j = 0; j < n-1; ++j)
      F[(i + 1) * (n + 1) + (j + 1)] += (n - 1) * (n - 1);

  for (unsigned i = 0; i < n; ++i) {
    for (unsigned j = 0; j < n; ++j) {
      (*cell_to_vertex)[cell_to_vertex_offset++] = F[(i + 0) * (n + 1) + (j + 0)];
      (*cell_to_vertex)[cell_to_vertex_offset++] = F[(i + 1) * (n + 1) + (j + 0)];
      (*cell_to_vertex)[cell_to_vertex_offset++] = F[(i + 1) * (n + 1) + (j + 1)];
      (*cell_to_vertex)[cell_to_vertex_offset++] = F[(i + 0) * (n + 1) + (j + 1)];
    }
  }

  free(edge_vertices);
  free(F);
}
