#ifndef WEIGHT_STORE_H
#define WEIGHT_STORE_H

#include <stdlib.h>

struct weight_vector {

  double * weights;
  size_t * idx; // source index
  size_t n;
};

struct weight_matrix {

  struct weight_vector * weights;
  size_t n;
};

void weight_vector_init(struct weight_vector * vector);
void weight_vector_add_weight(
  struct weight_vector * vector, double weight, size_t idx);
void weight_vector_mult_scalar(struct weight_vector * vector, double scalar);
void weight_vector_add_weight_vector(
  struct weight_vector * vector_a, struct weight_vector * vector_b,
  double factor); // a = a + b * factor
double weight_vector_mult_vector(
  struct weight_vector * vector, double * f);
void weight_vector_compact(struct weight_vector * vector);
void weight_vector_free(struct weight_vector * vector);

void weight_matrix_mult_vector(
  struct weight_matrix weights, double * f, double * results);

#endif // WEIGHT_STORE_H
