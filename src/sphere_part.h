/**
 * @file sphere_part.h
 *
 * @copyright Copyright  (C)  2014 Moritz Hanke <hanke@dkrz.de>
 *                                 Thomas Jahns <jahns@dkrz.de>
 *
 * @version 1.0
 * @author Moritz Hanke <hanke@dkrz.de>
 *         Thomas Jahns <jahns@dkrz.de>
 */
/*
 * Keywords:
 * Maintainer: Moritz Hanke <hanke@dkrz.de>
 * URL: https://doc.redmine.dkrz.de/YAC/html/index.html
 *
 * This file is part of YAC.
 *
 * YAC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * YAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with YAC.  If not, see <http://www.gnu.org/licenses/gpl.txt>.
 */

#ifndef SPHERE_PART_H
#define SPHERE_PART_H

#include "geometry.h"

struct point_sphere_part_node {

   unsigned flags;

   void * U, * T;

   unsigned U_size, T_size;

   double gc_norm_vector[3];
};

/**
 * \file sphere_part.h
 * \brief algorithm for searching cells and points on a grid
 *
 * \ref init_sphere_part_node generates a tree structure, which makes it easy to look for cells and points.
 * A documentation of the respective algorithm can be found at \ref sphere_part_docu.
 */

struct point_sphere_part_search;

struct point_sphere_part_search * yac_point_sphere_part_search_new (
  unsigned num_points, double * x_coordinates, double * y_coordinates);

void yac_delete_point_sphere_part_search(
  struct point_sphere_part_search * search);

/**
 * This routine does a nearest neighbour search between the points provided to
 * this routine and the matching yac_point_sphere_part_search_new call.
 */
void yac_point_sphere_part_search_NN(struct point_sphere_part_search * search,
                                     unsigned num_points,
                                     double * coordinates_xyz,
                                     double * cos_angles,
                                     unsigned ** local_point_ids,
                                     unsigned * local_point_ids_array_size,
                                     unsigned * num_local_point_ids);

/**
 * This routine does a n nearest neighbour search between the points provided to
 * this routine and the matching yac_point_sphere_part_search_new call.
 */
void yac_point_sphere_part_search_NNN(struct point_sphere_part_search * search,
                                      unsigned num_points,
                                      double * coordinates_xyz, unsigned n,
                                      double ** cos_angles,
                                      unsigned * cos_angles_array_size,
                                      unsigned ** local_point_ids,
                                      unsigned * local_point_ids_array_size,
                                      unsigned * num_local_point_ids);

/**
 * This routine returns true if the provided point_sphere_part_search contains
 * a point that is within the provided bounding circle.
 */
int yac_point_sphere_part_search_bnd_circle_contains_points(
  struct point_sphere_part_search * search,
  struct reduced_bounding_circle circle);

#endif // SPHERE_PART_H
