#ifndef SBB_PATCH_H
#define SBB_PATCH_H

void compute_sb_polynomials(
  unsigned degree, double * triangle, double * vertices, unsigned num_vertices,
  double * restrict sb_polynomials);
void compute_d_sb_polynomials(
  unsigned degree, double * triangle, double * vertices, unsigned num_vertices,
  double * direction, double * restrict sb_polynomials);
unsigned get_num_sbb_coefficients(unsigned degree);
void compute_sbb_patch_coefficients(
  unsigned degree, double * sb_polynomials, double * f_v, unsigned num_vertices,
  double * coefficients);
void evaluate_sbb_patch(
  double * vertices, unsigned num_vertices, double * triangle,
  unsigned degree, double * sbb_coefficients, double * p);
double compute_directional_derivative(
  unsigned degree, double * triangle, double * coefficients, double * vertex,
  double * direction);
double compute_directional_derivative_2(
  unsigned degree, double * triangle, double * coefficients, double * vertex,
  double * direction);

static inline unsigned get_sbb_coeff_idx_from_ijk(
  unsigned i, unsigned j, unsigned k, unsigned degree) {

  return (i*(2*degree + 3 - i))/2 + j;
}

#endif // SBB_PATCH_H
