#ifndef PATCH_COMMON_H
#define PATCH_COMMON_H

void compute_sb_coords(double * vertices, unsigned num_vertices,
                       double * triangle, double * sb_coords);

void compute_patch_coefficients(
  unsigned num_coeffs, double * polynomials, double * f_v,
  unsigned num_vertices, double * coefficients);

void evaluate_patch(
  double * vertices, size_t num_vertices, unsigned num_coeffs,
  double * restrict coefficients, double * restrict polynomials,
  double * restrict p);

void compute_patch_weights(
  unsigned num_coeffs, unsigned num_patch_vertices, double * patch_polynomials,
  unsigned num_eval_vertices, double * eval_polynomials, double * weights);

void evaluate_patch_weights(
  unsigned num_patch_vertices, unsigned num_eval_vertices,
  double * weights, double * f, double * p);
#endif