#include <math.h>

#include "test_function.h"
#include "geometry.h"

void compute_field_data(
  double * vertices, unsigned num_vertices, double * f_v) {

  for (unsigned i = 0; i < num_vertices; ++i) {

    double x = vertices[0 + 3*i];
    double y = vertices[1 + 3*i];
    double z = vertices[2 + 3*i];

    double lon, lat;
    XYZtoLL(&vertices[0 + 3*i], &lon, &lat);

    // f = 1 + x^8 + e^(2y^3) + e^(2x^2) + 10xyz
    f_v[i] = 1.0 + pow(x, 8.0) + exp(2.0*y*y*y) + exp(2.0*x*x) + 10.0*x*y*z;

    // f_v[i] = sin(lon * 2.0) * cos(lat * 3.0);
    // f_v[i] = sin(lon * 2.0) * cos(lat * 3.0) + 1.0;

    // f = x^3 + 3.0 * x*y^2 + 5 * x*y*z
    // f_v[i] = x*x*x + 3.0*x*y*y + 5.0*x*y*z + 1.0;

    // f_v[i] = 1.0;
  }
}

void compute_field_data_derivative(
  double * vertices, unsigned num_vertices, double * u, double * df_v) {

  double ux = u[0];
  double uy = u[1];
  double uz = u[2];

  for (unsigned i = 0; i < num_vertices; ++i) {

    double x = vertices[0 + 3*i];
    double y = vertices[1 + 3*i];
    double z = vertices[2 + 3*i];

    // d(f)/d(u) = ux * (8*x^7 + 4*x*e^(2x^2)+10yz) +
    //             uy * (6*y^2*e^(2y^3) + 10xz)) + uz * 10xy
    df_v[i] = ux * (8.0*pow(x, 7.0) + 4.0*x*exp(2.0*x*x) + 10.0*y*z) +
              uy * (6.0*y*y*exp(2.0*y*y*y) + 10.0*x*z) +
              uz * 10.0*x*y;

    // df_v[i] = 0.0;
  }
}

void compute_field_data_derivative_2 (
  double * vertices, unsigned num_vertices, double * u, double * df_v) {

  double ux = u[0];
  double uy = u[1];
  double uz = u[2];

  double scale = 1.0e-6;

  for (unsigned i = 0; i < num_vertices; ++i) {

    double test_vertices[2][3];

    test_vertices[0][0] = vertices[0 + 3*i];
    test_vertices[0][1] = vertices[1 + 3*i];
    test_vertices[0][2] = vertices[2 + 3*i];
    test_vertices[1][0] = vertices[0 + 3*i] + ux * scale;
    test_vertices[1][1] = vertices[1 + 3*i] + uy * scale;
    test_vertices[1][2] = vertices[2 + 3*i] + uz * scale;

    double f[2];

    compute_field_data(&test_vertices[0][0], 2, &f[0]);

    df_v[i] = (f[1] - f[0]) / scale;
  }
}
