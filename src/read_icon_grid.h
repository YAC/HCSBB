#ifndef READ_ICON_GRID_H
#define READ_ICON_GRID_H

/**
 * reads in an icon grid netcdf file and return the grid information in
 * a format that is supported by the YAC user interface.
 * @param[in]  filename              name of the icon grid netcdf file
 * @param[out] nbr_vertices          number of vertices in the grid
 * @param[out] nbr_cells             number of cells in the grid
 * @param[out] nbr_edges             number of edges in the grid
 * @param[out] num_vertices_per_cell number of vertices per cell
 * @param[out] cell_to_vertex        vertex indices for each cell
 * @param[out] edge_to_vertex        vertex indices for each edge
 * @param[out] x_vertices            longitudes of vertices
 * @param[out] y_vertices            latitudes of vertices
 * @param[out] x_cells               longitudes of cell center
 * @param[out] y_cells               latitudes of cell center
 * @param[out] x_edges               longitudes of edge middle point
 * @param[out] y_edges               latitudes of edge middle point
 * @param[out] cell_mask             mask for cells
 */
void read_icon_grid_information(const char * filename, int * nbr_vertices,
                                int * nbr_cells, int * nbr_edges,
                                int ** num_vertices_per_cell,
                                int ** cell_to_vertex, int ** vertex_to_cell,
                                int ** edge_to_vertex,
                                double ** x_vertices, double ** y_vertices,
                                double ** x_cells, double ** y_cells,
                                double ** x_edges, double ** y_edges,
                                int ** cell_mask);

#endif // READ_ICON_GRID_H
