#include <stdlib.h>
#include <stdio.h>

#include "utils.h"

void abort_message ( const char * text, const char * file, int line )
{
  fprintf(stderr, "%s \n", text); 
  fprintf(stderr, "Aborting in file %s, line %i ...\n", file, line );
  exit(EXIT_FAILURE);
}
